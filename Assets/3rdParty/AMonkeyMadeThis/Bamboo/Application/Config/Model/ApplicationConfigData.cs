﻿using AMonkeyMadeThis.Bamboo.AssetBundle;
using UnityEngine;

namespace AMonkeyMadeThis.Bamboo.Application.Config
{
	/// <summary>
	/// ApplicationConfigData
	/// </summary>
	[CreateAssetMenu(menuName = "A Monkey Made This/Bamboo/Application/Config/Model/Application Config Data", fileName = "ApplicationConfig")]
	public class ApplicationConfigData : DataAsset
	{
		#region Enums
		
		public enum ProtocolType
		{
			NA,
			File,
			HTTP,
			HTTPS
		}

		#endregion

		#region Properties

		public ProtocolType Protocol;
		public string Host = "localhost/Energizer/POS%20Placement";
		public string UriPattern = "{0}://{1}";

		public string LocalisationPath = "Resources/Localisation/Locale/{0}";
		public string DataPath = "Data/Data";

		#endregion

		#region Accessors

		public string BaseURL
		{
			get
			{
				string _protocol = Protocol.ToString().ToLower();
				string uri = string.Format(UriPattern, _protocol, Host);

				return uri;
			}
		}

		public string AssetsURL
		{
			get
			{
				string uri = BaseURL + "/Assets";
				
				return uri;
			}
		}

		#endregion
	}
}
