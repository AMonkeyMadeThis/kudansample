﻿using UnityEngine;

namespace AMonkeyMadeThis.Bamboo.AssetBundle
{
	/// <summary>
	/// DataAsset
	/// </summary>
	public class DataAsset : ScriptableObject, IAsset
	{
		string _assetID;
		/// <summary>
		/// AssetID
		/// </summary>
		public string AssetID
		{
			get { return _assetID; }
			set
			{
				_assetID = value;
			}
		}
	}
}
