﻿namespace AMonkeyMadeThis.Bamboo.AssetBundle
{
	/// <summary>
	/// IAsset
	/// </summary>
	public interface IAsset
	{
		#region Properties

		/// <summary>
		/// AssetID
		/// </summary>
		string AssetID { get; set; }

		#endregion
	}
}
