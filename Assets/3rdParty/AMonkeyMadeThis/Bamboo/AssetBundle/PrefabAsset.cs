﻿using UnityEngine;

namespace AMonkeyMadeThis.Bamboo.AssetBundle
{
	/// <summary>
	/// PrefabAsset
	/// </summary>
	public class PrefabAsset : MonoBehaviour, IAsset
	{
		[SerializeField]
		string _assetID;
		/// <summary>
		/// AssetID
		/// </summary>
		public string AssetID
		{
			get { return _assetID; }
			set
			{
				_assetID = value;
			}
		}

		[SerializeField]
		Sprite _icon;
		/// <summary>
		/// Icon
		/// </summary>
		public Sprite Icon
		{
			get { return _icon; }
			set
			{
				_icon = value;
			}
		}
	}
}
