﻿using AMonkeyMadeThis.Bamboo.AssetBundle;
using AMonkeyMadeThis.Bamboo.Localisation.Enum;
using AMonkeyMadeThis.Bamboo.Localisation.Model;
using AMonkeyMadeThis.Common.Controller;
using POSPlacement.Common.AssetBundle.Controller;
using POSPlacement.Common.AssetBundle.Model;
using POSPlacement.Common.Data.Scripts.Model.VO;
using System.Collections.Generic;

namespace AMonkeyMadeThis.Bamboo.Localisation.Controller
{
	/// <summary>
	/// LocalisationController
	/// </summary>
	public class LocalisationController : AbstractController
	{
		#region Singleton

		private static LocalisationController _instance;
		/// <summary>
		/// Instance
		/// </summary>
		public static LocalisationController Instance
		{
			get { return _instance; }
			private set
			{
				_instance = value;
			}
		}

		#endregion

		#region Instance vars

		/// <summary>
		/// AssetBundleController
		/// </summary>
		AssetBundleController AssetBundleController;
		/// <summary>
		/// AssetBundleModel
		/// </summary>
		AssetBundleModel AssetBundleModel;
		/// <summary>
		/// LocalisationModel
		/// </summary>
		LocalisationModel LocalisationModel;

		#endregion

		#region Constructor

		/// <summary>
		/// LocalisationController
		/// </summary>
		public LocalisationController()
		{
			Instance = this;
		}

		#endregion

		#region Lifecycle

		/// <summary>
		/// InitVars
		/// </summary>
		protected override void InitVars()
		{
			base.InitVars();

			AssetBundleController = AssetBundleController.Instance;
			AssetBundleModel = AssetBundleModel.Instance;
			LocalisationModel = LocalisationModel.Instance;
		}

		#endregion

		#region Loading

		public void LoadLocalisation(List<AssetBundleVO> assetBundles)
		{
			// tell asset bundle controller to load, we will process the result via a listener
			AssetBundleController.LoadAssets(assetBundles);
		}

		#endregion

		#region Listeners

		public override void AddListeners()
		{
			base.AddListeners();

			AddListeners(AssetBundleModel);
		}

		private void AddListeners(AssetBundleModel assetBundleModel)
		{
			if (assetBundleModel != null)
			{
				assetBundleModel.OnAssetsChanged += HandleAssetBundleModelOnAssetsChanged;
			}
		}
		
		public override void RemoveListeners()
		{
			base.RemoveListeners();

			RemoveListeners(AssetBundleModel);
		}

		private void RemoveListeners(AssetBundleModel assetBundleModel)
		{
			if (assetBundleModel != null)
			{
				assetBundleModel.OnAssetsChanged -= HandleAssetBundleModelOnAssetsChanged;
			}
		}
		
		#endregion

		#region Handlers

		private void HandleAssetBundleModelOnAssetsChanged(List<IAsset> assets)
		{
			PopulateFromAssets(assets);
			SelectInitialLocale();
		}

		#endregion

		#region Populate

		private void PopulateFromAssets(List<IAsset> assets)
		{
			List<ResourceBundle> resourceBundles = GetAllResourceBundles(assets);

			if (resourceBundles != null && resourceBundles.Count > 0)
			{
				List<LocaleChain> localeChains = GetLocaleChains(assets);

				LocalisationModel.LoadedResourceBundles = resourceBundles;
				LocalisationModel.Chains = localeChains.ToArray();
				LocalisationModel.Available = localeChains;
			}
		}

		private List<ResourceBundle> GetAllResourceBundles(List<IAsset> assets)
		{
			List<ResourceBundle> result = new List<ResourceBundle>();

			foreach (var asset in assets)
			{
				var resourceBundle = asset as ResourceBundle;

				if (resourceBundle != null)
					result.Add(resourceBundle);
			}

			return result;
		}

		private List<LocaleChain> GetLocaleChains(List<IAsset> assets)
		{
			List<LocaleChain> result = new List<LocaleChain>();

			foreach(var asset in assets)
			{
				var localeChain = asset as LocaleChain;

				if (localeChain != null)
					result.Add(localeChain);
			}

			return result;
		}

		#endregion

		#region Registrations

		/// <summary>
		/// RegisterResourceBundleUse
		/// </summary>
		/// <param name="resourceBundleName"></param>
		public void RegisterResourceBundleUse(string resourceBundleName)
		{
			LocalisationModel.RegisterResourceBundleUse(resourceBundleName);
		}

		/// <summary>
		/// UnRegisterResourceBundleUse
		/// </summary>
		/// <param name="resourceBundleName"></param>
		public void UnRegisterResourceBundleUse(string resourceBundleName)
		{
			LocalisationModel.UnRegisterResourceBundleUse(resourceBundleName);
		}

		#endregion

		private void SelectInitialLocale()
		{
			if (LocalisationModel.HasAvailable)
			{
				LocaleCode first = LocalisationModel.FirstAvailable;
				LocalisationModel.Current = first;
			}
		}
		
		public string LocaliseString(string key, string resourceBundleName = "Application")
		{
			return LocalisationModel.LocaliseString(key, resourceBundleName);
		}
	}
}
