﻿using System;

namespace AMonkeyMadeThis.Bamboo.Localisation.Enum
{
	/// <summary>
	/// LocaleCode
	/// </summary>
	[Flags]
	public enum LocaleCode
	{
		NA,//Untranslated
		en,//English
		en_GB,//British English
		en_US,//American English
		en_CA,//Canadian English
		en_IN,//Indian English
		es,//Spanish
		es_ES,//Castilian Spanish
		es_MX,// Mexican Spanish
		es_AR,//Argentine Spanish
		es_CO,//Colombian Spanish
		es_CL,//Chilean Spanish
		pt,//Portugese
		pt_PT,//European Portuguese
		pt_BR,//Brazilian Portuguese
		te_IN,//Indian Telugu
		ta_IN,//Indian Tamil
		ta_LK,//Sri Lankan Tamil
		kn_IN,//Indian Kannada
		zh,//Chinese
		zh_CN,//Simplified Chinese
		zh_TW,//Taiwan, traditional characters
		zh_HK,//Hong Kong, traditional characters
	}
}
