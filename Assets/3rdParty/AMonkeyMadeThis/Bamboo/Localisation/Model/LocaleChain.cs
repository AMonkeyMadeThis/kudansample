﻿using AMonkeyMadeThis.Bamboo.AssetBundle;
using AMonkeyMadeThis.Bamboo.Localisation.Enum;
using UnityEngine;

namespace AMonkeyMadeThis.Bamboo.Localisation.Model
{
	/// <summary>
	/// LocaleChain
	/// </summary>
	[CreateAssetMenu(menuName = "A Monkey Made This/Bamboo/Localisation/Model/Locale Chain", fileName = "LocaleChain")]
	public class LocaleChain : DataAsset
	{
		#region Properties
		
		/// <summary>
		/// Locale
		/// </summary>
		public LocaleCode Locale;
		/// <summary>
		/// Label
		/// </summary>
		public string Label;
		/// <summary>
		/// Flag
		/// </summary>
		public Sprite Flag;
		/// <summary>
		/// Chain
		/// </summary>
		public LocaleCode[] Chain;

		#endregion
	}
}
