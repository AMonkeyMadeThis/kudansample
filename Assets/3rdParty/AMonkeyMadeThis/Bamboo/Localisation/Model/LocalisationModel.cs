﻿using AMonkeyMadeThis.Bamboo.Localisation.Enum;
using AMonkeyMadeThis.Common.Model;
using System.Collections.Generic;
using System;

namespace AMonkeyMadeThis.Bamboo.Localisation.Model
{
	/// <summary>
	/// LocalisationModel
	/// </summary>
	public class LocalisationModel : AbstractModel
	{
		#region Singleton

		private static LocalisationModel _instance;
		/// <summary>
		/// Instance
		/// </summary>
		public static LocalisationModel Instance
		{
			get { return _instance; }
			private set
			{
				_instance = value;
			}
		}

		#endregion

		#region Events

		/// <summary>
		/// LocalisationAvailableAction
		/// </summary>
		public delegate void LocalisationAvailableAction();
		/// <summary>
		/// OnLocalisationAvailable
		/// </summary>
		public event LocalisationAvailableAction OnLocalisationAvailable;
		
		/// <summary>
		/// LocaleChangedAction
		/// </summary>
		public delegate void LocaleChangedAction();
		/// <summary>
		/// OnLocaleChanged
		/// </summary>
		public event LocaleChangedAction OnLocaleChanged;

		#endregion

		#region Instance Vars

		private LocaleCode _current;
		/// <summary>
		/// Current
		/// </summary>
		public LocaleCode Current
		{
			get { return _current; }
			set
			{
				SetCurrent(value);
			}
		}

		/// <summary>
		/// Available
		/// </summary>
		public List<LocaleChain> Available;
		/// <summary>
		/// Chains
		/// </summary>
		public LocaleChain[] Chains;
		/// <summary>
		/// CurrentChain
		/// </summary>
		private LocaleChain CurrentChain
		{
			get
			{
				foreach (var chain in Chains)
				{
					if (chain.Locale == Current)
						return chain;
				}

				return null;
			}
		}

		private Dictionary<string, int> _resourceBundleRegistrations;
		/// <summary>
		/// ResourceBundleRegistrations
		/// </summary>
		public Dictionary<string, int> ResourceBundleRegistrations
		{
			get { return _resourceBundleRegistrations; }
			set
			{
				_resourceBundleRegistrations = value;
			}
		}
		/// <summary>
		/// LoadedResourceBundles
		/// </summary>
		public List<ResourceBundle> LoadedResourceBundles;
		/// <summary>
		/// ResourceBundles
		/// </summary>
		public Dictionary<string, Dictionary<string, string>> ResourceBundles = new Dictionary<string, Dictionary<string, string>>();

		#endregion

		#region Accessors

		public bool HasAvailable
		{
			get
			{
				bool hasAvailable = (Available != null && Available.Count > 0);
				return hasAvailable;
			}
		}

		public LocaleCode FirstAvailable
		{
			get
			{
				LocaleCode firstAvailable = (HasAvailable) ? Available[0].Locale : LocaleCode.NA;

				return firstAvailable;
			}
		}

		/// <summary>
		/// CurrentChainList
		/// </summary>
		public List<LocaleCode> CurrentChainList
		{
			get
			{
				List<LocaleCode> locales = new List<LocaleCode>();
				locales.Add(Current);

				if (CurrentChain != null && CurrentChain.Chain != null)
					locales.AddRange(CurrentChain.Chain);

				locales.Reverse();

				return locales;
			}
		}

		#endregion

		#region Mutators

		private void SetCurrent(LocaleCode value)
		{
			if (_current != value)
			{
				_current = value;
				BuildDictionaries();
				DispatchOnLocaleChanged();
			}
		}
		
		public void ClearResourceBundles()
		{
			ResourceBundles.Clear();
		}

		#endregion

		#region Constructor

		/// <summary>
		/// LocalisationModel
		/// </summary>
		public LocalisationModel()
		{
			Instance = this;
		}

		#endregion

		#region Processing
		
		private void BuildDictionaries()
		{
			// Clear
			ClearResourceBundles();
			// (Re)Build dictionaries
			ProcessResourceBundleChain(CurrentChainList);
		}

		private void ProcessResourceBundleChain(List<LocaleCode> currentChainList)
		{
			foreach (var locale in currentChainList)
			{
				foreach (var resourceBundle in LoadedResourceBundles)
				{
					if (resourceBundle != null && resourceBundle.Locale == locale)
					{
						string resourceBundleName = resourceBundle.BundleName;

						if (resourceBundleName != null && resourceBundleName.Length > 0)
							BuildResourceBundle(resourceBundleName, locale);
					}
				}
			}
		}

		private void BuildResourceBundle(string resourceBundleName, LocaleCode locale)
		{
			bool resourceBundleExists = ResourceBundles.ContainsKey(resourceBundleName);
			Dictionary<string, string> resourceBundle = (resourceBundleExists) ? ResourceBundles[resourceBundleName] : new Dictionary<string, string>();
			ResourceBundle loadedResourceBundle = GetLoadedResourceBundle(resourceBundleName, locale);
			
			if (loadedResourceBundle != null && loadedResourceBundle.Content != null)
			{
				List<ResourceItem> content = loadedResourceBundle.Content;

				foreach (var item in content)
				{
					string key = item.Name;
					string value = item.Value;

					if (resourceBundle.ContainsKey(key))
					{
						resourceBundle[key] = value;
					}
					else
					{
						resourceBundle.Add(key, value);
					}
				}

				if (!resourceBundleExists)
					ResourceBundles.Add(resourceBundleName, resourceBundle);
			}
		}

		private ResourceBundle GetLoadedResourceBundle(String bundleName, LocaleCode locale)
		{
			if (LoadedResourceBundles != null && LoadedResourceBundles.Count > 0)
			{
				foreach(var resourceBundle in LoadedResourceBundles)
				{
					if (resourceBundle != null && resourceBundle.Locale == locale && resourceBundle.BundleName == bundleName)
						return resourceBundle;
				}
			}

			return null;
		}
		
		#endregion

		#region Registrations

		/// <summary>
		/// RegisterResourceBundleUse
		/// </summary>
		/// <param name="resourceBundleName"></param>
		public void RegisterResourceBundleUse(string resourceBundleName)
		{
			if (ResourceBundleRegistrations == null)
				ResourceBundleRegistrations = new Dictionary<string, int>();

			if (!ResourceBundleRegistrations.ContainsKey(resourceBundleName))
			{
				ResourceBundleRegistrations.Add(resourceBundleName, 1);
				BuildDictionaries();
			}
			else
			{
				ResourceBundleRegistrations[resourceBundleName]++;
			}
		}

		/// <summary>
		/// UnRegisterResourceBundleUse
		/// </summary>
		/// <param name="resourceBundleName"></param>
		public void UnRegisterResourceBundleUse(string resourceBundleName)
		{
			if (ResourceBundleRegistrations == null)
				ResourceBundleRegistrations = new Dictionary<string, int>();

			if (ResourceBundleRegistrations.ContainsKey(resourceBundleName))
			{
				ResourceBundleRegistrations[resourceBundleName]--;

				if (ResourceBundleRegistrations[resourceBundleName] <= 0)
					ResourceBundleRegistrations.Remove(resourceBundleName);
			}
		}

		#endregion

		#region Content
		
		public string LocaliseString(string key, string resourceBundleName = "Application")
		{
			string _resourceBundleName = (resourceBundleName != null && resourceBundleName.Length > 0) ? resourceBundleName : "Application";
			
			if (_resourceBundleName != null && _resourceBundleName.Length > 0 && ResourceBundles.ContainsKey(_resourceBundleName))
			{
				Dictionary<string, string> resourceBundle = ResourceBundles[_resourceBundleName];

				if (resourceBundle != null && key != null && key.Length > 0 && resourceBundle.ContainsKey(key))
				{
					string _result = resourceBundle[key];
					
					return _result;
				}
			}

			return string.Empty;
		}

		#endregion

		#region Dispatchers

		/// <summary>
		/// DispatchOnLocalisationAvailable
		/// </summary>
		private void DispatchOnLocalisationAvailable()
		{
			if (OnLocalisationAvailable != null)
				OnLocalisationAvailable();
		}
		/// <summary>
		/// DispatchOnLocaleChanged
		/// </summary>
		private void DispatchOnLocaleChanged()
		{
			if (OnLocaleChanged != null)
				OnLocaleChanged();
		}

		#endregion
	}
}
