﻿using AMonkeyMadeThis.Bamboo.AssetBundle;
using AMonkeyMadeThis.Bamboo.Localisation.Enum;
using System.Collections.Generic;
using UnityEngine;

namespace AMonkeyMadeThis.Bamboo.Localisation.Model
{
	/// <summary>
	/// ResourceBundle
	/// </summary>
	[CreateAssetMenu(menuName = "A Monkey Made This/Bamboo/Localisation/Model/Resource Bundle", fileName = "ResourceBundle")]
	public class ResourceBundle : DataAsset
	{
		#region Properties
		
		/// <summary>
		/// Locale
		/// </summary>
		public LocaleCode Locale;
		/// <summary>
		/// Name
		/// </summary>
		public string BundleName;
		/// <summary>
		/// Content
		/// </summary>
		public List<ResourceItem> Content;

		#endregion
	}
}