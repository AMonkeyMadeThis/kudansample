﻿using System;

namespace AMonkeyMadeThis.Bamboo.Localisation.Model
{
	/// <summary>
	/// ResourceItem
	/// </summary>
	[Serializable]
	public class ResourceItem
	{
		/// <summary>
		/// Key
		/// </summary>
		public string Name;
		/// <summary>
		/// Value
		/// </summary>
		public string Value;
	}
}
