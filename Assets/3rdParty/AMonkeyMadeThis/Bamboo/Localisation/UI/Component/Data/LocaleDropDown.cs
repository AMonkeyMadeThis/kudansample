﻿using AMonkeyMadeThis.Bamboo.Localisation.Controller;
using AMonkeyMadeThis.Bamboo.Localisation.Enum;
using AMonkeyMadeThis.Bamboo.Localisation.Model;
using AMonkeyMadeThis.Bamboo.UI.Component;

namespace AMonkeyMadeThis.Bamboo.Localisation.UI.Component.Data
{
	/// <summary>
	/// LocaleDropDown
	/// </summary>
	public class LocaleDropDown : DropDownController
	{
		public override void Start()
		{
			base.Start();

			UpdateDataSource();
		}

		#region Listeners

		/// <summary>
		/// AddListeners
		/// </summary>
		public override void AddListeners()
		{
			base.AddListeners();

			AddListeners(LocalisationModel.Instance);
		}

		private void AddListeners(LocalisationModel localisationModel)
		{
			if (localisationModel != null)
			{
				localisationModel.OnLocalisationAvailable += HandleLocalisationAvailable;
			}
		}

		/// <summary>
		/// RemoveListeners
		/// </summary>
		public override void RemoveListeners()
		{
			base.RemoveListeners();

			RemoveListeners(LocalisationModel.Instance);
		}

		private void RemoveListeners(LocalisationModel localisationModel)
		{
			if (localisationModel != null)
			{
				localisationModel.OnLocalisationAvailable -= HandleLocalisationAvailable;
			}
		}

		#endregion

		#region Handlers

		/// <summary>
		/// HandleLocalisationAvailable
		/// </summary>
		private void HandleLocalisationAvailable()
		{
			UpdateDataSource();
		}

		/// <summary>
		/// HandleDropDownValueChanged
		/// </summary>
		protected override void HandleDropDownValueChanged()
		{
			base.HandleDropDownValueChanged();
			
			LocaleChain selected = (LocaleChain)SelectedItem;
			LocaleCode locale = selected.Locale;
			LocalisationModel.Instance.Current = locale;
		}

		#endregion

		#region Population

		/// <summary>
		/// UpdateDataSource
		/// </summary>
		private void UpdateDataSource()
		{
			this.Datasource = LocalisationModel.Instance.Available;
			MakeInitialSelection();
		}

		#endregion

		#region Selection

		protected override void MakeInitialSelection()
		{
			base.MakeInitialSelection();

			if (Datasource != null && Datasource.Count > 0)
			{
				SelectedIndex = IndexOfLocale(LocalisationModel.Instance.Current);
			}
		}

		private int IndexOfLocale(LocaleCode target)
		{
			foreach (var chain in Datasource)
			{
				var _chain = chain as LocaleChain;

				if (_chain != null && _chain.Locale == target)
					return Datasource.IndexOf(_chain);
			}

			return -1;
		}

		#endregion
	}
}
