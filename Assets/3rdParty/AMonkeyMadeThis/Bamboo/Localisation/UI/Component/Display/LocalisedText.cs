﻿using AMonkeyMadeThis.Bamboo.Localisation.Controller;
using AMonkeyMadeThis.Bamboo.UI.Component;
using UnityEngine;
using UnityEngine.UI;

namespace AMonkeyMadeThis.Bamboo.Localisation.UI.Component.Display
{
	/// <summary>
	/// LocalisedText
	/// </summary>
	[RequireComponent(typeof(Text))]
	public class LocalisedText : UIComponent
	{
		[SerializeField]
		private string _resourceKey;
		/// <summary>
		/// ResourceKey
		/// </summary>
		public string ResourceKey
		{
			get { return _resourceKey; }
			set
			{
				_resourceKey = value;
				InvalidateProperties();
			}
		}

		/// <summary>
		/// Label
		/// </summary>
		protected Text Label;

		public override void OnEnable()
		{
			base.OnEnable();

			ClearLabelText();
		}

		public override void OnDisable()
		{
			base.OnDisable();
			
			ClearLabelText();
		}

		public override void Awake()
		{
			base.Awake();

			ClearLabelText();
		}
		
		protected override void CollectComponents()
		{
			base.CollectComponents();

			Label = GetComponent<Text>();
		}
		
		protected override void CommitProperties()
		{
			UpdateLabelText();

			base.CommitProperties();
		}

		private void ClearLabelText()
		{
			if (Label != null)
				Label.text = string.Empty;
		}

		private void UpdateLabelText()
		{
			if (Label != null)
				Label.text = LocalisedLabelText;
		}

		protected virtual string LocalisedLabelText
		{
			get
			{
				string result = LocalisationController.Instance.LocaliseString(ResourceKey, ResourceBundle);
				return result;
			}
		}

		public override void OnValidate()
		{
			base.OnValidate();
			
			var label = GetComponent<Text>();

			if (label != null)
				label.text = ResourceKey;
		}
	}
}
