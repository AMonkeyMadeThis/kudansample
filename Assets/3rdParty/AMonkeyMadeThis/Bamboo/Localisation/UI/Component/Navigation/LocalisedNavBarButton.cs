﻿using AMonkeyMadeThis.Bamboo.Localisation.UI.Component.Display;
using AMonkeyMadeThis.Bamboo.UI.Component.Navigation;

namespace AMonkeyMadeThis.Bamboo.Localisation.UI.Component.Navigation
{
	/// <summary>
	/// LocalisedNavBarButton
	/// </summary>
	public class LocalisedNavBarButton : NavBarButton
	{
		/// <summary>
		/// LocalisedText
		/// </summary>
		public LocalisedText LocalisedText;

		protected override void CollectComponents()
		{
			base.CollectComponents();

			LocalisedText = GetComponentInChildren<LocalisedText>();
		}
		
		protected override void SetLabelText()
		{
			if (LocalisedText != null)
				LocalisedText.ResourceKey = Text;
		}
	}
}
