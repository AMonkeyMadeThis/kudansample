﻿using AMonkeyMadeThis.Bamboo.UI.Effects.Screen.Fade;
using AMonkeyMadeThis.Common.Controller;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace AMonkeyMadeThis.Bamboo.UI.Component.Application
{
	/// <summary>
	/// ModuleLoader
	/// </summary>
	public class ModuleLoader : AbstractController
	{
		#region Singleton

		static ModuleLoader _instance;
		/// <summary>
		/// Instance
		/// </summary>
		public static ModuleLoader Instance
		{
			get { return _instance; }
			private set
			{
				_instance = value;
			}
		}

		#endregion

		#region Properties

		/// <summary>
		/// History
		/// </summary>
		private Stack History;
		/// <summary>
		/// ScreenFadeOut
		/// </summary>
		ScreenFadeOut ScreenFadeOut;
		/// <summary>
		/// ScreenFadeIn
		/// </summary>
		ScreenFadeIn ScreenFadeIn;

		#endregion

		#region Constructor

		public ModuleLoader()
		{
			Instance = this;
		}

		#endregion

		#region Lifecycle
		
		protected override void InitVars()
		{
			base.InitVars();

			History = new Stack();
		}

		protected override void CollectComponents()
		{
			base.CollectComponents();

			ScreenFadeOut = GetComponent<ScreenFadeOut>();
			ScreenFadeIn = GetComponent<ScreenFadeIn>();
		}

		#endregion

		#region Navigation

		public void GoTo(string moduleName)
		{
			GoTo(moduleName, true);
		}

		public void GoTo(string moduleName, bool addHistory)
		{
			if (!string.IsNullOrEmpty(moduleName))
			{
				if (addHistory)
					History.Push(SceneManager.GetActiveScene().name);
				//
				LoadScene(moduleName);
			}
		}

		public void GoBack()
		{
			if (History.Count > 0)
			{
				var moduleName = History.Pop() as string;

				if (!string.IsNullOrEmpty(moduleName))
					LoadScene(moduleName);
			}
		}

		private void LoadScene(string moduleName)
		{
			StartCoroutine(DoLoadScene(moduleName));
		}

		private IEnumerator DoLoadScene(string moduleName)
		{
			// Fade out
			if (ScreenFadeOut != null)
			{
				ScreenFadeOut.Animate();
				yield return new WaitForSecondsRealtime(ScreenFadeOut.Duration);
			}

			// Load new scene
			SceneManager.LoadScene(moduleName);

			// Fade in
			if (ScreenFadeIn != null)
			{
				ScreenFadeIn.Animate();

				if (ScreenFadeOut != null)
					ScreenFadeOut.Release();

				yield return new WaitForSecondsRealtime(ScreenFadeIn.Duration);
			}
		}

		#endregion
	}
}
