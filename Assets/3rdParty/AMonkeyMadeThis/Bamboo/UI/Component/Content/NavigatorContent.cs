﻿namespace AMonkeyMadeThis.Bamboo.UI.Component.Content
{
	/// <summary>
	/// NavigatorContent
	/// </summary>
	public class NavigatorContent : UIComponent
	{
		/// <summary>
		/// Label
		/// </summary>
		public string Label;
		/// <summary>
		/// ShowInMenu
		/// </summary>
		public bool ShowInMenu = true;
    }
}
