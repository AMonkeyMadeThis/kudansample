﻿using AMonkeyMadeThis.Common.Model.VO;
using AMonkeyMadeThis.Bamboo.UI.Component.Data;
using AMonkeyMadeThis.Bamboo.UI.Component.Data.ItemRenderers;
using System;

namespace AMonkeyMadeThis.Bamboo.UI.Component.Content
{
	/// <summary>
	/// Page
	/// </summary>
	public abstract class Page : NavigatorContent
    {
		#region Events

		/// <summary>
		/// delegate NavigateBackAction
		/// </summary>
		public delegate void NavigateBackAction();
		/// <summary>
		/// OnNavigateBack
		/// </summary>
		public event NavigateBackAction OnNavigateBack;
		/// <summary>
		/// delegate NavigatePreviousAction
		/// </summary>
		public delegate void NavigatePreviousAction();
		/// <summary>
		/// OnNavigatePrevious
		/// </summary>
		public event NavigatePreviousAction OnNavigatePrevious;
		/// <summary>
		/// delegate NavigateNextAction
		/// </summary>
		public delegate void NavigateNextAction();
		/// <summary>
		/// OnNavigateNext
		/// </summary>
		public event NavigateNextAction OnNavigateNext;

        #endregion

        #region Vars

        private ValueObject[] _dataSource;
		/// <summary>
		/// dataSource
		/// </summary>
		public ValueObject[] dataSource
		{
			get { return (container != null) ? container.dataSource : null; }
			set
			{
                if (container != null)
                    container.dataSource = value;
			}
		}

		/// <summary>
		/// itemRenderers
		/// </summary>
		public ItemRenderer[] itemRenderers;

		#endregion

		#region Components

		/// <summary>
		/// container
		/// </summary>
		public DataGroup container;

		#endregion

		#region Lifecycle

		/// <summary>
		/// CollectComponents
		/// </summary>
		protected override void CollectComponents()
        {
            base.CollectComponents();
            container = GetComponentInChildren<DataGroup>();
            container.ItemRendererFunction = ItemRendererFunction;
        }

		/// <summary>
		/// OnDisable
		/// </summary>
		public override void OnDisable()
        {
            base.OnDisable();
            dataSource = null;
        }

		#endregion

		#region Listeners

		/// <summary>
		/// AddListeners
		/// </summary>
		public override void AddListeners()
        {
            base.AddListeners();
            AddContainerListeners();
        }

		/// <summary>
		/// RemoveListeners
		/// </summary>
		public override void RemoveListeners()
        {
            base.RemoveListeners();
            RemoveContainerListeners();
        }

		/// <summary>
		/// AddContainerListeners
		/// </summary>
		private void AddContainerListeners()
        {
            if (container != null)
            {
                container.OnItemSelected += HandleOnItemSelected;
                container.OnContainerEmpty += HandleOnContainerEmpty;
            }
        }

		/// <summary>
		/// RemoveContainerListeners
		/// </summary>
		private void RemoveContainerListeners()
        {
            if (container != null)
            {
                container.OnItemSelected -= HandleOnItemSelected;
                container.OnContainerEmpty -= HandleOnContainerEmpty;
            }
        }

		#endregion

		#region Handlers

		/// <summary>
		/// HandleOnItemSelected
		/// </summary>
		/// <param name="itemRenderer"></param>
		private void HandleOnItemSelected(ItemRenderer itemRenderer)
        {
            ItemSelected(itemRenderer);
        }

		/// <summary>
		/// HandleOnContainerEmpty
		/// </summary>
		private void HandleOnContainerEmpty()
        {
            DispatchNavigateBack();
        }

		/// <summary>
		/// HandleOnItemRendererTapped
		/// </summary>
		/// <param name="itemRenderer"></param>
		private void HandleOnItemRendererTapped(ItemRenderer itemRenderer)
        {
            ItemSelected(itemRenderer);
        }

		#endregion

		#region Data

		/// <summary>
		/// set data provider, allowing a place to override functionality to add title objects for example
		/// </summary>
		/// <param name="dataSource"></param>
		protected virtual void updateDataProvider(ValueObject[] dataSource)
		{
			this.dataSource = dataSource;
		}

		#endregion

		#region ItemRenderers

		/// <summary>
		/// ItemRendererFunction allows a DataGroup to delegate its requests for an item renderer
		/// so that the components controlling it are able to configure the ItemRenderers used
		/// to display the contents of the datasource.
		/// 
		/// However, as unity uses prefabs and we can't just instantiate a script we need to hold
		/// an array of the prefb'd item renderers. 
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		protected virtual ItemRenderer ItemRendererFunction(ValueObject item)
        {
			return null;
        }

		/// <summary>
		/// SearchItemRenderersForType
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		protected ItemRenderer SearchItemRenderersForType(Type type)
        {
            foreach (var itemRenderer in itemRenderers)
			{
				if (itemRenderer.GetType() == type)
					return itemRenderer;
            }

            return null;
        }

		/// <summary>
		/// ItemSelected
		/// </summary>
		/// <param name="itemRenderer"></param>
		protected virtual void ItemSelected(ItemRenderer itemRenderer)
        {
            // Template method
        }

        #endregion

        #region Dispatch Event

        protected void DispatchNavigateBack()
        {
            if (OnNavigateBack != null)
                OnNavigateBack();
        }

        protected void DispatchNavigatePrevious()
        {
            if (OnNavigatePrevious != null)
                OnNavigatePrevious();
        }

        protected void DispatchNavigateNext()
        {
            if (OnNavigateNext != null)
                OnNavigateNext();
        }

        #endregion
    }
}