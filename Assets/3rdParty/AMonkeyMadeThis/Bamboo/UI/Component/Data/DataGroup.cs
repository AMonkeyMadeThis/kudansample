﻿using AMonkeyMadeThis.Common.Model.VO;
using AMonkeyMadeThis.Bamboo.UI.Component.Data.ItemRenderers;
using AMonkeyMadeThis.Bamboo.UI.Component.Data.Layouts;
using AMonkeyMadeThis.Bamboo.UI.Effects;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace AMonkeyMadeThis.Bamboo.UI.Component.Data
{
	/// <summary>
	/// DataGroup
	/// </summary>
	public class DataGroup : UIComponent
    {
		#region Delegates

		/// <summary>
		/// ItemRendererFunction
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		public delegate ItemRenderer ItemRendererFunctionDelegate(ValueObject item);
		/// <summary>
		/// ItemRendererFunction
		/// </summary>
		public ItemRendererFunctionDelegate ItemRendererFunction;

		#endregion

		#region Events

		/// <summary>
		/// Delegate to handle when an item is selected
		/// </summary>
		/// <param name="itemRenderer"></param>
		public delegate void ItemSelected(ItemRenderer itemRenderer);
		/// <summary>
		/// Dispatched when an item is selected
		/// </summary>
		public event ItemSelected OnItemSelected;
		/// <summary>
		/// Delegate to handle when the container is empty
		/// </summary>
		public delegate void ContainerEmpty();
		/// <summary>
		/// Dispatched when the container is empty
		/// </summary>
		public event ContainerEmpty OnContainerEmpty;

		#endregion

		#region Vars

		/// <summary>
		/// Width
		/// </summary>
		public int Width = 500;
		/// <summary>
		/// Height
		/// </summary>
		public int Height = 500;

		private ValueObject[] _dataSource;
		/// <summary>
		/// dataSource
		/// </summary>
		public ValueObject[] dataSource
        {
            get { return _dataSource; }
            set
            {
				if (DataSourceChanged(dataSource, value))
				{
					removeItemRenderers();
					_dataSource = value;
					createItemRenderers();
				}
            }
        }

		[SerializeField]
        private int _selectedIndex = -1;
		/// <summary>
		/// SelectedIndex
		/// </summary>
		public int SelectedIndex
        {
            get { return _selectedIndex; }
            set
			{
				_selectedIndex = value;
                UpdateRendererSelection();
            }
        }

        private ItemRenderer _selectedItem;
		/// <summary>
		/// SelectedIndex
		/// </summary>
		public ItemRenderer SelectedItem
        {
            get { return (SelectedIndex > -1 && SelectedIndex < ItemRenderers.Count) ? ItemRenderers[SelectedIndex] : null; }
            set
			{
				SelectedIndex = ItemRenderers.IndexOf(value);
                UpdateRendererSelection();
            }
        }

        private LayoutType _layout = LayoutType.Grid;
		/// <summary>
		/// LayoutType
		/// </summary>
		public LayoutType Layout
        {
            get { return _layout; }
            set
			{
				_layout = value;
                updateLayouts();
            }
        }

		/// <summary>
		/// Layouts
		/// </summary>
		public Layout[] Layouts;
		/// <summary>
		/// ItemRenderers
		/// </summary>
		public List<ItemRenderer> ItemRenderers;

        #endregion

        #region Lifecycle

        protected override void CollectComponents()
		{
			base.CollectComponents();
            Layouts = GetComponents<Layout>();
        }

		#endregion

		#region Data
		
		private bool DataSourceChanged(ValueObject[] previous, ValueObject[] current)
		{
			// if either is null then we could do with changing
			if (current == null || previous == null)
				return true;
			// if the lengths are different then we obviously have changed
			else if (previous.Length != current.Length)
			{
				return true;
			}
			// same length and neither null ... better check the contents!
			else
			{
				int n = current.Length;

				for (int i = 0; i < n; i++)
				{
					ValueObject itemPrevious = previous[i];
					ValueObject itemCurrent = current[i];

					// it any aren't what they were previously we have changed
					if (itemPrevious != itemCurrent)
						return true;
				}

				// all the same? No change.
				return false;
			}
		}

		#endregion

		#region Listeners

		private void addListeners(ItemRenderer itemRenderer)
        {
            if (itemRenderer != null)
            {
                itemRenderer.OnSelectedChanged += HandleItemRendererOnSelectedChanged;
                itemRenderer.OnItemRendererClose += HandleItemRendererOnItemRendererClose;
            }
        }

        private void removeListeners(ItemRenderer itemRenderer)
        {
            if (itemRenderer != null)
			{
				itemRenderer.OnSelectedChanged -= HandleItemRendererOnSelectedChanged;
				itemRenderer.OnItemRendererClose -= HandleItemRendererOnItemRendererClose;
            }
        }

		#endregion

		#region Handlers

		private void HandleItemRendererOnSelectedChanged(ItemRenderer itemRenderer)
		{
			if (itemRenderer.Selected)
			{
				if (SelectedItem != itemRenderer)
				{
					SelectedItem = itemRenderer;
					dispatchOnItemSelected(itemRenderer);
				}
				else
				{
					SelectedItem = null;
				}
			}
		}

		private void HandleItemRendererOnItemRendererClose(ItemRenderer itemRenderer)
		{
			removeItemRenderer(itemRenderer, true);
		}

		#endregion

		#region Item Renderers

		private void UpdateRendererSelection()
		{
			if (ItemRenderers != null)
            {
                ItemRenderer selected = SelectedItem;

                foreach (var itemRenderer in ItemRenderers)
                {
                    itemRenderer.Selected = itemRenderer == selected;
                }
            }
        }

        public ItemRenderer GetTopItemRenderer()
		{
			int lastChildIndex = transform.childCount - 1;

            if (lastChildIndex > -1)
            {
                Transform child = transform.GetChild(lastChildIndex);

                if (child != null)
                {
                    ItemRenderer itemRenderer = child.GetComponent<ItemRenderer>();

                    if (itemRenderer != null)
                        return itemRenderer;
                }
            }

            return null;
        }

		/// <summary>
		/// GetItemRendererDisplayingItem
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		public ItemRenderer GetItemRendererDisplayingItem(ValueObject item)
		{
			if (ItemRenderers != null)
            {
                foreach (var itemRenderer in ItemRenderers)
                {
                    if (itemRenderer.Data == item)
                        return itemRenderer;
                }
            }

            return null;
        }

		/// <summary>
		/// createItemRenderers
		/// </summary>
		protected virtual void createItemRenderers()
		{
			if (dataSource != null)
            {
                if (ItemRenderers == null)
                    ItemRenderers = new List<ItemRenderer>();

                ItemRenderers.Clear();

                foreach (var item in dataSource)
                {
                    ItemRenderer itemRenderer = createItemRenderer(item);

					if (itemRenderer != null)
					{
						itemRenderer.Data = item;

						ItemRenderers.Add(itemRenderer);
						addChild(itemRenderer);
						ApplyCurrentLayout();
					}
                }
            }
        }

		/// <summary>
		/// createItemRenderer
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		protected virtual ItemRenderer createItemRenderer(ValueObject item)
		{
			ItemRenderer prototype = ItemRendererFunction(item);

            if (prototype)
            {
                ItemRenderer instance = Instantiate(prototype.gameObject).GetComponent<ItemRenderer>();
                addListeners(instance);

                return instance;
            }

            return null;
        }

		/// <summary>
		/// removeItemRenderers, tries to remove all children from this container. Make sure to use a suitable number of 'tries'
		/// to avoid infinite loop so uses 3x the length of the datasource. If this component is placed on the same object as a
		/// 'page' for example the page will be enabled/disabled which will make unity fail to remove the children as it cannot do
		/// so while the parent is toggling its enabled, however grandparent is fine.
		/// </summary>
		public void removeItemRenderers()
        {
			int tries = (dataSource != null) ? dataSource.Length * 3 : 100;

			while (transform.childCount > 0 && --tries > 0)
			{
				ItemRenderer itemRenderer = transform.GetChild(0).GetComponent<ItemRenderer>();
				removeItemRenderer(itemRenderer);
			}
        }

		/// <summary>
		/// removeItemRenderer, overload to enable dispatching a message if the last child is removed. We can't do this every time
		/// as when the previous page is cleared in the background it will push us back to the index, so we can never navigate past
		/// the index due to the index being cleared. This method allows us to differentiate a user removing the item.
		/// </summary>
		/// <param name="itemRenderer"></param>
		/// <param name="dispatchEventWhenLastRendererRemoved"></param>
		private void removeItemRenderer(ItemRenderer itemRenderer, bool dispatchEventWhenLastRendererRemoved)
		{
			removeItemRenderer(itemRenderer);

            if (dispatchEventWhenLastRendererRemoved && transform.childCount == 0)
                dispatchOnContainerEmpty();
        }

        private void removeItemRenderer(ItemRenderer itemRenderer)
		{
			if (itemRenderer != null)
            {
                if (ItemRenderers.Contains(itemRenderer))
                    ItemRenderers.Remove(itemRenderer);

                removeListeners(itemRenderer);
                itemRenderer.transform.SetParent(null);
                DestroyImmediate(itemRenderer.gameObject);
                updateLayouts();
            }
        }

        #endregion

        public void addChild(ItemRenderer itemRenderer)
		{
			int childIndex = transform.childCount - 1;

            itemRenderer.transform.SetParent(transform);
			itemRenderer.transform.localPosition = Vector3.zero;
			itemRenderer.transform.localRotation = Quaternion.Euler(Vector3.zero);
			itemRenderer.transform.localScale = Vector3.one;
			AdjustEffectIn(itemRenderer, childIndex);
		}

		#region Layouts

		/// <summary>
		/// SearchLayoutsForType
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		private Layout SearchLayoutsForType(LayoutType type)
		{
			foreach (var layout in Layouts)
            {
                if (layout.LayoutType == type)
                    return layout;
            }

            return null;
        }

		/// <summary>
		/// updateLayouts
		/// </summary>
		private void updateLayouts()
        {
            ToggleLayoutsEnabled();
            ApplyCurrentLayout();
        }

        private void ToggleLayoutsEnabled()
        {
            LayoutType selected = Layout;

            foreach (var layout in Layouts)
            {
                bool isSelected = layout.LayoutType == selected;
                layout.enabled = isSelected;
            }
        }

        private void ApplyCurrentLayout()
		{
			Layout layout = CurrentLayout;

            if (layout != null)
                layout.ApplyLayout();
        }

		/// <summary>
		/// CurrentLayout
		/// </summary>
		private Layout CurrentLayout
        {
            get
            {
                LayoutType selected = Layout;

                foreach (var layout in Layouts)
                {
                    if (layout.LayoutType == selected)
                        return layout;
                }

                return null;
            }
        }

		#endregion

		#region Effects

		/// <summary>
		/// multiply the start delay by the index to make them appear sequentially but still respect the relative delay
		/// </summary>
		/// <param name="itemRenderer"></param>
		/// <param name="index"></param>
		private void AdjustEffectIn(ItemRenderer itemRenderer, int index)
        {
            EffectIn effectIn = itemRenderer.GetComponent<EffectIn>();
            
            if (effectIn)
                effectIn.StartDelay *= index;
        }

		#endregion

		#region Event Dispatching

		/// <summary>
		/// dispatchOnItemSelected
		/// </summary>
		/// <param name="itemRenderer"></param>
		protected void dispatchOnItemSelected(ItemRenderer itemRenderer)
		{
			if (OnItemSelected != null)
                OnItemSelected(itemRenderer);
        }

		/// <summary>
		/// dispatchOnItemSelected
		/// </summary>
		protected void dispatchOnContainerEmpty()
		{
			if (OnContainerEmpty != null)
                OnContainerEmpty();
        }

        #endregion
    }
}