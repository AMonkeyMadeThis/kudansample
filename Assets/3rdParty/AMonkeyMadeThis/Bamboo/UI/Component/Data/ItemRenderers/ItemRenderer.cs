﻿using AMonkeyMadeThis.Bamboo.UI.Effects;
using AMonkeyMadeThis.Common.Model.VO;
using System;

namespace AMonkeyMadeThis.Bamboo.UI.Component.Data.ItemRenderers
{
	/// <summary>
	/// ItemRenderer
	/// </summary>
	public abstract class ItemRenderer : UIComponent
	{
		#region Events

		/// <summary>
		/// Delegate to handle a change in data
		/// </summary>
		/// <param name="renderer"></param>
		public delegate void DataChanged(ItemRenderer renderer);
		/// <summary>
		/// Dispatched when data changes
		/// </summary>
		public event DataChanged OnDataChanged;
		/// <summary>
		/// Delegate to handle a change in selected
		/// </summary>
		/// <param name="renderer"></param>
		public delegate void SelectedChanged(ItemRenderer renderer);
		/// <summary>
		/// Dispatched when selected changes
		/// </summary>
		public event SelectedChanged OnSelectedChanged;
		/// <summary>
		/// ItemRendererCloseAction
		/// </summary>
		/// <param name="itemRenderer"></param>
		public delegate void ItemRendererCloseAction(ItemRenderer itemRenderer);
		/// <summary>
		/// OnItemRendererClose
		/// </summary>
		public event ItemRendererCloseAction OnItemRendererClose;

		#endregion

		#region Properties

		ValueObject _data;
		/// <summary>
		/// Data
		/// </summary>
		public ValueObject Data
		{
			get { return _data; }
			set
			{
				SetData(value);
			}
		}
		
		bool _selected;
		/// <summary>
		/// Selected
		/// </summary>
		public bool Selected
        {
            get { return _selected; }
            set
			{
				SetSelected(value);
            }
        }

		#endregion

		#region Mutators

		private void SetData(ValueObject value)
		{
			_data = value;
			DispatchOnDataChanged();
			Invalidate();
		}

		private void SetSelected(Boolean value)
		{
			_selected = value;
			DispatchOnSelectedChanged();
		}

		public void Select()
		{
			Selected = true;
		}

		#endregion

		#region Lifecycle
		
		protected override void CommitProperties()
		{
			base.CommitProperties();

			PopulateUI();
		}

		protected virtual void PopulateUI()
		{
			//template method
		}

		#endregion
		
		#region Fade out

		public void Close()
		{
			// Get EffectOut by type, but cast as AbstractEffect to access its events.
			// Just getting AbstractEffect would get both EffectIn and EffectOut
			AbstractEffect effectOut = (AbstractEffect)GetComponent<EffectOut>();

            if (effectOut != null)
            {
				effectOut.OnEffectEnd += HandleOnEffectEnd;
				effectOut.Animate();
            }
            else
            {
                dispatchOnItemRendererClose();
            }
        }

        protected virtual void HandleOnEffectEnd(AbstractEffect effect)
        {
			effect.OnEffectEnd -= HandleOnEffectEnd;

            dispatchOnItemRendererClose();
        }

		#endregion

		#region Dispatchers

		private void DispatchOnSelectedChanged()
		{
			if (OnSelectedChanged != null)
				OnSelectedChanged(this);
		}

		private void DispatchOnDataChanged()
		{
			if (OnDataChanged != null)
				OnDataChanged(this);
		}

		private void dispatchOnItemRendererClose()
        {
            if (OnItemRendererClose != null)
                OnItemRendererClose(this);
        }
		
		#endregion
    }
}
