﻿using UnityEngine;

namespace AMonkeyMadeThis.Bamboo.UI.Component.Data.Layouts
{
	/// <summary>
	/// GridLayout
	/// </summary>
	public class GridLayout : Layout
    {
		/// <summary>
		/// Columns
		/// </summary>
		public int Columns = 5;
		/// <summary>
		/// Rows
		/// </summary>
		public int Rows = 3;
		/// <summary>
		/// Scale
		/// </summary>
		public float Scale = .75f;

		/// <summary>
		/// AlignChild
		/// </summary>
		/// <param name="child"></param>
		/// <param name="index"></param>
		protected override void AlignChild(Transform child, int index)
        {
            int column = index % Columns;
            int row = Mathf.FloorToInt(index / Columns);
            Vector3 position = new Vector3();
            float columnWidth = (2 * DataGroup.Width * Scale) / (Columns - 1);
            float rowHeight = (DataGroup.Height * Scale) / (Rows - 1);
            position.x = (-(DataGroup.Width * Scale) / 1) + (columnWidth * column);
            position.y = ((DataGroup.Height * Scale) / 2) - (rowHeight * row);

            child.localScale = Vector3.one;
            child.localRotation = Quaternion.Euler(0f, 0f, 0f);
            child.localPosition = position;
        }
    }
}
