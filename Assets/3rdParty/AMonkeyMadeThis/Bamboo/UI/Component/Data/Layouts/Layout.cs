﻿using AMonkeyMadeThis.Bamboo.UI.Effects;
using UnityEngine;

namespace AMonkeyMadeThis.Bamboo.UI.Component.Data.Layouts
{
	/// <summary>
	/// Layout
	/// </summary>
	[RequireComponent(typeof(DataGroup))]
    public abstract class Layout : UIComponent
    {
		/// <summary>
		/// LayoutType
		/// </summary>
		public LayoutType LayoutType = LayoutType.Random;
		/// <summary>
		/// UpdateMode
		/// </summary>
		public UpdateMode UpdateMode = UpdateMode.Manual;
		/// <summary>
		/// DataGroup
		/// </summary>
		protected DataGroup DataGroup;

		/// <summary>
		/// CollectComponents
		/// </summary>
		protected override void CollectComponents()
        {
			base.CollectComponents();

            DataGroup = GetComponent<DataGroup>();
        }

		/// <summary>
		/// Update
		/// </summary>
		public void Update()
        {
            if (UpdateMode == UpdateMode.Update)
                ApplyLayout();
        }

		/// <summary>
		/// LateUpdate
		/// </summary>
		public void LateUpdate()
        {
            if (UpdateMode == UpdateMode.LateUpdate)
                ApplyLayout();
        }

		/// <summary>
		/// FixedUpdate
		/// </summary>
		public void FixedUpdate()
        {
            if (UpdateMode == UpdateMode.FixedUpdate)
                ApplyLayout();
        }

		/// <summary>
		/// ApplyLayout
		/// </summary>
		public virtual void ApplyLayout()
        {
            int numChildren = transform.childCount;

            if (numChildren > 0)
            {
                for (int i = 0; i < numChildren; i++)
                {
                    Transform child = transform.GetChild(i);

                    if (child)
                        AlignChild(child, i);
                }
            }   
        }

        protected virtual void AlignChild(Transform child, int childIndex)
        {
            child.localScale = Vector3.one;
            child.localRotation = Quaternion.Euler(Vector3.zero);
            child.localPosition = Vector3.zero;
        }
    }
}
