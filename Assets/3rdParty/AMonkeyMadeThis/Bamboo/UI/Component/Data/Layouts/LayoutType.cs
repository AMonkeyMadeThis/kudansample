﻿namespace AMonkeyMadeThis.Bamboo.UI.Component.Data.Layouts
{
	/// <summary>
	/// Inbuilt layouts don't play nice with touchy feely input and can't be switched at runtime so ...
	/// </summary>
	public enum LayoutType
    {
        Random,
        Grid,
        Stack,
        StackTL,
        StackTR,
        StackBL,
        StackBR,
        Vertical,
        Horizontal,
        Ring
    }
}
