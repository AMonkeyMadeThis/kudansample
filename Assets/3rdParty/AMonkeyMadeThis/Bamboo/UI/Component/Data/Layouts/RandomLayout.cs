﻿using UnityEngine;

namespace AMonkeyMadeThis.Bamboo.UI.Component.Data.Layouts
{
	/// <summary>
	/// RandomLayout
	/// </summary>
	public class RandomLayout : Layout
    {
		/// <summary>
		/// AlignChild
		/// </summary>
		/// <param name="child"></param>
		/// <param name="index"></param>
		protected override void AlignChild(Transform child, int index)
        {
            child.localScale = Vector3.one;
            child.localRotation = Quaternion.Euler(0f, 0f, Random.Range(0f, 360f));
            child.localPosition = new Vector3(Random.Range(-DataGroup.Width / 4, DataGroup.Width / 4), Random.Range(-DataGroup.Height / 2, DataGroup.Height / 2), 0);
        }
    }
}
