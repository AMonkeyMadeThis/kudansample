﻿using AMonkeyMadeThis.Common.Controller;
using System.Collections;

namespace AMonkeyMadeThis.Bamboo.UI.Component
{
	/// <summary>
	/// DataController
	/// </summary>
	public class DataController : AbstractController
	{
		#region Events

		/// <summary>
		/// DataSourceChangedAction
		/// </summary>
		public delegate void DataSourceChangedAction();
		/// <summary>
		/// OnDataSourceChanged
		/// </summary>
		public event DataSourceChangedAction OnDataSourceChanged;
		/// <summary>
		/// SelectedIndexChangedAction
		/// </summary>
		public delegate void SelectedIndexChangedAction();
		/// <summary>
		/// OnSelectedIndexChanged
		/// </summary>
		public event SelectedIndexChangedAction OnSelectedIndexChanged;
		/// <summary>
		/// SelectedItemChangedAction
		/// </summary>
		public delegate void SelectedItemChangedAction();
		/// <summary>
		/// OnSelectedItemChanged
		/// </summary>
		public event SelectedIndexChangedAction OnSelectedItemChanged;

		#endregion

		#region Member Vars
		
		private IList _datasource;
		/// <summary>
		/// Datasource
		/// </summary>
		public virtual IList Datasource
		{
			get { return _datasource; }
			set
			{
				if (_datasource != value)
				{
					_datasource = value;
					DispatchOnDataSourceChanged();
					Reset();
				}
			}
		}

		private int _selectedIndex = -1;
		/// <summary>
		/// SelectedIndex
		/// </summary>
		public virtual int SelectedIndex
		{
			get { return _selectedIndex; }
			set
			{
				if (_selectedIndex != value)
				{
					_selectedIndex = value;
					DispatchOnSelectedIndexChanged();
					UpdateSelectedItem();
				}
			}
		}

		private object _selectedItem;
		/// <summary>
		/// SelectedItem
		/// </summary>
		public virtual object SelectedItem
		{
			get { return _selectedItem; }
			set
			{
				if (_selectedItem != value)
				{
					_selectedItem = value;
					DispatchOnSelectedItemChanged();
					UpdateSelectedIndex();
				}
			}
		}

		#endregion

		#region Datasource sync methods

		/// <summary>
		/// Reset
		/// </summary>
		protected virtual void Reset()
		{
			SelectedIndex = -1;
			SelectedItem = null;
		}

		/// <summary>
		/// UpdateSelectedItem
		/// </summary>
		private void UpdateSelectedItem()
		{
			object item = (SelectedIndex>-1 && SelectedIndex<Datasource.Count) ? Datasource[SelectedIndex] : null;
			SelectedItem = item;
		}

		/// <summary>
		/// UpdateSelectedIndex
		/// </summary>
		protected virtual void UpdateSelectedIndex()
		{
			int index = Datasource.IndexOf(SelectedItem);
			SelectedIndex = index;
		}

		#endregion

		#region Event Dispatching

		/// <summary>
		/// DispatchOnDataSourceChanged
		/// </summary>
		private void DispatchOnDataSourceChanged()
		{
			if (OnDataSourceChanged != null)
				OnDataSourceChanged();
		}

		/// <summary>
		/// DispatchOnSelectedIndexChanged
		/// </summary>
		private void DispatchOnSelectedIndexChanged()
		{
			if (OnSelectedIndexChanged != null)
				OnSelectedIndexChanged();
		}

		/// <summary>
		/// DispatchOnSelectedItemChanged
		/// </summary>
		private void DispatchOnSelectedItemChanged()
		{
			if (OnSelectedItemChanged != null)
				OnSelectedItemChanged();
		}

		#endregion
	}
}
