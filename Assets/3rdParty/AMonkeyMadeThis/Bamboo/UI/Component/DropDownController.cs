﻿using AMonkeyMadeThis.Common.Util;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace AMonkeyMadeThis.Bamboo.UI.Component
{
	/// <summary>
	/// DropDownController
	/// </summary>
	[RequireComponent(typeof(Dropdown))]
	public class DropDownController : DataController
	{
		#region Member vars

		/// <summary>
		/// Dropdown
		/// </summary>
		public Dropdown Dropdown;
		/// <summary>
		/// LabelField, the field to be used as a label when adding the contents of the datasource to the dropdown's options. If empty then object.ToString() will be used instead.
		/// </summary>
		public string LabelField;
		/// <summary>
		/// HasLabelField
		/// </summary>
		public bool HasLabelField
		{
			get { return !string.IsNullOrEmpty(LabelField); }
		}
		/// <summary>
		/// ImageField, the field to be used as an image when adding the contents of the datasource to the dropdown's options.
		/// </summary>
		public string ImageField;
		/// <summary>
		/// HasImageField
		/// </summary>
		public bool HasImageField
		{
			get { return !string.IsNullOrEmpty(ImageField); }
		}

		#endregion

		#region Initialisation

		/// <summary>
		/// Awake
		/// </summary>
		public override void Awake()
		{
			base.Awake();

			PopulateDropdown();
			MakeInitialSelection();
		}

		/// <summary>
		/// CollectComponents
		/// </summary>
		protected override void CollectComponents()
		{
			base.CollectComponents();

			Dropdown = GetComponent<Dropdown>();
		}

		/// <summary>
		/// Reset
		/// </summary>
		protected override void Reset()
		{
			base.Reset();

			PopulateDropdown();
		}

		#endregion

		#region Listeners

		/// <summary>
		/// AddListeners
		/// </summary>
		public override void AddListeners()
		{
			base.AddListeners();
			
			Dropdown.onValueChanged.AddListener(delegate { HandleDropDownValueChanged(); });
		}

		#endregion

		#region Handlers

		/// <summary>
		/// HandleDropDownValueChanged
		/// </summary>
		protected virtual void HandleDropDownValueChanged()
		{
			int index = Dropdown.value;
			SelectedIndex = index;
		}

		#endregion

		#region Population

		/// <summary>
		/// PopulateDropdown
		/// </summary>
		protected virtual void PopulateDropdown()
		{
			if (Dropdown != null && Datasource != null)
			{
				List<Dropdown.OptionData> options = new List<Dropdown.OptionData>();

				foreach (var item in Datasource)
				{
					Dropdown.OptionData option = new Dropdown.OptionData();
					option.text = GetItemLabel(item);
					option.image = GetItemImage(item);

					options.Add(option);
				}

				Dropdown.ClearOptions();
				Dropdown.AddOptions(options);
			}
		}

		/// <summary>
		/// GetItemLabel
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		protected virtual string GetItemLabel(object item)
		{
			if (!HasLabelField || !ObjectUtils.HasOwnProperty(item, LabelField))
				return item.ToString();

			object value = ObjectUtils.GetProperty(item, LabelField);
			string result = value.ToString();

			return result;
		}

		/// <summary>
		/// GetItemImage
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		protected virtual Sprite GetItemImage(object item)
		{
			if (!HasImageField || !ObjectUtils.HasOwnProperty(item, ImageField))
				return null;

			object value = ObjectUtils.GetProperty(item, ImageField);
			Sprite result = value as Sprite;

			return result;
		}

		#endregion

		#region Selection

		protected virtual void MakeInitialSelection()
		{
			// template method
		}

		protected override void UpdateSelectedIndex()
		{
			base.UpdateSelectedIndex();

			Dropdown.value = SelectedIndex;
		}

		#endregion
	}
}
