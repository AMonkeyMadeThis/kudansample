﻿using AMonkeyMadeThis.Bamboo.UI.Component.Content;
using UnityEngine;
using UnityEngine.UI;

namespace AMonkeyMadeThis.Bamboo.UI.Component.Navigation
{
	/// <summary>
	/// NavBar
	/// </summary>
	[RequireComponent(typeof(ToggleGroup))]
	public class NavBar : UIComponent
	{
		/// <summary>
		/// ButtonPrototype
		/// </summary>
		public NavBarButton ButtonPrototype;

		private ViewStack _viewStack;
		/// <summary>
		/// ViewStack
		/// </summary>
		public ViewStack ViewStack
		{
			get { return _viewStack; }
			set
			{
				_viewStack = value;
				UpdateChildren();
			}
		}
		
		[SerializeField]
		private int _selectedIndex = -1;
		/// <summary>
		/// SelectedIndex
		/// </summary>
		public int SelectedIndex
		{
			get { return _selectedIndex; }
			set
			{
				_selectedIndex = value;
				UpdateChildren();
			}
		}

		/// <summary>
		/// ToggleGroup
		/// </summary>
		private ToggleGroup ToggleGroup;

		protected override void CollectComponents()
		{
			base.CollectComponents();

			ToggleGroup = GetComponent<ToggleGroup>();
		}

		private void UpdateChildren()
		{
			if (ViewStack != null && ViewStack.Views != null && ViewStack.Views.Length > 0)
				CreateViews(ViewStack.Views);
		}

		private void CreateViews(NavigatorContent[] views)
		{
			if (ButtonPrototype != null)
			{
				int n = views.Length;

				for (int i = 0; i < n; i++)
				{
					NavigatorContent view = views[i];

					if (view != null && view.ShowInMenu)
					{
						NavBarButton instance = Instantiate(ButtonPrototype) as NavBarButton;
						instance.Index = i;
						instance.View = view;
						instance.Text = view.Label;
						instance.Selected = i == SelectedIndex;

						InitToggle(instance);
						InitTransform(instance);
					}
				}
			}
		}

		private void InitToggle(NavBarButton instance)
		{
			Toggle toggle = instance.GetComponent<Toggle>();

			if (toggle != null)
			{
				toggle.group = ToggleGroup;
				toggle.onValueChanged.AddListener(delegate { HandleToggleChanged(instance); });
			}
		}
		
		private void HandleToggleChanged(NavBarButton instance)
		{
			ViewStack.GoToLabel(instance.Text);
		}

		private void InitTransform(NavBarButton instance)
		{
			instance.transform.parent = this.transform;
			instance.transform.localPosition = Vector3.zero;
			instance.transform.localRotation = Quaternion.Euler(Vector3.zero);
			instance.transform.localScale = Vector3.one;
		}
	}
}
