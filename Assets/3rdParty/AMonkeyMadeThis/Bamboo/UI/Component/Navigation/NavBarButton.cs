﻿using AMonkeyMadeThis.Bamboo.UI.Component.Content;
using UnityEngine;
using UnityEngine.UI;

namespace AMonkeyMadeThis.Bamboo.UI.Component.Navigation
{
	/// <summary>
	/// NavBarButton
	/// </summary>
	public class NavBarButton : UIComponent
	{
		private int _index;
		/// <summary>
		/// Index
		/// </summary>
		public int Index
		{
			get { return _index; }
			set
			{
				_index = value;
				InvalidateProperties();
			}
		}

		private NavigatorContent _view;
		/// <summary>
		/// View
		/// </summary>
		public NavigatorContent View
		{
			get { return _view; }
			set
			{
				_view = value;
				InvalidateProperties();
			}
		}

		private string _text;
		/// <summary>
		/// Text
		/// </summary>
		public string Text
		{
			get { return _text; }
			set
			{
				_text = value;
				InvalidateProperties();
			}
		}

		[SerializeField]
		private bool _selected;
		/// <summary>
		/// Selected
		/// </summary>
		public bool Selected
		{
			get { return _selected; }
			set
			{
				_selected = value;
				InvalidateProperties();
			}
		}

		/// <summary>
		/// Label
		/// </summary>
		protected Text Label;
		/// <summary>
		/// Toggle
		/// </summary>
		protected Toggle Toggle;

		protected override void CollectComponents()
		{
			Label = GetComponent<Text>();
			Toggle = GetComponent<Toggle>();
		}

		public override void AddListeners()
		{
			base.AddListeners();

			AddToggleListeners();
		}

		private void AddToggleListeners()
		{
			if (Toggle != null)
				Toggle.onValueChanged.AddListener(delegate { Selected = Toggle.isOn; InvalidateProperties(); });
		}
		
		protected override void CommitProperties()
		{
			SetLabelText();
			SetToggleSelected();

			base.CommitProperties();
		}

		private void SetToggleSelected()
		{
			if (Toggle != null)
			{
				Toggle.isOn = Selected;
				Toggle.targetGraphic.color = (Toggle.isOn) ? Toggle.colors.highlightedColor : Toggle.colors.normalColor;
			}
		}

		protected virtual void SetLabelText()
		{
			if (Label != null)
				Label.text = Text;
		}
	}
}
