﻿using AMonkeyMadeThis.Bamboo.UI.Component.Content;
using System;
using UnityEngine;

namespace AMonkeyMadeThis.Bamboo.UI.Component.Navigation
{
	/// <summary>
	/// ViewStack
	/// </summary>
	public class ViewStack : UIComponent
	{
		/// <summary>
		/// Views
		/// </summary>
		public NavigatorContent[] Views;
		/// <summary>
		/// Navigator
		/// </summary>
		public NavBar Navigator;

		[SerializeField]
		private int _selectedIndex = -1;
		/// <summary>
		/// SelectedIndex
		/// </summary>
		public int SelectedIndex
		{
			get { return _selectedIndex; }
			set
			{
				_selectedIndex = value;
				updateChildren();
			}
		}

		private NavigatorContent _selectedItem;
		/// <summary>
		/// SelectedIndex
		/// </summary>
		public NavigatorContent SelectedItem
		{
			get { return (SelectedIndex<Views.Length) ? Views[SelectedIndex] : null; }
			set
			{
				SelectedIndex = Array.IndexOf(Views, value);
			}
		}
		
		public override void Awake()
		{
            base.Awake();

			SelectedIndex = 0;
			UpdateNavigator();
		}

		private void UpdateNavigator()
		{
			if (Navigator != null)
				Navigator.ViewStack = this;
		}

		protected override void CollectComponents()
		{
			base.CollectComponents();

			Views = GetComponentsInChildren<NavigatorContent>();
		}

		private void updateChildren()
		{
			NavigatorContent selected = SelectedItem;

			foreach (var view in Views)
			{
				bool isSelectedItem = view == selected;
				view.gameObject.SetActive(isSelectedItem);
			}
		}

		public void GoToLabel(string label)
		{
			NavigatorContent view = GetViewByLabel(label);

			if (view != null)
				SelectedItem = view;
		}

		public NavigatorContent GetViewByLabel(string label)
		{
			foreach (var view in Views)
			{
				if (view.Label == label)
					return view;
			}

			return null;
		}
	}
}
