﻿using AMonkeyMadeThis.Common.Localisation;

namespace AMonkeyMadeThis.Bamboo.UI.Component
{
	/// <summary>
	/// UIComponent
	/// </summary>
	public abstract class UIComponent : LocalisedBehaviour
	{
		//
	}
}
