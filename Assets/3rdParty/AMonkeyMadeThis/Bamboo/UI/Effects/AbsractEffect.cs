﻿using System.Collections;
using UnityEngine;

namespace AMonkeyMadeThis.Bamboo.UI.Effects
{
	public abstract class AbstractEffect : MonoBehaviour
	{
		#region Events

		public delegate void EffectEndEvent(AbstractEffect effect);
		public event EffectEndEvent OnEffectEnd;

		#endregion

		public float StartDelay = 0;
		public float Duration = 1;
		public AnimationCurve Easing = AnimationCurve.Linear(0, 0, 1, 1);
		public bool Hold = false;

		protected bool isAnimating = false;
		protected float startTime;
		protected float endTime;
		bool release = false;

		public virtual void OnEnable()
		{
			CollectComponents();
		}

		protected virtual void CollectComponents()
		{
			// Template method
		}

		public virtual void Animate()
		{
			SetTimings();
			release = false;
			if (!isAnimating)
				StartCoroutine(AnimationThread());
		}

		public virtual void Release()
		{
			release = true;
		}

		private void SetTimings()
		{
			startTime = Time.time + StartDelay;
			endTime = startTime + Duration;
		}

		protected virtual IEnumerator AnimationThread()
		{
			isAnimating = true;

			if (StartDelay > 0)
			{
				UpdateAnimation(0);
				yield return new WaitForSeconds(StartDelay);
			}

			while (Time.time < endTime)
			{
				float currentTime = Time.time;
				float elapsedTime = currentTime - startTime;
				float progress = Mathf.Clamp01(elapsedTime / Duration);
				float easedProgress = Easing.Evaluate(progress);
				
				UpdateAnimation(easedProgress);

				yield return new WaitForEndOfFrame();
			}

			if (Hold)
			{
				while(!release)
				{
					UpdateAnimation(1);

					yield return new WaitForEndOfFrame();
				}
			}

			UpdateAnimation(1);
			isAnimating = false;
			DispatchOnEffectEnd();
		}

		protected virtual void UpdateAnimation(float progress)
		{
			// template method
		}

		#region Dispatcher

		protected void DispatchOnEffectEnd()
		{
			if (OnEffectEnd != null)
			{
				OnEffectEnd(this);
			}
		}

		#endregion
	}
}