﻿using System.Collections;
using UnityEngine;

namespace AMonkeyMadeThis.Bamboo.UI.Effects.Audio
{
	/// <summary>
	/// AudioIn
	/// </summary>
	[RequireComponent(typeof(AudioSource))]
	public class AudioIn : EffectIn
	{
		public AudioClip AudioClip;

		AudioSource audioSource;

		protected override void CollectComponents()
		{
			base.CollectComponents();

			audioSource = GetComponent<AudioSource>();
		}

		protected override IEnumerator AnimationThread()
		{
			isAnimating = true;

			if (StartDelay > 0)
			{
				UpdateAnimation(0);
				yield return new WaitForSeconds(StartDelay);
			}

			audioSource.clip = AudioClip;
			audioSource.Play();

			while (audioSource.isPlaying)
			{
				yield return new WaitForEndOfFrame();
			}

			isAnimating = false;
			DispatchOnEffectEnd();
		}
	}
}
