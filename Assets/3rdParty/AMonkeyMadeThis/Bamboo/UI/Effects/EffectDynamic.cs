﻿using System.Collections;
using UnityEngine;

namespace AMonkeyMadeThis.Bamboo.UI.Effects
{
	/// <summary>
	/// EffectDynamic
	/// </summary>
	public class EffectDynamic : MonoBehaviour
    {
		#region Events

		/// <summary>
		/// EffectEndAction
		/// </summary>
		public delegate void EffectEndAction();
		/// <summary>
		/// OnEffectEnd
		/// </summary>
		public event EffectEndAction OnEffectEnd;

		#endregion

		/// <summary>
		/// StartPosition
		/// </summary>
		public Vector3 StartPosition;
		/// <summary>
		/// StartRotation
		/// </summary>
		public Vector3 StartRotation;
		/// <summary>
		/// StartScale
		/// </summary>
		public Vector3 StartScale;
		/// <summary>
		/// EndPosition
		/// </summary>
		public Vector3 EndPosition;
		/// <summary>
		/// EndRotation
		/// </summary>
		public Vector3 EndRotation;
		/// <summary>
		/// EndScale
		/// </summary>
		public Vector3 EndScale;

		/// <summary>
		/// StartDelay
		/// </summary>
		public float StartDelay = 0f;
		/// <summary>
		/// Duration
		/// </summary>
		public float Duration = 0f;

		/// <summary>
		/// Play
		/// </summary>
		public void Play()
        {
            StartCoroutine("Animate");
        }
        
        private IEnumerator Animate()
        {
            UpdateTransform(StartPosition, StartRotation, StartScale);

            yield return new WaitForSeconds(StartDelay);

            double startTime = Time.realtimeSinceStartup;
            double endTime = startTime + Duration;

            while (Time.realtimeSinceStartup < endTime)
            {
                float progress = (float)((Time.realtimeSinceStartup-startTime) / Duration);

                Vector3 position = Vector3.Lerp(StartPosition, EndPosition, progress);
                Vector3 rotation = Vector3.Lerp(StartRotation, EndRotation, progress);
                Vector3 scale = Vector3.Lerp(StartScale, EndScale, progress);
                
                UpdateTransform(position, rotation, scale);

                yield return new WaitForEndOfFrame();
            }

            UpdateTransform(EndPosition, EndRotation, EndScale);

            DispatchEffectEnd();
        }

        private void UpdateTransform(Vector3 position, Vector3 rotation, Vector3 scale)
        {
            // position
            transform.localPosition = position;
            // rotation
            Quaternion _rotation = new Quaternion();
            _rotation.eulerAngles = rotation;
            transform.localRotation = _rotation;
            // scale
            transform.localScale = scale;
        }

        /**
         * DispatchEffectEnd
         */
        protected void DispatchEffectEnd()
        {
            if (OnEffectEnd != null)
                OnEffectEnd();
        }
    }
}
