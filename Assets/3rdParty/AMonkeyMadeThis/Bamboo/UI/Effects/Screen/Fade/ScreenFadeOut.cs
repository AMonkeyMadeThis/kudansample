﻿using UnityEngine;

namespace AMonkeyMadeThis.Bamboo.UI.Effects.Screen.Fade
{
	/// <summary>
	/// ScreenFadeOut
	/// </summary>
	public class ScreenFadeOut : EffectOut
	{
		public Color fadeColor = new Color(0.01f, 0.01f, 0.01f, 1.0f);

		public Material fadeMaterial;
		
		protected override void UpdateAnimation(float progress)
		{
			Color color = fadeColor;
			color.a = Mathf.Clamp01(progress);
			fadeMaterial.color = color;
			fadeMaterial.SetPass(0);
			GL.PushMatrix();
			GL.LoadOrtho();
			GL.Color(fadeMaterial.color);
			GL.Begin(GL.QUADS);
			GL.Vertex3(0f, 0f, 0f);// -12f);
			GL.Vertex3(0f, 1f, 0f);//-12f);
			GL.Vertex3(1f, 1f, 0f);//-12f);
			GL.Vertex3(1f, 0f, 0f);//-12f);
			GL.End();
			GL.PopMatrix();
		}
	}
}
