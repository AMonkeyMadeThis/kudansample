﻿using UnityEngine;

namespace AMonkeyMadeThis.Bamboo.UI.Effects.Transform.Move
{
	/// <summary>
	/// MoveIn
	/// </summary>
	public class MoveIn : EffectIn
	{
		public Vector3 StartPosition;
		public Vector3 EndPosition;

		protected override void UpdateAnimation(float progress)
		{
			Vector3 delta = EndPosition - StartPosition;
			Vector3 current = StartPosition + (delta * progress);
			this.transform.position = current;
		}
	}
}
