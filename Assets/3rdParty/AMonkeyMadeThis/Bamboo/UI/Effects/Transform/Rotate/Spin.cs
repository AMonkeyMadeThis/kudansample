﻿using UnityEngine;

namespace AMonkeyMadeThis.Bamboo.UI.Effects.Transform.Rotate
{
	/// <summary>
	/// Spin
	/// </summary>
	public class Spin : MonoBehaviour
	{
		/// <summary>
		/// Rotation speed in RPM
		/// </summary>
		public Vector3 AngularVelocity;

		void Update()
		{
			transform.Rotate(AngularVelocity / 60 * Time.deltaTime);
		}
	}
}