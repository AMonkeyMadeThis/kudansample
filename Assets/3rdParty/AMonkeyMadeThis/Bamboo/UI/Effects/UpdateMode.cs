﻿using System;

namespace AMonkeyMadeThis.Bamboo.UI.Effects
{
	/// <summary>
	/// UpdateMode
	/// </summary>
    [Flags]
    public enum UpdateMode
    {
        Manual,
        Update,
        FixedUpdate,
        LateUpdate
    }
}
