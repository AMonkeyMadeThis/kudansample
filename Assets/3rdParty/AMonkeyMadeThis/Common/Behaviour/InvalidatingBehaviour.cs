﻿using AMonkeyMadeThis.Common.Interface;
using UnityEngine;

namespace AMonkeyMadeThis.Common.Behaviour
{
	/// <summary>
	/// An invalidating behaviour to provide the basis for 'lazier' more async behaviours
	/// </summary>
	public abstract class InvalidatingBehaviour : StructuredBehaviour, IInvalidating
	{
		#region Vars

		/// <summary>
		/// IsValid
		/// </summary>
		bool IsValid = true;
		/// <summary>
		/// propertiesDirty
		/// </summary>
		bool propertiesDirty = false;

		#endregion

		#region Lifecycle

		/// <summary>
		/// OnEnable
		/// </summary>
		public override void OnEnable()
		{
			base.OnEnable();
			
			Invalidate();
		}
		
		/// <summary>
		/// Start
		/// </summary>
		public override void Start()
		{
			base.Start();

			Invalidate();
		}
		
		#endregion
		
		#region Invalidation

		/// <summary>
		/// Mark the behaviour as invalid so that it updates
		/// </summary>
		public void Invalidate()
		{
			if (IsValid)
			{
				propertiesDirty = true;
				IsValid = CallLater(Validate);
			}
		}

		/// <summary>
		/// Invalidate the behaviours properties so that it updates
		/// </summary>
		public virtual void InvalidateProperties()
		{
			propertiesDirty = true;
			Invalidate();
		}

		/// <summary>
		/// CommitProperties
		/// </summary>
		protected virtual void CommitProperties()
		{
			propertiesDirty = false;
		}

		/// <summary>
		/// Validate, part of the "Invalidate > Commit > Validate" cycle
		/// </summary>
		public virtual void Validate()
		{
			if (propertiesDirty)
				CommitProperties();
			// we are clean again
			IsValid = true;
		}

		/// <summary>
		/// OnValidate called when a field is changed in the UI, make us invalidate so the rest of the process runs
		/// </summary>
		public virtual void OnValidate()
		{
			//Invalidate();
		}

		#endregion
	}
}
