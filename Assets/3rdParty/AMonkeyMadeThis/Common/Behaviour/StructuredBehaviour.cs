﻿using System;
using System.Collections;
using UnityEngine;

namespace AMonkeyMadeThis.Common.Behaviour
{
	/// <summary>
	/// StructuredBehaviour
	/// </summary>
	public abstract class StructuredBehaviour : MonoBehaviour
	{
		#region Lifecycle

		/// <summary>
		/// OnEnable
		/// </summary>
		public virtual void OnEnable()
		{
			InitVars();
			AddListeners();
		}

		/// <summary>
		/// InitVars
		/// </summary>
		protected virtual void InitVars()
		{
			// Template method
		}

		/// <summary>
		/// Awake
		/// </summary>
		public virtual void Awake()
		{
			CollectComponents();
		}

		/// <summary>
		/// CollectComponents
		/// </summary>
		protected virtual void CollectComponents()
		{
			// Template method
		}

		/// <summary>
		/// Start
		/// </summary>
		public virtual void Start()
		{
			// Template method
		}

		/// <summary>
		/// OnDisable
		/// </summary>
		public virtual void OnDisable()
		{
			RemoveListeners();
		}

		/// <summary>
		/// OnDestroy
		/// </summary>
		public virtual void OnDestroy()
		{
			// Stop any outstanding invokes
			CancelInvoke();
			// Remove listeners to stop us loitering
			RemoveListeners();
		}

		#endregion

		#region Listeners

		/// <summary>
		/// AddListeners
		/// </summary>
		public virtual void AddListeners()
		{
			// Template method
		}

		/// <summary>
		/// RemoveListeners
		/// </summary>
		public virtual void RemoveListeners()
		{
			// Template method
		}

		#endregion

		#region Async

		protected bool CallLater(Action action)
		{
			if (isActiveAndEnabled)
			{
				StartCoroutine(DelayedCall(action, .125f));
				return true;
			}

			return false;
		}

		private IEnumerator DelayedCall(Action action, float delay)
		{
			yield return new WaitForSecondsRealtime(delay);
			action.Invoke();
		}

		#endregion
	}
}
