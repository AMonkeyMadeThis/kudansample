﻿using AMonkeyMadeThis.Common.Behaviour;

namespace AMonkeyMadeThis.Common.Controller.Behaviours
{
	/// <summary>
	/// DoNotDestroy
	/// </summary>
	public class DoNotDestroy : StructuredBehaviour
	{
		public override void Awake()
		{
			base.Awake();

			DontDestroyOnLoad(this.gameObject);
		}
	}
}
