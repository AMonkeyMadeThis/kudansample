﻿using AMonkeyMadeThis.Common.Behaviour;
using UnityEngine;

namespace AMonkeyMadeThis.Common.Controller.Behaviours
{
	/// <summary>
	/// NeverSleep
	/// </summary>
	public class NeverSleep : StructuredBehaviour
	{
		public override void Awake()
		{
			base.Awake();

			Screen.sleepTimeout = SleepTimeout.NeverSleep;
		}
	}
}
