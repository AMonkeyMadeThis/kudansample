﻿using AMonkeyMadeThis.Common.Behaviour;
using UnityEngine;

namespace AMonkeyMadeThis.Common.Controller.Behaviours
{
	/// <summary>
	/// TargetFrameRate
	/// </summary>
	public class TargetFrameRate : StructuredBehaviour
	{
		/// <summary>
		/// FrameRate
		/// </summary>
		public int FrameRate = 30;

		public override void Awake()
		{
			base.Awake();

			Application.targetFrameRate = FrameRate;
		}
	}
}
