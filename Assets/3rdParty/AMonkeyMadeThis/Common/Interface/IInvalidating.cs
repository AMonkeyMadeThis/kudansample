﻿namespace AMonkeyMadeThis.Common.Interface
{
	/// <summary>
	/// IInvalidating
	/// </summary>
	public interface IInvalidating
	{
		/// <summary>
		/// Invalidate this object
		/// </summary>
		void Invalidate();
	}
}
