﻿using AMonkeyMadeThis.Common.Behaviour;
using AMonkeyMadeThis.Bamboo.Localisation.Controller;
using AMonkeyMadeThis.Bamboo.Localisation.Model;
using System;
using UnityEngine;

namespace AMonkeyMadeThis.Common.Localisation
{
	/// <summary>
	/// LocalisedBehaviour
	/// </summary>
	public class LocalisedBehaviour : InvalidatingBehaviour
	{
		#region Vars

		[SerializeField]
		private string _resourceBundle = "Application";
		/// <summary>
		/// ResourceBundle
		/// </summary>
		public string ResourceBundle
		{
			get { return _resourceBundle; }
			set
			{
				SetResourceBundle(value);
			}
		}

		#endregion

		#region Mutators

		private void SetResourceBundle(String value)
		{
			UnRegisterResourceBundleUse();
			_resourceBundle = value;
			RegisterResourceBundleUse();
			Invalidate();
		}

		#endregion

		#region Lifecycle

		/// <summary>
		/// Start
		/// </summary>
		public override void Start()
		{
			base.Start();

			RegisterResourceBundleUse();
		}
		
		#endregion

		#region Localisation

		private void RegisterResourceBundleUse()
		{
			if (LocalisationController.Instance != null && ResourceBundle != null && ResourceBundle.Length > 0)
				LocalisationController.Instance.RegisterResourceBundleUse(ResourceBundle);
			else
				Debug.LogError("Error registering resource bundle usage, either 'Main' doesn't have a LocalisationController or a scene other than Main was run");
		}

		private void UnRegisterResourceBundleUse()
		{
			if (LocalisationController.Instance != null && ResourceBundle != null && ResourceBundle.Length > 0)
				LocalisationController.Instance.UnRegisterResourceBundleUse(ResourceBundle);
			else
				Debug.LogError("Error registering resource bundle usage, either 'Main' doesn't have a LocalisationController or a scene other than Main was run");
		}

		#endregion
		
		#region Listeners

		/// <summary>
		/// AddListeners
		/// </summary>
		public override void AddListeners()
		{
			base.AddListeners();

			AddListeners(LocalisationModel.Instance);
		}

		/// <summary>
		/// RemoveListeners
		/// </summary>
		public override void RemoveListeners()
		{
			base.RemoveListeners();

			RemoveListeners(LocalisationModel.Instance);
		}

		private void AddListeners(LocalisationModel localisationModel)
		{
			if (localisationModel != null)
			{
				localisationModel.OnLocaleChanged += HandleOnLocaleChanged;
			}
		}

		private void RemoveListeners(LocalisationModel localisationModel)
		{
			if (localisationModel != null)
			{
				localisationModel.OnLocaleChanged -= HandleOnLocaleChanged;
			}
		}

		#endregion

		#region Handlers

		protected virtual void HandleOnLocaleChanged()
		{
			Invalidate();
		}

		#endregion
	}
}
