﻿using System.Xml.Serialization;

namespace AMonkeyMadeThis.Common.Model.DTO
{
	/// <summary>
	/// AbstractDTO
	/// </summary>
	public abstract class AbstractDTO
	{
		/// <summary>
		/// ID
		/// </summary>
		[XmlAttribute(AttributeName = "id")]
		public string ID;
	}
}
