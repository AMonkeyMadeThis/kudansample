﻿using AMonkeyMadeThis.Common.Behaviour;
using AMonkeyMadeThis.Bamboo.Network;

namespace AMonkeyMadeThis.Common.Service
{
	/// <summary>
	/// AbstractService
	/// </summary>
	public class AbstractService : InvalidatingBehaviour
	{
		#region Senders

		/// <summary>
		/// SendResult
		/// </summary>
		/// <param name="responder"></param>
		/// <param name="result"></param>
		protected virtual void SendResult(Responder responder, object result = null)
		{
			if (responder != null && responder.Result != null)
				responder.Result(result);
		}

		/// <summary>
		/// SendFault
		/// </summary>
		/// <param name="responder"></param>
		/// <param name="fault"></param>
		protected virtual void SendFault(Responder responder, object fault = null)
		{
			if (responder != null && responder.Fault != null)
				responder.Fault(fault);
		}

		#endregion
	}
}
