﻿namespace AMonkeyMadeThis.Common.Util
{
	/// <summary>
	/// ArrayUtils
	/// </summary>
	public static class ArrayUtils
	{
		/// <summary>
		/// Add
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="target"></param>
		/// <param name="item"></param>
		/// <returns></returns>
		public static T[] Add<T>(this T[] target, T item)
		{
			if (target != null)
			{
				T[] result = new T[target.Length + 1];
				target.CopyTo(result, 0);
				result[target.Length] = item;

				return result;
			}

			return null;
		}
	}
}
