﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AMonkeyMadeThis.Common.Util
{
	/// <summary>
	/// ListUtils
	/// </summary>
	public class ListUtils
	{
		/// <summary>
		/// MakeGeneric
		/// </summary>
		/// <param name="source"></param>
		/// <returns></returns>
		public static List<object> MakeGeneric(IList source)
		{
			List<object> result = new List<object>();

			foreach (var item in source)
			{
				result.Add((object)item);
			}

			return result;
		}

		/// <summary>
		/// Gets a number of random items from a list
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="source"></param>
		/// <param name="items"></param>
		/// <returns></returns>
		public static List<T> GetRandomItems<T>(List<T> source, int items)
		{
			List<T> result = new List<T>();

			if (source != null && source.Count > items)
			{
				List<T> available = new List<T>(source);
				int tries = available.Count;

				while(available.Count > 0 && result.Count < items && tries > 0)
				{
					int n = available.Count;
					int i = Random.Range(0, n);
					T item = source[i];

					if (!result.Contains(item))
					{
						result.Add(item);
						available.Remove(item);
					}
					else
					{
						tries--;
					}
				}
			}
			else
			{
				result.AddRange(source);
			}

			return result;
		}
	}
}
