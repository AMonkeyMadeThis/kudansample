﻿using System;
using System.Reflection;

namespace AMonkeyMadeThis.Common.Util
{
	/// <summary>
	/// ObjectUtils
	/// </summary>
	public class ObjectUtils
	{
		/// <summary>
		/// HasOwnProperty
		/// </summary>
		/// <param name="target"></param>
		/// <param name="propertyName"></param>
		/// <returns></returns>
		public static bool HasOwnProperty(object target, string propertyName)
		{
			Type type = target.GetType();
			PropertyInfo property = type.GetProperty(propertyName);
			FieldInfo field = type.GetField(propertyName);
			bool result = (property != null) || (field != null);

			return result;
		}

		/// <summary>
		/// GetProperty
		/// </summary>
		/// <param name="target"></param>
		/// <param name="propertyName"></param>
		/// <returns></returns>
		public static object GetProperty(object target, string propertyName)
		{
			if (!HasOwnProperty(target, propertyName))
				return null;

			Type type = target.GetType();
			PropertyInfo property = type.GetProperty(propertyName);
			FieldInfo field = type.GetField(propertyName);

			if (property != null)
			{
				return property.GetValue(target, null);
			}
			else if (field != null)
			{
				return field.GetValue(target);
			}
			else
			{
				return null;
			}
		}
	}
}
