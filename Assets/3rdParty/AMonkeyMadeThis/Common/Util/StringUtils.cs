﻿using System.IO;
using System.Text;

namespace AMonkeyMadeThis.Util
{
	/// <summary>
	/// StringUtils
	/// </summary>
	public class StringUtils
	{
		/// <summary>
		/// ConvertStreamToDataString
		/// </summary>
		/// <param name="stream"></param>
		/// <returns></returns>
		public static string ConvertStreamToDataString(MemoryStream stream)
		{
			int length = (int)stream.Length;
			byte[] byteArray = new byte[length];

			stream.Read(byteArray, 0, length);

			string result = GetStringFromByteArray(byteArray);

			return result;
		}

		/// <summary>
		/// ConvertDataStringToStream
		/// </summary>
		/// <param name="raw"></param>
		/// <returns></returns>
		public static Stream ConvertDataStringToStream(string raw)
		{
			byte[] byteArray = GetUTF8ByteArrayFromString(raw);
			MemoryStream result = new MemoryStream(byteArray);
			
			return result;
		}

		/// <summary>
		/// GetUTF8ByteArrayFromString
		/// 
		/// Gets the UTF8 bytes from a string.
		/// </summary>
		/// <param name="raw"></param>
		/// <returns></returns>
		public static byte[] GetUTF8ByteArrayFromString(string raw)
		{
			UTF8Encoding encoding = new UTF8Encoding();
			byte[] result = encoding.GetBytes(raw);

			return result;
		}

		/// <summary>
		/// GetStringFromByteArray
		/// 
		/// Gets the UTF8 string from bytes.
		/// </summary>
		/// <param name="byteArray"></param>
		/// <returns></returns>
		public static string GetStringFromByteArray(byte[] byteArray)
		{
			string result = Encoding.UTF8.GetString(byteArray);

			return result;
		}
	}
}
