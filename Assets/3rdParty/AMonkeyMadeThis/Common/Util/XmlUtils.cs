﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace AMonkeyMadeThis.Util
{
	/// <summary>
	/// XmlUtils
	/// </summary>
	public class XmlUtils
	{
		/// <summary>
		/// Serialize type to string
		/// </summary>
		/// <param name="type"></param>
		/// <param name="data"></param>
		/// <returns></returns>
		public static string Serialize(Type type, object data)
		{
			StringWriter writer = new StringWriter();
			XmlSerializer serialiser = new XmlSerializer(type);
			serialiser.Serialize(writer, data);
			writer.Close();

			string result = writer.ToString();
			
			return result;
		}

		/// <summary>
		/// Deserialize string to type
		/// </summary>
		/// <param name="type"></param>
		/// <param name="raw"></param>
		/// <returns></returns>
		public static object Deserialize(Type type, string raw)
		{
			XmlSerializer serialiser = new XmlSerializer(type);
			Stream stream = StringUtils.ConvertDataStringToStream(raw);
			object result = serialiser.Deserialize(stream);

			return result;
		}

		/// <summary>
		/// Deserialize string to T
		/// </summary>
		/// <param name="source"></param>
		/// <returns></returns>
		public static T Deserialise<T>(string source)
		{
			XmlSerializer serialiser = new XmlSerializer(typeof(T));
			Stream stream = StringUtils.ConvertDataStringToStream(source);
			T result = (T)serialiser.Deserialize(stream);

			return result;
		}
	}
}
