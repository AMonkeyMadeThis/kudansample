﻿using AMonkeyMadeThis.Common.Behaviour;
using UnityEngine;

namespace AMonkeyMadeThis.Common.View
{
	/// <summary>
	/// AbstractView
	/// </summary>
	public abstract class AbstractView : InvalidatingBehaviour
	{
		#region Lifecycle

		public override void Awake()
		{
			base.Awake();

			ConfigureScreenRotation();
		}

		public virtual void Update()
		{
			CheckForBackButton();
		}

		#endregion

		#region Screen rotation

		protected virtual void ConfigureScreenRotation()
		{
			// Template mothod
		}

		#endregion

		#region Navigation
		
		private void CheckForBackButton()
		{
			if (Input.GetKeyUp(KeyCode.Escape))
				GoBack();
		}

		public virtual void GoBack()
		{
			// Template method
		}

		#endregion
	}
}
