﻿using System;
using System.Collections;
using AMonkeyMadeThis.Common.Controller;
using POSPlacement.ActivationPlacement.Scripts.Model;
using POSPlacement.Common.Data.Scripts.Model;
using POSPlacement.Common.Data.Scripts.Model.VO;
using POSPlacement.Common.Scripts.Application.Controller;
using POSPlacement.Common.Scripts.Application.Model;
using UnityEngine;

namespace POSPlacement.ActivationPlacement.Scripts.Controller
{
	/// <summary>
	/// ActivationPlacementViewController
	/// </summary>
	public class ActivationPlacementViewController : AbstractController
	{
		#region Properties

		/// <summary>
		/// ApplicationController
		/// </summary>
		ApplicationController ApplicationController;
		/// <summary>
		/// ApplicationModel
		/// </summary>
		ApplicationModel ApplicationModel;
		/// <summary>
		/// ApplicationDataModel
		/// </summary>
		ApplicationDataModel ApplicationDataModel;
		/// <summary>
		/// ActivationPlacementViewModel
		/// </summary>
		ActivationPlacementViewModel ActivationPlacementViewModel;

		#endregion

		#region Lifecycle

		protected override void CollectComponents()
		{
			base.CollectComponents();

			ApplicationController = ApplicationController.Instance;
			ApplicationModel = ApplicationModel.Instance;
			ApplicationDataModel = ApplicationDataModel.Instance;
			ActivationPlacementViewModel = GetComponent<ActivationPlacementViewModel>();
		}

		public override void Start()
		{
			base.Start();

			PopulateModel();
		}

		#endregion

		#region Data

		private void PopulateModel()
		{
			if (ActivationPlacementViewModel != null)
			{
				PopulateActivations();
				PopulateWatermark();
			}
			else
			{
				Debug.LogError("Could not find ActivationPlacementViewModel");
			}
		}

		private void PopulateActivations()
		{
			if (ActivationPlacementViewModel != null && ApplicationModel != null && ApplicationModel.CurrentArea != null && ApplicationDataModel != null)
			{
				ActivationPlacementViewModel.Activations = ApplicationDataModel.GetActivations(ApplicationModel.CurrentArea.Activations);
			}
			else
			{
				Debug.LogError("Error populating activations");
			}
		}

		private void PopulateWatermark()
		{
			if (ApplicationModel != null && ApplicationModel.CurrentBrand != null)
			{
				BrandVO brand = ApplicationModel.CurrentBrand;

				if (brand != null && !string.IsNullOrEmpty(brand.Watermark))
				{
					string file = brand.Watermark;

					StartCoroutine(LoadWatermark(file));
				}
			}
			else
			{
				Debug.LogError("Error populating watermark");
			}
		}

		private IEnumerator LoadWatermark(String file)
		{
			ResourceRequest request = Resources.LoadAsync<Sprite>(file);

			while(!request.isDone)
				yield return null;

			var watermark = request.asset as Sprite;

			if (watermark != null)
				ActivationPlacementViewModel.Watermark = watermark;
		}

		#endregion

		#region Placement actions

		public void ToggleActivationVisibility()
		{
			if (ActivationPlacementViewModel != null)
				ActivationPlacementViewModel.ActivationsVisible = !ActivationPlacementViewModel.ActivationsVisible;
		}
		
		#endregion

		#region Navigation

		public void GoBack()
		{
			ApplicationController.GoBack();
		}

		#endregion
	}
}
