﻿using AMonkeyMadeThis.Common.Model;
using POSPlacement.Common.Data.Scripts.Model.VO;
using System.Collections.Generic;
using UnityEngine;

namespace POSPlacement.ActivationPlacement.Scripts.Model
{
	/// <summary>
	/// ActivationPlacementViewModel
	/// </summary>
	public class ActivationPlacementViewModel : AbstractModel
	{
		#region Events

		/// <summary>
		/// Delegate to handle changes in activationsVisible
		/// </summary>
		/// <param name="activationsVisible"></param>
		public delegate void ActivationsVisibleChanged(bool activationsVisible);
		/// <summary>
		/// Dispatched when activationsVisible changes
		/// </summary>
		public event ActivationsVisibleChanged OnActivationsVisibleChanged;
		/// <summary>
		/// Delegate to handle changes in activations
		/// </summary>
		/// <param name="activations"></param>
		public delegate void ActivationsChanged(List<ActivationVO> activations);
		/// <summary>
		/// Dispatched when activations change
		/// </summary>
		public event ActivationsChanged OnActivationsChanged;
		/// <summary>
		/// Delegate to handle changes in watermark
		/// </summary>
		/// <param name="watermark"></param>
		public delegate void WatermarkChanged(Sprite watermark);
		/// <summary>
		/// Dispatched when watermark changes
		/// </summary>
		public event WatermarkChanged OnWatermarkChanged;
		/// <summary>
		/// Delegate to handle changes in LightAzimuth
		/// </summary>
		/// <param name="lightAzimuth"></param>
		public delegate void LightAzimuthChanged(float lightAzimuth);
		/// <summary>
		/// Dispatched when LightAzimuth changes
		/// </summary>
		public event LightAzimuthChanged OnLightAzimuthChanged;
		/// <summary>
		/// Delegate to handle changes in LightElevation
		/// </summary>
		/// <param name="lightElevation"></param>
		public delegate void LightElevationChanged(float lightElevation);
		/// <summary>
		/// Dispatched when LightElevation changes
		/// </summary>
		public event LightElevationChanged OnLightElevationChanged;
		/// <summary>
		/// Delegate to handle changes in LightIntensity
		/// </summary>
		/// <param name="lightIntensity"></param>
		public delegate void LightIntensityChanged(float lightIntensity);
		/// <summary>
		/// Dispatched hen LightIntensity changes
		/// </summary>
		public event LightIntensityChanged OnLightIntensityChanged;
		
		#endregion

		#region Properties

		bool _activationsVisible = false;
		/// <summary>
		/// ActivationsVisible
		/// </summary>
		public bool ActivationsVisible
		{
			get { return _activationsVisible; }
			set
			{
				SetActivationsVisible(value);
			}
		}

		List<ActivationVO> _activations;
		/// <summary>
		/// Activations
		/// </summary>
		public List<ActivationVO> Activations
		{
			get { return _activations; }
			set
			{
				SetActivations(value);
			}
		}

		Sprite _watermark;
		/// <summary>
		/// Watermark
		/// </summary>
		public Sprite Watermark
		{
			get { return _watermark; }
			set
			{
				SetWatermark(value);
			}
		}

		float _lightAzimuth = 180;
		/// <summary>
		/// LightAzimuth
		/// </summary>
		public float LightAzimuth
		{
			get { return _lightAzimuth; }
			set
			{
				SetLightAzimuth(value);
			}
		}

		float _lightElevation = 45;
		/// <summary>
		/// LightElevation
		/// </summary>
		public float LightElevation
		{
			get { return _lightElevation; }
			set
			{
				SetLightElevation(value);
			}
		}

		float _lightIntensity = .8f;
		/// <summary>
		/// LightIntensity
		/// </summary>
		public float LightIntensity
		{
			get { return _lightIntensity; }
			set
			{
				SetLightIntensity(value);
			}
		}

		#endregion

		#region Mutators

		private void SetActivationsVisible(bool value)
		{
			_activationsVisible = value;
			DispatchOnActivationsVisibleChanged();
		}

		private void SetActivations(List<ActivationVO> value)
		{
			_activations = value;
			DispatchOnActivationsChanged();
		}

		private void SetWatermark(Sprite value)
		{
			_watermark = value;
			DispatchOnWatermarkChanged();
		}

		private void SetLightAzimuth(float value)
		{
			if (LightAzimuth != value)
			{
				_lightAzimuth = value;
				DispatchOnAzimuthChanged();
				Invalidate();
			}
		}

		private void SetLightElevation(float value)
		{
			if (LightElevation != value)
			{
				_lightElevation = value;
				DispatchOnElevationChanged();
				Invalidate();
			}
		}

		private void SetLightIntensity(float value)
		{
			if (LightIntensity != value)
			{
				_lightIntensity = value;
				DispatchOnIntensityChanged();
				Invalidate();
			}
		}

		#endregion

		#region Dispatchers

		private void DispatchOnActivationsVisibleChanged()
		{
			if (OnActivationsVisibleChanged != null)
				OnActivationsVisibleChanged(ActivationsVisible);
		}

		private void DispatchOnActivationsChanged()
		{
			if (OnActivationsChanged != null)
				OnActivationsChanged(Activations);
		}

		private void DispatchOnWatermarkChanged()
		{
			if (OnWatermarkChanged != null)
				OnWatermarkChanged(Watermark);
		}

		private void DispatchOnAzimuthChanged()
		{
			if (OnLightAzimuthChanged != null)
				OnLightAzimuthChanged(LightAzimuth);
		}

		private void DispatchOnElevationChanged()
		{
			if (OnLightElevationChanged != null)
				OnLightElevationChanged(LightElevation);
		}

		private void DispatchOnIntensityChanged()
		{
			if (OnLightIntensityChanged != null)
				OnLightIntensityChanged(LightIntensity);
		}

		#endregion
	}
}
