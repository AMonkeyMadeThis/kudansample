﻿using AMonkeyMadeThis.Bamboo.AssetBundle;
using AMonkeyMadeThis.Bamboo.Localisation.Controller;
using AMonkeyMadeThis.Bamboo.UI.Component.Data;
using AMonkeyMadeThis.Bamboo.UI.Component.Data.ItemRenderers;
using AMonkeyMadeThis.Common.Model.VO;
using AMonkeyMadeThis.Common.View;
using POSPlacement.ActivationPlacement.Scripts.Controller;
using POSPlacement.ActivationPlacement.Scripts.Model;
using POSPlacement.ActivationPlacement.Scripts.View.Components;
using POSPlacement.Common.Data.Scripts.Model;
using POSPlacement.Common.Data.Scripts.Model.VO;
using POSPlacement.Common.Highlighting.View;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using System;

namespace POSPlacement.ActivationPlacement.Scripts.View
{
	/// <summary>
	/// ActivationPlacementView
	/// </summary>
	public class ActivationPlacementView : AbstractView
	{
		#region Consts

		/// <summary>
		/// RESOURCE_BUNDLE_NAME_APPLICATION
		/// </summary>
		const string RESOURCE_BUNDLE_NAME_APPLICATION = "Application";
		/// <summary>
		/// RESOURCE_BUNDLE_NAME_ACTIVATIONS
		/// </summary>
		const string RESOURCE_BUNDLE_NAME_ACTIVATIONS = "Activations";
		/// <summary>
		/// IMAGE_NAME_PATTERN
		/// </summary>
		const string IMAGE_NAME_PATTERN = "Application.Save.ImageName";

		#endregion

		#region Properties

		[Header("Properties")]

		/// <summary>
		/// ActivationItemRendererPrototype
		/// </summary>
		public ItemRenderer ActivationItemRendererPrototype;
		/// <summary>
		/// ActivationPlacementViewModel
		/// </summary>
		ActivationPlacementViewModel ActivationPlacementViewModel;
		/// <summary>
		/// ActivationPlacementViewController
		/// </summary>
		ActivationPlacementViewController ActivationPlacementViewController;

		#endregion

		#region Skin parts

		[Header("Skin parts")]

		/// <summary>
		/// ActivationButton
		/// </summary>
		public Button ActivationButton;
		/// <summary>
		/// ScreenShotButton
		/// </summary>
		public Button ScreenShotButton;
		/// <summary>
		/// RefreshButton
		/// </summary>
		public Button RefreshButton;
		/// <summary>
		/// HomeButton
		/// </summary>
		public Button HomeButton;
		/// <summary>
		/// ActivationsScrollRect
		/// </summary>
		public ScrollRect ActivationsScrollRect;
		/// <summary>
		/// ActivationsDataGroup
		/// </summary>
		public DataGroup ActivationsDataGroup;
		/// <summary>
		/// ActivationInfo
		/// </summary>
		public ActivationInfo ActivationInfo;
		/// <summary>
		/// AR
		/// </summary>
		public AR AR;
		/// <summary>
		/// LightControls
		/// </summary>
		public LightControls LightControls;
		/// <summary>
		/// SunDolly
		/// </summary>
		public SunDolly SunDolly;
		/// <summary>
		/// ScreenShot
		/// </summary>
		public ScreenShot ScreenShot;
		/// <summary>
		/// Watermark
		/// </summary>
		public Image Watermark;

		#endregion

		#region Lifecycle

		protected override void InitVars()
		{
			base.InitVars();

			SetActivationPlacementViewModelItemRendererFunction();
		}

		protected override void CollectComponents()
		{
			base.CollectComponents();

			ActivationPlacementViewController = GetComponent<ActivationPlacementViewController>();
			ActivationPlacementViewModel = GetComponent<ActivationPlacementViewModel>();
		}

		public override void Start()
		{
			base.Start();
			
			if (ActivationPlacementViewModel != null)
				PopulateActivationsList(ActivationPlacementViewModel.Activations);
		}

		protected override void CommitProperties()
		{
			UpdateActivationsVisibility();
			UpdateLightControls();
			UpdateSunDolly();
			UpdateWatermark();
			base.CommitProperties();
		}

		private void UpdateWatermark()
		{
			if (Watermark != null)
				Watermark.sprite = ActivationPlacementViewModel.Watermark;
		}

		private void UpdateActivationInfo()
		{
			if (AR != null)
				UpdateActivationInfo(AR.Selected);
		}

		private void UpdateActivationInfo(InteractiveObject selected)
		{
			if (ActivationInfo != null)
			{
				// lazy, set it as null to start off
				ActivationInfo.gameObject.SetActive(false);

				if (selected != null)
				{
					var asset = selected.GetComponent<PrefabAsset>();

					if (asset != null)
					{
						ActivationVO activation = ApplicationDataModel.Instance.GetActivationById(asset.AssetID);

						if (activation != null)
						{
							ActivationInfo.gameObject.SetActive(true);
							ActivationInfo.Activation = activation;
						}
					}
				}
			}
		}

		private void UpdateActivationsVisibility()
		{
			if (ActivationsScrollRect != null)
			{
				ActivationsScrollRect.gameObject.SetActive(ActivationPlacementViewModel.ActivationsVisible);

				if (ActivationPlacementViewModel.ActivationsVisible)
					PopulateActivationsList(ActivationPlacementViewModel.Activations);
			}
		}

		private void UpdateLightControls()
		{
			if (ActivationPlacementViewModel != null && LightControls != null)
			{
				LightControls.Azimuth = ActivationPlacementViewModel.LightAzimuth;
				LightControls.Elevation = ActivationPlacementViewModel.LightElevation;
				LightControls.Intensity = ActivationPlacementViewModel.LightIntensity;
			}
		}

		private void UpdateSunDolly()
		{
			if (ActivationPlacementViewModel != null && SunDolly != null)
			{
				SunDolly.Azimuth = ActivationPlacementViewModel.LightAzimuth;
				SunDolly.Elevation = ActivationPlacementViewModel.LightElevation;
				SunDolly.Intensity = ActivationPlacementViewModel.LightIntensity;
			}
		}

		#endregion

		#region Screen rotation

		protected override void ConfigureScreenRotation()
		{
			Screen.autorotateToPortrait = true;
			Screen.autorotateToPortraitUpsideDown = true;
			Screen.autorotateToLandscapeLeft = true;
			Screen.autorotateToLandscapeRight = true;
		}

		#endregion

		#region Navigation

		public override void GoBack()
		{
			ActivationPlacementViewController.GoBack();
		}

		#endregion

		#region Screenshots

		private void TakeScreenshot()
		{
			if (ScreenShot != null)
			{
				string fileNamePattern = LocalisationController.Instance.LocaliseString(IMAGE_NAME_PATTERN, RESOURCE_BUNDLE_NAME_APPLICATION);
				string dateTimeStamp = DateTime.Now.ToString("dd/MM/yyyy hh:mm");
				string fileName = string.Format(fileNamePattern, dateTimeStamp);

				ScreenShot.SaveScreenshot(fileName);
			}
		}

		#endregion

		#region Data group

		private void PopulateActivationsList(List<ActivationVO> activations)
		{
			if (ActivationsDataGroup != null)
			{
				var alphabeticallySorted = activations.OrderBy(go => LocalisationController.Instance.LocaliseString(go.Name, RESOURCE_BUNDLE_NAME_ACTIVATIONS));

				ActivationsDataGroup.dataSource = alphabeticallySorted.ToArray();
			}
		}

		#endregion

		#region Item renderers

		private void SetActivationPlacementViewModelItemRendererFunction()
		{
			if (ActivationsDataGroup != null)
				ActivationsDataGroup.ItemRendererFunction = ActivationsDataGroupItemRendererFunction;
		}

		/// <summary>
		/// Select an appropriate item renderer for the value object, we know this will only be getting
		/// instances of ActivationVO (unless something has gone badly wrong) but the DataGroup is generic
		/// and only really cares if it is given descendents of ValueObjects to render with ItemRenderers.
		/// </summary>
		/// <param name="item">base class for any data, this method will inspec its type and select a specific ItemRenderer</param>
		/// <returns></returns>
		private ItemRenderer ActivationsDataGroupItemRendererFunction(ValueObject item)
		{
			if (item is ActivationVO)
				return ActivationItemRendererPrototype;

			// we could give a 'DefaultItemRenderer' but we don't want anything else showing up in the list, not that it could / should
			return null;
		}
		
		#endregion

		#region Listeners

		public override void AddListeners()
		{
			base.AddListeners();

			AddListeners(ActivationPlacementViewModel);
			AddListeners(ActivationsDataGroup);
			AddListeners(ActivationButton);
			AddListeners(ScreenShotButton);
			AddListeners(RefreshButton);
			AddListeners(HomeButton);
			AddListeners(AR);
			AddListeners(LightControls);
		}

		private void AddListeners(ActivationPlacementViewModel activationPlacementViewModel)
		{
			if (activationPlacementViewModel != null)
			{
				activationPlacementViewModel.OnActivationsVisibleChanged += HandleActivationPlacementViewModelOnActivationsVisibleChanged;
				activationPlacementViewModel.OnActivationsChanged += HandleActivationPlacementViewModelOnRegionsChanged;
				activationPlacementViewModel.OnWatermarkChanged += HandleActivationPlacementViewModelOnWatermarkChanged;
				activationPlacementViewModel.OnLightAzimuthChanged += HandleActivationPlacementViewModelOnLightAzimuthChanged;
				activationPlacementViewModel.OnLightElevationChanged += HandleActivationPlacementViewModelOnLightElevationChanged;
				activationPlacementViewModel.OnLightIntensityChanged += HandleActivationPlacementViewModelOnLightIntensityChanged;
			}
		}

		private void AddListeners(DataGroup areasDataGroup)
		{
			if (areasDataGroup != null)
			{
				areasDataGroup.OnItemSelected += HandleActivationsDataGroupOnItemSelected;
			}
		}

		private void AddListeners(Button button)
		{
			if (button != null)
			{
				if (button == ActivationButton)
					button.onClick.AddListener(HandleActivationButtonOnClick);
				else if (button == ScreenShotButton)
					button.onClick.AddListener(HandleScreenShotButtonOnClick);
				else if (button == RefreshButton)
					button.onClick.AddListener(HandleRefreshButtonOnClick);
				else if (button == HomeButton)
					button.onClick.AddListener(HandleHomeButtonOnClick);
			}
		}

		private void AddListeners(AR aR)
		{
			if (AR != null)
			{
				AR.OnSelectedChanged += HandleAROnSelectedChanged;
			}
		}

		private void AddListeners(LightControls lightControls)
		{
			if (lightControls != null)
			{
				lightControls.OnAzimuthChanged += HandleLightControlsOnAzimuthChanged;
				lightControls.OnElevationChanged += HandleLightControlsOnElevationChanged;
				lightControls.OnIntensityChanged += HandleLightControlsOnIntensityChanged;
			}
		}

		public override void RemoveListeners()
		{
			base.RemoveListeners();

			RemoveListeners(ActivationPlacementViewModel);
			RemoveListeners(ActivationsDataGroup);
			RemoveListeners(ActivationButton);
			RemoveListeners(ScreenShotButton);
			RemoveListeners(RefreshButton);
			RemoveListeners(HomeButton);
			RemoveListeners(AR);
		}

		private void RemoveListeners(ActivationPlacementViewModel activationPlacementViewModel)
		{
			if (activationPlacementViewModel != null)
			{
				activationPlacementViewModel.OnActivationsVisibleChanged -= HandleActivationPlacementViewModelOnActivationsVisibleChanged;
				activationPlacementViewModel.OnActivationsChanged -= HandleActivationPlacementViewModelOnRegionsChanged;
				activationPlacementViewModel.OnWatermarkChanged -= HandleActivationPlacementViewModelOnWatermarkChanged;
				activationPlacementViewModel.OnLightAzimuthChanged -= HandleActivationPlacementViewModelOnLightAzimuthChanged;
				activationPlacementViewModel.OnLightElevationChanged -= HandleActivationPlacementViewModelOnLightElevationChanged;
				activationPlacementViewModel.OnLightIntensityChanged -= HandleActivationPlacementViewModelOnLightIntensityChanged;
			}
		}

		private void RemoveListeners(DataGroup areasDataGroup)
		{
			if (areasDataGroup != null)
			{
				areasDataGroup.OnItemSelected -= HandleActivationsDataGroupOnItemSelected;
			}
		}

		private void RemoveListeners(Button button)
		{
			if (button != null)
			{
				if (button == ActivationButton)
					button.onClick.RemoveListener(HandleHomeButtonOnClick);
				else if (button == ScreenShotButton)
					button.onClick.RemoveListener(HandleScreenShotButtonOnClick);
				else if (button == RefreshButton)
					button.onClick.RemoveListener(HandleRefreshButtonOnClick);
				else if (button == HomeButton)
					button.onClick.RemoveListener(HandleHomeButtonOnClick);
			}
		}

		private void RemoveListeners(AR aR)
		{
			if (AR != null)
			{
				AR.OnSelectedChanged -= HandleAROnSelectedChanged;
			}
		}

		private void RemoveListeners(LightControls lightControls)
		{
			if (lightControls != null)
			{
				lightControls.OnAzimuthChanged -= HandleLightControlsOnAzimuthChanged;
				lightControls.OnElevationChanged -= HandleLightControlsOnElevationChanged;
				lightControls.OnIntensityChanged -= HandleLightControlsOnIntensityChanged;
			}
		}

		#endregion

		#region Handlers

		private void HandleActivationPlacementViewModelOnRegionsChanged(List<ActivationVO> activations)
		{
			PopulateActivationsList(activations);
		}

		private void HandleActivationPlacementViewModelOnActivationsVisibleChanged(bool activationsVisible)
		{
			Invalidate();
		}

		private void HandleActivationPlacementViewModelOnWatermarkChanged(Sprite watermark)
		{
			Invalidate();
		}

		private void HandleActivationPlacementViewModelOnLightAzimuthChanged(float lightAzimuth)
		{
			Invalidate();
		}

		private void HandleActivationPlacementViewModelOnLightElevationChanged(float lightElevation)
		{
			Invalidate();
		}

		private void HandleActivationPlacementViewModelOnLightIntensityChanged(float lightIntensity)
		{
			Invalidate();
		}
		
		private void HandleActivationsDataGroupOnItemSelected(ItemRenderer itemRenderer)
		{
			if (AR != null && itemRenderer != null)
			{
				var activation = itemRenderer.Data as ActivationVO;
				
				if (activation != null && activation.Asset != null)
				{
					var prefabAsset = activation.Asset as PrefabAsset;

					if (prefabAsset != null)
					{
						InteractiveObject prototype = prefabAsset.GetComponent<InteractiveObject>();

						if (prototype != null)
							AR.AddTrackedObject(prototype);
					}
				}
			}
		}

		private void HandleActivationButtonOnClick()
		{
			ActivationPlacementViewController.ToggleActivationVisibility();
		}

		private void HandleScreenShotButtonOnClick()
		{
			TakeScreenshot();
		}

		private void HandleRefreshButtonOnClick()
		{
			if (AR != null)
				AR.Reset();
		}

		private void HandleHomeButtonOnClick()
		{
			ActivationPlacementViewController.GoBack();
		}

		private void HandleAROnSelectedChanged(InteractiveObject selected)
		{
			CallLater(UpdateActivationInfo);
			//UpdateActivationInfo(selected);
		}

		private void HandleLightControlsOnAzimuthChanged(float azimuth)
		{
			if (ActivationPlacementViewModel != null)
				ActivationPlacementViewModel.LightAzimuth = azimuth;
		}

		private void HandleLightControlsOnElevationChanged(float elevation)
		{
			if (ActivationPlacementViewModel != null)
				ActivationPlacementViewModel.LightElevation = elevation;
		}

		private void HandleLightControlsOnIntensityChanged(float intensity)
		{
			if (ActivationPlacementViewModel != null)
				ActivationPlacementViewModel.LightIntensity = intensity;
		}

		#endregion
	}
}
