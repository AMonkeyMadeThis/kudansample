﻿using AMonkeyMadeThis.Common.View;
using Kudan.AR;
using POSPlacement.Common.Activations.View;
using POSPlacement.Common.Highlighting.View;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace POSPlacement.ActivationPlacement.Scripts.View.Components
{
	/// <summary>
	/// AR
	/// </summary>
	public class AR : AbstractView
	{
		#region Events

		/// <summary>
		/// Delegate to handle changes in selected
		/// </summary>
		/// <param name="selected"></param>
		public delegate void SelectedChanged(InteractiveObject selected);
		/// <summary>
		/// Dispatched when selected changes
		/// </summary>
		public event SelectedChanged OnSelectedChanged;

		#endregion

		#region Skin parts

		[Header("Skin parts")]

		/// <summary>
		/// KudanTracker
		/// </summary>
		public KudanTracker KudanTracker;
		/// <summary>
		/// TrackingContainer
		/// </summary>
		public Transform TrackingContainer;
		/// <summary>
		/// Reticle
		/// </summary>
		public Image Reticle;
		/// <summary>
		/// Arrow
		/// </summary>
		public Transform Arrow;

		#endregion

		#region Properties

		[Header("Properties")]

		/// <summary>
		/// ScaleSpeed
		/// </summary>
		public float ScaleSpeed = 10;
		/// <summary>
		/// AdjustSpeed
		/// </summary>
		public float AdjustSpeed = .1f;
		/// <summary>
		/// RotateSpeed
		/// </summary>
		public float RotateSpeed = 10;
		/// <summary>
		/// Initial scale for spawned objects, it looks like Kudan is off by two orders of magnitude so we need to scale up by 100
		/// </summary>
		public Vector3 SpawnedObjectInitialScale = new Vector3(100, 100, 100);
		/// <summary>
		/// trackingInitialised
		/// </summary>
		bool trackingInitialised = false;
		/// <summary>
		/// Camera
		/// </summary>
		Camera Camera;
		/// <summary>
		/// TouchInteraction
		/// </summary>
		public TouchInteraction TouchInteraction;
		/// <summary>
		/// trackedObjects
		/// </summary>
		List<InteractiveObject> trackedObjects;
		
		InteractiveObject _selected;
		/// <summary>
		/// Selected
		/// </summary>
		public InteractiveObject Selected
		{
			get { return _selected; }
			set
			{
				SetSelected(value);
			}
		}

		#endregion

		#region Accessors

		public bool HasSelected
		{
			get
			{
				return (Selected != null);
			}
		}

		#endregion

		#region Mutators

		private void SetSelected(InteractiveObject value)
		{
			if (_selected != value)
			{
				_selected = value;
				DispatchOnSelectedChanged();
				Invalidate();
			}
		}

		#endregion

		#region Lifecycle

		protected override void InitVars()
		{
			base.InitVars();

			trackedObjects = new List<InteractiveObject>();
		}

		protected override void CollectComponents()
		{
			base.CollectComponents();

			if (KudanTracker != null)
				Camera = KudanTracker.GetComponent<Camera>();
		}

		public override void Awake()
		{
			base.Awake();

			InitTracking();
		}
		
		protected override void CommitProperties()
		{
			UpdateSelection();
			base.CommitProperties();
		}

		public override void Update()
		{
			base.Update();

			if (TouchInteraction != null && TouchInteraction.IsTouching)
			{
				HideArrow();
				// the listeners / handles will take care of the interaction
			}
			else
			{
				AutoSelectItemAtCentreOfScreen();
				UpdateArrow();
			}
		}

		#endregion

		#region Arrow

		private void UpdateArrow()
		{
			if (Selected != null)
			{
				HideArrow();
			}
			else
			{
				ShowArrow();
			}
		}

		private void ShowArrow()
		{
			if (Arrow != null)
			{
				PlaceObject(Arrow.gameObject);
				Arrow.gameObject.SetActive(true);
			}
		}

		private void HideArrow()
		{
			if (Arrow != null)
				Arrow.gameObject.SetActive(false);
		}
		
		#endregion
		
		#region Tracking

		private void InitTracking()
		{
			try
			{
				Vector3 position;
				Quaternion orientation;

				Reset();

				KudanTracker.FloorPlaceGetPose(out position, out orientation);
				KudanTracker.ArbiTrackStart(position, orientation);

				trackingInitialised = true;
			}
			catch
			{
				Debug.LogError("Error initialising AR tracking.");
				trackingInitialised = false;
			}
		}

		public void AddTrackedObject(InteractiveObject prototype)
		{
			if (prototype != null)
			{
				InteractiveObject instance = Instantiate<InteractiveObject>(prototype, TrackingContainer);
				PlaceObject(instance.gameObject);
				AddListeners(instance);

				trackedObjects.Add(instance);
			}
		}

		private void PlaceObject(GameObject gameObject)
		{
			if (gameObject != null)
			{
				if (!trackingInitialised)
					InitTracking();
				
				if (trackingInitialised)
				{
					Vector3 position;
					Quaternion orientation;
					KudanTracker.FloorPlaceGetPose(out position, out orientation);
					gameObject.transform.parent = TrackingContainer;
					gameObject.transform.position = position;
					gameObject.transform.rotation = orientation;
					gameObject.transform.localScale = SpawnedObjectInitialScale;

					PointAt(gameObject, Camera);
				}
				else
				{
					Debug.LogError("Unable to place object until tracking is initialised");
				}
			}
		}

		void PointAt(GameObject gameObject, Camera camera)
		{
			if (gameObject != null && Camera != null)
			{
				Transform observer = gameObject.transform;
				Transform target = Camera.transform;

				float distanceToPlane = Vector3.Dot(observer.up, target.position - observer.position);
				Vector3 pointOnPlane = target.position - (observer.up * distanceToPlane);

				observer.LookAt(pointOnPlane, observer.up);
			}
		}

		public void Reset()
		{
			if (trackedObjects != null && trackedObjects.Count > 0)
			{
				// Clone list so we don't have issues when we remove items
				InteractiveObject[] _trackedObjects = trackedObjects.ToArray();

				foreach(var trackedObject in _trackedObjects)
				{
					RemoveTrackedObject(trackedObject);
				}

				// Clear just in case
				trackedObjects.Clear();
				// (re)init tracking
				InitTracking();
			}
		}

		public void RemoveTrackedObject(InteractiveObject instance)
		{
			if (instance != null)
			{
				instance.transform.position = Vector3.up * 1000;
				RemoveListeners(instance);
				// Remove from tracked objects
				if (trackedObjects != null && trackedObjects.Contains(instance))
					trackedObjects.Remove(instance);
				// Destroy!!
				Destroy(instance.gameObject);
				// Set no selection
				Selected = null;
			}
		}

		#endregion

		#region Selection

		private void UpdateSelection()
		{
			if (trackedObjects != null && trackedObjects.Count > 0)
			{
				foreach (var trackedObject in trackedObjects)
				{
					if (trackedObject != null)
					{
						bool isSelected = (trackedObject == Selected);

						trackedObject.Selected = isSelected;
					}
				}
			}
		}

		private void AutoSelectItemAtCentreOfScreen()
		{
			SelectInteractiveObjectUnderPoint(new Vector2(.5f, .5f));
		}

		public void DeleteSelected()
		{
			if (Selected != null)
			{
				RemoveTrackedObject(Selected);
				Selected = null;
			}
		}

		void SelectInteractiveObjectUnderPoint(Vector2 position)
		{
			SetReticlePosition(position);

			if (Camera != null && position != null)
			{
				Ray ray = Camera.ViewportPointToRay(new Vector3(position.x, position.y, 0));
				RaycastHit hit;

				if (Physics.Raycast(ray, out hit))
				{
					InteractiveObject target = hit.collider.GetComponentInParent<InteractiveObject>();

					if (target != null)
						target.Selected = true;
				}
				else
				{
					Selected = null;
				}
			}
		}

		private void MoveSelected()
		{
			if (KudanTracker != null && Selected != null)
			{
				if (!trackingInitialised)
					InitTracking();

				if (trackingInitialised)
				{
					Vector3 position;
					Quaternion orientation;
					KudanTracker.FloorPlaceGetPose(out position, out orientation);

					Selected.transform.position = position;
				}
			}
		}

		private void RotateSelected(float scroll)
		{
			if (Selected != null)
			{
				float change = scroll * RotateSpeed * Time.deltaTime;
				Selected.transform.Rotate(new Vector3(0, change, 0), Space.Self);
			}
		}

		private void AdjustSelected(float scroll)
		{
			if (Selected != null)
			{
				var activation = Selected.GetComponent<Activation>();

				if (activation != null && activation.IsAdjustable == true)
				{
					float change = scroll * AdjustSpeed * Time.deltaTime;
					activation.Adjustment += change;
				}
			}
		}

		private void ScaleSelected(float zoom)
		{
			if (Selected != null)
			{
				float change = zoom - 1;
				float _zoom = 1 + (change * ScaleSpeed);
				Vector3 scale = Selected.transform.localScale * _zoom;

				Selected.transform.localScale = scale;
			}
		}

		#endregion

		#region Reticle

		void SetReticlePosition(Vector2 position)
		{
			if (Reticle != null)
				Reticle.transform.position = new Vector3(Screen.width * position.x, Screen.height * position.y, 0);
		}

		#endregion

		#region Listeners

		public override void AddListeners()
		{
			base.AddListeners();

			AddListeners(TouchInteraction);
		}

		private void AddListeners(TouchInteraction touchInteraction)
		{
			if (touchInteraction != null)
			{
				touchInteraction.OnIsTouchingChanged += HandleTouchInteractionOnIsTouchingChanged;
				touchInteraction.OnSelect += HandleTouchInteractionOnSelect;
				touchInteraction.OnHold += HandleTouchInteractionOnHold;
				touchInteraction.OnDrag += HandleTouchInteractionOnDrag;
				touchInteraction.OnPinch += HandleTouchInteractionOnPinch;
			}
		}

		private void AddListeners(InteractiveObject interactiveObject)
		{
			if (interactiveObject != null)
			{
				interactiveObject.OnSelectedChanged += HandleInteractiveObjectOnSelectedChanged;
			}
		}

		public override void RemoveListeners()
		{
			base.RemoveListeners();

			RemoveListeners(TouchInteraction);
		}

		private void RemoveListeners(TouchInteraction touchInteraction)
		{
			if (touchInteraction != null)
			{
				touchInteraction.OnIsTouchingChanged -= HandleTouchInteractionOnIsTouchingChanged;
				touchInteraction.OnSelect -= HandleTouchInteractionOnSelect;
				touchInteraction.OnHold -= HandleTouchInteractionOnHold;
				touchInteraction.OnDrag -= HandleTouchInteractionOnDrag;
				touchInteraction.OnPinch -= HandleTouchInteractionOnPinch;
			}
		}

		private void RemoveListeners(InteractiveObject interactiveObject)
		{
			if (interactiveObject != null)
			{
				interactiveObject.OnSelectedChanged -= HandleInteractiveObjectOnSelectedChanged;
			}
		}

		#endregion

		#region Handlers

		private void HandleTouchInteractionOnIsTouchingChanged(bool isTouching)
		{
			// TODO
		}

		private void HandleTouchInteractionOnSelect(Vector2 position)
		{
			SelectInteractiveObjectUnderPoint(new Vector2(.5f, .5f));
		}

		private void HandleTouchInteractionOnHold(Vector2 position)
		{
			//SelectInteractiveObjectUnderPoint(new Vector2(.5f, .5f));
			//MoveSelected();
		}

		private void HandleTouchInteractionOnDrag(Vector2 position, Vector2 deltaPosition)
		{
			RotateSelected(-deltaPosition.x);
			AdjustSelected(deltaPosition.y);
		}

		private void HandleTouchInteractionOnPinch(float scalar)
		{
			ScaleSelected(scalar);
		}

		private void HandleInteractiveObjectOnSelectedChanged(InteractiveObject interactiveObject)
		{
			if (interactiveObject != null && interactiveObject.Selected == true)
				Selected = interactiveObject;
		}

		#endregion

		#region Dispatchers

		private void DispatchOnSelectedChanged()
		{
			if (OnSelectedChanged != null)
				OnSelectedChanged(Selected);
		}

		#endregion
	}
}