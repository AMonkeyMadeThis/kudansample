﻿using AMonkeyMadeThis.Bamboo.Localisation.Model;
using AMonkeyMadeThis.Common.View;
using POSPlacement.Common.Data.Scripts.Model.VO;
using UnityEngine.UI;

namespace POSPlacement.ActivationPlacement.Scripts.View.Components
{
	/// <summary>
	/// ActivationInfo
	/// </summary>
	public class ActivationInfo : AbstractView
	{
		#region Consts

		/// <summary>
		/// Activations resource bundle name
		/// </summary>
		const string ASSET_BUNDLE_NAME_ACTIVATIONS = "Activations";
		/// <summary>
		/// Activation height pattern eg "{0} centimetres" or "{0} centimeters" or "{0} centímetros"
		/// </summary>
		const string ACTIVATION_HEIGHT_PATTERN = "Activations.attributes.height";

		#endregion

		#region Skin parts

		/// <summary>
		/// ActivationNameLabel
		/// </summary>
		public Text ActivationNameLabel;
		/// <summary>
		/// ActivationSizeLabel
		/// </summary>
		public Text ActivationSizeLabel;
		/// <summary>
		/// DeleteActivationButton
		/// </summary>
		public Button DeleteActivationButton;

		#endregion

		#region Properties

		ActivationVO _activation;
		/// <summary>
		/// Activation
		/// </summary>
		public ActivationVO Activation
		{
			get { return _activation; }
			set
			{
				SetActivation(value);
			}
		}

		#endregion

		#region Mutators

		private void SetActivation(ActivationVO value)
		{
			if (Activation != value)
			{
				_activation = value;
				Invalidate();
			}
		}

		#endregion

		#region Lifecycle

		protected override void CommitProperties()
		{
			UpdateVisibility();
			UpdateUI();
			base.CommitProperties();
		}

		private void UpdateVisibility()
		{
			bool hasActivation = (Activation != null);

			//gameObject.SetActive(hasActivation);
		}

		private void UpdateUI()
		{
			if (Activation != null)
			{
				SetActivationNameLabel();

				SetActivationSizeLabel();
			}
		}

		private void SetActivationNameLabel()
		{
			if (ActivationNameLabel != null)
			{
				string name = LocalisationModel.Instance.LocaliseString(Activation.Name, ASSET_BUNDLE_NAME_ACTIVATIONS);
				ActivationNameLabel.text = name;
			}
		}

		private void SetActivationSizeLabel()
		{
			if (ActivationSizeLabel != null)
			{
				string pattern = LocalisationModel.Instance.LocaliseString(ACTIVATION_HEIGHT_PATTERN, ASSET_BUNDLE_NAME_ACTIVATIONS);
				string height = string.Format(pattern, Activation.Height);
				ActivationSizeLabel.text = height;
			}
		}

		#endregion
	}
}
