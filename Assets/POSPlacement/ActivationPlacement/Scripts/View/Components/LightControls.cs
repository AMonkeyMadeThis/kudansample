﻿using AMonkeyMadeThis.Common.View;
using UnityEngine;
using UnityEngine.UI;

namespace POSPlacement.ActivationPlacement.Scripts.View.Components
{
	/// <summary>
	/// LightControls
	/// </summary>
	public class LightControls : AbstractView
	{
		#region Events

		/// <summary>
		/// Delegate to handle changes in IsExpanded
		/// </summary>
		/// <param name="isExpanded"></param>
		public delegate void IsExpandedChanged(bool isExpanded);
		/// <summary>
		/// Dispatched when IsExpandedChanges
		/// </summary>
		public event IsExpandedChanged OnIsExpandedChanged;
		/// <summary>
		/// Delegate to handle changes in Azimuth
		/// </summary>
		/// <param name="azimuth"></param>
		public delegate void AzimuthChanged(float azimuth);
		/// <summary>
		/// Dispatched when Azimuth changes
		/// </summary>
		public event AzimuthChanged OnAzimuthChanged;
		/// <summary>
		/// Delegate to handle changes in Elevation
		/// </summary>
		/// <param name="elevation"></param>
		public delegate void ElevationChanged(float elevation);
		/// <summary>
		/// Dispatched when Elevation changes
		/// </summary>
		public event ElevationChanged OnElevationChanged;
		/// <summary>
		/// Delegate to handle changes in Intensity
		/// </summary>
		/// <param name="intensity"></param>
		public delegate void IntensityChanged(float intensity);
		/// <summary>
		/// Dispatched hen Intensity changes
		/// </summary>
		public event IntensityChanged OnIntensityChanged;

		#endregion

		#region Skin parts

		[Header("Skin parts")]

		/// <summary>
		/// LightButton
		/// </summary>
		public Button LightButton;
		/// <summary>
		/// AzimuthSlider
		/// </summary>
		public Slider AzimuthSlider;
		/// <summary>
		/// ElevationSlider
		/// </summary>
		public Slider ElevationSlider;
		/// <summary>
		/// IntensitySlider
		/// </summary>
		public Slider IntensitySlider;
		/// <summary>
		/// ControlsPanel
		/// </summary>
		public RectTransform ControlsPanel;

		#endregion

		#region Properties

		bool _isExpanded = false;
		/// <summary>
		/// IsExpanded
		/// </summary>
		public bool IsExpanded
		{
			get { return _isExpanded; }
			set
			{
				SetIsExpanded(value);
			}
		}

		float _azimuth;
		/// <summary>
		/// Azimuth
		/// </summary>
		public float Azimuth
		{
			get { return _azimuth; }
			set
			{
				SetAzimuth(value);
			}
		}

		float _elevation;
		/// <summary>
		/// Elevation
		/// </summary>
		public float Elevation
		{
			get { return _elevation; }
			set
			{
				SetElevation(value);
			}
		}

		float _intensity;
		/// <summary>
		/// Intensity
		/// </summary>
		public float Intensity
		{
			get { return _intensity; }
			set
			{
				SetIntensity(value);
			}
		}

		#endregion

		#region Mutators

		private void SetIsExpanded(bool value)
		{
			if (IsExpanded != value)
			{
				_isExpanded = value;
				DispatchOnIsExpandedChanged();
				Invalidate();
			}
		}

		private void SetAzimuth(float value)
		{
			if (Azimuth != value)
			{
				_azimuth = value;
				DispatchOnAzimuthChanged();
				Invalidate();
			}
		}

		private void SetElevation(float value)
		{
			if (Elevation != value)
			{
				_elevation = value;
				DispatchOnElevationChanged();
				Invalidate();
			}
		}

		private void SetIntensity(float value)
		{
			if (Intensity != value)
			{
				_intensity = value;
				DispatchOnIntensityChanged();
				Invalidate();
			}
		}

		void ToggleIsExpanded()
		{
			IsExpanded = !IsExpanded;
		}

		#endregion

		#region Lifecycle

		protected override void CommitProperties()
		{
			UpdateControlsPanel();
			UpdateAzimuthSlider();
			UpdateElevationSlider();
			UpdateIntensitySlider();
			base.CommitProperties();
		}

		private void UpdateControlsPanel()
		{
			if (ControlsPanel != null)
				ControlsPanel.gameObject.SetActive(IsExpanded);
		}

		private void UpdateAzimuthSlider()
		{
			if (AzimuthSlider != null)
				AzimuthSlider.value = Azimuth;
		}

		private void UpdateElevationSlider()
		{
			if (ElevationSlider != null)
				ElevationSlider.value = Elevation;
		}

		private void UpdateIntensitySlider()
		{
			if (IntensitySlider != null)
				IntensitySlider.value = Intensity;
		}

		#endregion

		#region Listeners

		public override void AddListeners()
		{
			base.AddListeners();

			AddListeners(LightButton);
			AddListeners(AzimuthSlider);
			AddListeners(ElevationSlider);
			AddListeners(IntensitySlider);
		}

		private void AddListeners(Button button)
		{
			if (button != null)
			{
				if (button == LightButton)
					button.onClick.AddListener(HandleLightButtonOnClick);
			}
		}

		private void AddListeners(Slider slider)
		{
			if (slider != null)
			{
				if (slider == AzimuthSlider)
					slider.onValueChanged.AddListener(HandleAzimuthSlideronValueChanged);
				else if (slider == ElevationSlider)
					slider.onValueChanged.AddListener(HandleElevationSlideronValueChanged);
				else if (slider == IntensitySlider)
					slider.onValueChanged.AddListener(HandleIntensitySlideronValueChanged);
			}
		}

		public override void RemoveListeners()
		{
			base.RemoveListeners();

			RemoveListeners(LightButton);
			RemoveListeners(AzimuthSlider);
			RemoveListeners(ElevationSlider);
			RemoveListeners(IntensitySlider);
		}

		private void RemoveListeners(Button button)
		{
			if (button != null)
			{
				if (button == LightButton)
					button.onClick.RemoveListener(HandleLightButtonOnClick);
			}
		}

		private void RemoveListeners(Slider slider)
		{
			if (slider != null)
			{
				if (slider == AzimuthSlider)
					slider.onValueChanged.RemoveListener(HandleAzimuthSlideronValueChanged);
				else if (slider == ElevationSlider)
					slider.onValueChanged.RemoveListener(HandleElevationSlideronValueChanged);
				else if (slider == IntensitySlider)
					slider.onValueChanged.RemoveListener(HandleIntensitySlideronValueChanged);
			}
		}

		#endregion

		#region Handlers

		private void HandleLightButtonOnClick()
		{
			ToggleIsExpanded();
		}

		private void HandleAzimuthSlideronValueChanged(float azimuth)
		{
			Azimuth = azimuth;
		}

		private void HandleElevationSlideronValueChanged(float elevation)
		{
			Elevation = elevation;
		}

		private void HandleIntensitySlideronValueChanged(float intensity)
		{
			Intensity = intensity;
		}

		#endregion

		#region Dispatchers

		private void DispatchOnIsExpandedChanged()
		{
			if (OnIsExpandedChanged != null)
				OnIsExpandedChanged(IsExpanded);
		}

		private void DispatchOnAzimuthChanged()
		{
			if (OnAzimuthChanged != null)
				OnAzimuthChanged(Azimuth);
		}

		private void DispatchOnElevationChanged()
		{
			if (OnElevationChanged != null)
				OnElevationChanged(Elevation);
		}

		private void DispatchOnIntensityChanged()
		{
			if (OnIntensityChanged != null)
				OnIntensityChanged(Intensity);
		}

		#endregion
	}
}
