﻿using AMonkeyMadeThis.Bamboo.Localisation.Controller;
using AMonkeyMadeThis.Bamboo.UI.Component;
using System;
using UnityEngine;

namespace POSPlacement.ActivationPlacement.Scripts.View.Components
{
	/// <summary>
	/// ScreenShot
	/// </summary>
	public class ScreenShot : UIComponent
	{
		#region Consts

		/// <summary>
		/// ALBUM_NAME
		/// </summary>
		public const string ALBUM_NAME = "Application.Save.AlbumName";
		/// <summary>
		/// APPLICATION_ASSET_BUNDLE_NAME
		/// </summary>
		public const string APPLICATION_ASSET_BUNDLE_NAME = "Application";

		#endregion

		#region Enums

		/// <summary>
		/// Format
		/// </summary>
		[Flags]
		public enum Format
		{
			jpeg,
			png
		}

		#endregion

		#region Properties
		
		/// <summary>
		/// IncludeUI
		/// </summary>
		public bool IncludeUI = false;
		/// <summary>
		/// UI
		/// </summary>
		public Canvas UI;
		/// <summary>
		/// ScreenshotOverlays
		/// </summary>
		public Canvas ScreenshotOverlays;
		/// <summary>
		/// Components
		/// </summary>
		public MonoBehaviour[] Effects;

		#endregion

		#region Lifecycle

		public override void Start()
		{
			base.Start();

			PlayMode();
		}

		#endregion

		#region Capture

		public void SaveScreenshot(string fileName)
		{
			string format = Format.jpeg.ToString();
			string albumName = LocalisationController.Instance.LocaliseString(ALBUM_NAME, APPLICATION_ASSET_BUNDLE_NAME);

			CaptureMode();

			ScreenshotManager.SaveScreenshot(fileName, albumName, format);
		}

		public void SaveImage(Texture2D texture, string fileName)
		{
			string format = Format.png.ToString();
			string albumName = LocalisationController.Instance.LocaliseString(ALBUM_NAME, APPLICATION_ASSET_BUNDLE_NAME);

			ScreenshotManager.SaveImage(texture, fileName, albumName, format);
		}

		#endregion

		#region Modes

		void CaptureMode()
		{
			ShowOverlays();
			HideUI();
			DisableEffects();
		}

		void PlayMode()
		{
			HideOverlays();
			ShowUI();
			EnableEffects();
		}

		#endregion

		#region Effects

		private void DisableEffects()
		{
			SetEffectsState(false);
		}

		private void EnableEffects()
		{
			SetEffectsState(true);
		}

		void SetEffectsState(bool isEnabled)
		{
			if (Effects != null)
			{
				foreach (var effect in Effects)
				{
					if (effect != null)
						effect.enabled = isEnabled;
				}
			}
		}

		#endregion

		#region UI & Overlays

		private void ShowOverlays()
		{
			SetOverlaysVisibility(true);
		}

		private void HideOverlays()
		{
			SetOverlaysVisibility(false);
		}

		private void HideUI()
		{
			SetUIVisibility(false);
		}

		private void ShowUI()
		{
			SetUIVisibility(true);
		}

		private void SetUIVisibility(bool visible)
		{
			if (!IncludeUI && UI != null)
				SetVisibility(UI.gameObject, visible);
		}

		private void SetOverlaysVisibility(bool visible)
		{
			if (ScreenshotOverlays != null)
				SetVisibility(ScreenshotOverlays.gameObject, visible);
		}

		private void SetVisibility(GameObject target, bool visible)
		{
			if (target != null)
				target.SetActive(visible);
		}

		#endregion

		#region Listeners

		public override void AddListeners()
		{
			base.AddListeners();

			ScreenshotManager.OnScreenshotTaken += HandleScreenshotManagerOnScreenshotTaken;
			ScreenshotManager.OnScreenshotSaved += HandleScreenshotManagerOnScreenshotSaved;
			ScreenshotManager.OnImageSaved += HandleScreenshotManagerOnImageSaved;
		}

		public override void RemoveListeners()
		{
			base.RemoveListeners();

			ScreenshotManager.OnScreenshotTaken -= HandleScreenshotManagerOnScreenshotTaken;
			ScreenshotManager.OnScreenshotSaved -= HandleScreenshotManagerOnScreenshotSaved;
			ScreenshotManager.OnImageSaved -= HandleScreenshotManagerOnImageSaved;
		}

		#endregion

		#region Handlers

		private void HandleScreenshotManagerOnScreenshotTaken(Texture2D obj)
		{
			PlayMode();
		}

		private void HandleScreenshotManagerOnScreenshotSaved(string obj)
		{
			PlayMode();
		}

		private void HandleScreenshotManagerOnImageSaved(string obj)
		{
			PlayMode();
		}

		#endregion
	}
}