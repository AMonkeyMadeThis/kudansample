﻿using AMonkeyMadeThis.Common.Math;
using AMonkeyMadeThis.Common.View;
using UnityEngine;

namespace POSPlacement.ActivationPlacement.Scripts.View.Components
{
	/// <summary>
	/// SunDolly
	/// </summary>
	public class SunDolly : AbstractView
	{
		#region Events
		
		/// <summary>
		/// Delegate to handle changes in Azimuth
		/// </summary>
		/// <param name="azimuth"></param>
		public delegate void AzimuthChanged(float azimuth);
		/// <summary>
		/// Dispatched when Azimuth changes
		/// </summary>
		public event AzimuthChanged OnAzimuthChanged;
		/// <summary>
		/// Delegate to handle changes in Elevation
		/// </summary>
		/// <param name="elevation"></param>
		public delegate void ElevationChanged(float elevation);
		/// <summary>
		/// Dispatched when Elevation changes
		/// </summary>
		public event ElevationChanged OnElevationChanged;
		/// <summary>
		/// Delegate to handle changes in Intensity
		/// </summary>
		/// <param name="intensity"></param>
		public delegate void IntensityChanged(float intensity);
		/// <summary>
		/// Dispatched hen Intensity changes
		/// </summary>
		public event IntensityChanged OnIntensityChanged;

		#endregion

		#region Skin parts
		
		/// <summary>
		/// Light
		/// </summary>
		Light Light;

		#endregion

		#region Properties
		
		float _azimuth;
		/// <summary>
		/// Azimuth
		/// </summary>
		public float Azimuth
		{
			get { return _azimuth; }
			set
			{
				SetAzimuth(value);
			}
		}

		float _elevation;
		/// <summary>
		/// Elevation
		/// </summary>
		public float Elevation
		{
			get { return _elevation; }
			set
			{
				SetElevation(value);
			}
		}

		float _intensity;
		/// <summary>
		/// Intensity
		/// </summary>
		public float Intensity
		{
			get { return _intensity; }
			set
			{
				SetIntensity(value);
			}
		}

		#endregion

		#region Mutators
		
		private void SetAzimuth(float value)
		{
			if (Azimuth != value)
			{
				_azimuth = value;
				DispatchOnAzimuthChanged();
				UpdateOrientation();
			}
		}

		private void SetElevation(float value)
		{
			if (Elevation != value)
			{
				_elevation = value;
				DispatchOnElevationChanged();
				UpdateOrientation();
			}
		}

		private void SetIntensity(float value)
		{
			if (Intensity != value)
			{
				_intensity = value;
				DispatchOnIntensityChanged();
				UpdateBrightness();
			}
		}

		#endregion

		#region Lifecycle

		protected override void CollectComponents()
		{
			base.CollectComponents();

			Light = GetComponentInChildren<Light>();
		}

		/*
		protected override void CommitProperties()
		{
			UpdateOrientation();
			UpdateBrightness();
			base.CommitProperties();
		}
		*/

		private void UpdateOrientation()
		{
			if (Light != null && !float.IsNaN(Azimuth) && !float.IsNaN(Elevation))
			{
				float radAzimuth = ((Azimuth + 180) % 360) * Mathf.Deg2Rad;
				float radElevation = -Elevation * Mathf.Deg2Rad;
				SphericalCoordinates spherical = new SphericalCoordinates(1, radAzimuth, radElevation, 0, 10, 0, 2*Mathf.PI, -Mathf.PI, Mathf.PI);
				Vector3 position = transform.TransformPoint(spherical.toCartesian);

				Light.transform.LookAt(position);
			}
		}

		private void UpdateBrightness()
		{
			if (Light != null && !float.IsNaN(Intensity))
				Light.intensity = Intensity;
		}

		#endregion

		#region Dispatchers

		private void DispatchOnAzimuthChanged()
		{
			if (OnAzimuthChanged != null)
				OnAzimuthChanged(Azimuth);
		}

		private void DispatchOnElevationChanged()
		{
			if (OnElevationChanged != null)
				OnElevationChanged(Elevation);
		}

		private void DispatchOnIntensityChanged()
		{
			if (OnIntensityChanged != null)
				OnIntensityChanged(Intensity);
		}

		#endregion
	}
}
