﻿using AMonkeyMadeThis.Common.Controller;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace POSPlacement.ActivationPlacement.Scripts.View.Components
{
	/// <summary>
	/// TouchInteraction
	/// </summary>
	public class TouchInteraction : AbstractController
	{
		#region Events

		/// <summary>
		/// Delegate to handle changes in IsTouching
		/// </summary>
		/// <param name="isTouching"></param>
		public delegate void IsTouchingChanged(bool isTouching);
		/// <summary>
		/// Dispatched when IsTouching changes
		/// </summary>
		public event IsTouchingChanged OnIsTouchingChanged;
		/// <summary>
		/// Delegate to handle a select gesture
		/// </summary>
		/// <param name="position"></param>
		public delegate void Select(Vector2 position);
		/// <summary>
		/// Dispatched when a select gesture is detected
		/// </summary>
		public event Select OnSelect;
		/// <summary>
		/// Delegate to handle a hold gesture
		/// </summary>
		/// <param name="position"></param>
		public delegate void Hold(Vector2 position);
		/// <summary>
		/// Dispatched when a hold gesture is detected
		/// </summary>
		public event Hold OnHold;
		/// <summary>
		/// Delegate to handle a drag gesture
		/// </summary>
		/// <param name="position"></param>
		/// <param name="deltaPosition"></param>
		public delegate void Drag(Vector2 position, Vector2 deltaPosition);
		/// <summary>
		/// Dispatched when a drag gesture is detected
		/// </summary>
		public event Drag OnDrag;
		/// <summary>
		/// Delegate tot handle a pinch gesture
		/// </summary>
		/// <param name="scalar"></param>
		public delegate void Pinch(float scalar);
		/// <summary>
		/// Dispatched when a pinch gesture is detected
		/// </summary>
		public event Pinch OnPinch;

		#endregion

		#region Debug

		public Text DebugOutput;

		#endregion

		#region Properties

		bool _isTouching;
		/// <summary>
		/// IsTouching
		/// </summary>
		public bool IsTouching
		{
			get { return _isTouching; }
			set
			{
				SetIsTouching(value);
			}
		}

		/// <summary>
		/// EventSystem
		/// </summary>
		EventSystem EventSystem;

		#endregion

		#region Mutators

		private void SetIsTouching(bool value)
		{
			if (IsTouching != value)
			{
				_isTouching = value;
				DispatchOnIsTouchingChanged();
			}
		}

		#endregion

		#region Lifecycle

		protected override void CollectComponents()
		{
			base.CollectComponents();

			EventSystem = EventSystem.current;
		}

		void Update()
		{
			ClearDebugOutput();
			ProcessTouches();
		}

		#endregion

		#region Debug

		private void ClearDebugOutput()
		{
			if (DebugOutput != null)
				DebugOutput.text = "";
		}

		private void AddDebugOutput(string message)
		{
			if (DebugOutput != null)
				DebugOutput.text += message + "\n";
		}
		
		#endregion

		#region Touches

		private void ProcessTouches()
		{
			AddDebugOutput("Touch supported = " + Input.touchSupported);

			if (Input.touchSupported && Input.touchCount > 0)
			{
				AddDebugOutput("Touch count = " + Input.touchCount);

				// user is touching the screen, not to say it is a recognisable gesture
				IsTouching = true;
				// get all touches that arent ending or cancelled
				List<Touch> activeTouches = GetActiveTouches();

				if (activeTouches != null)
				{
					int numTouches = activeTouches.Count;

					AddDebugOutput("Active touch count = " + numTouches);

					if (numTouches == 0)
						ProcessNoFingerGesture();
					else if (numTouches == 1)
						ProcessSingleFingerGesture(activeTouches.First());
					else if (numTouches == 2)
						ProcessTwoFingerGesture(activeTouches.First(), activeTouches.Last());
					else
						ProcessFacePalmGesture();
				}
				else
				{
					AddDebugOutput("No ACTIVE touches");
				}
			}
			else
			{
				AddDebugOutput("No touches");

				IsTouching = false;
			}
		}

		private List<Touch> GetActiveTouches()
		{
			if (Input.touchSupported && Input.touchCount > 0)
			{
				Touch[] touches = Input.touches;
				List<Touch> result = new List<Touch>();

				foreach (var touch in touches)
				{
					if (!IsPointerOverUI(touch.fingerId))
					{
						TouchPhase phase = touch.phase;

						if (phase == TouchPhase.Began || phase == TouchPhase.Moved || phase == TouchPhase.Stationary)
							result.Add(touch);
						// else is ended or cancelled
					}
				}

				return result;
			}

			return null;
		}

		bool IsPointerOverUI(int fingerId)
		{
			return (EventSystem.IsPointerOverGameObject(fingerId) && EventSystem.currentSelectedGameObject != null);
		}
		
		#endregion

		#region Gesture processing

		private void ProcessNoFingerGesture()
		{
			AddDebugOutput("NoFingerGesture");

			// we could get here if the last touch was ending or cancelled, don't set IsTouching to false just yet though
		}

		private void ProcessSingleFingerGesture(Touch touch)
		{
			AddDebugOutput("SingleFingerGesture");

			if (touch.phase == TouchPhase.Began)
				GestureSelect(touch.position);
			else if (touch.phase == TouchPhase.Stationary)
				GestureHold(touch.position);
			else if (touch.phase == TouchPhase.Moved)
				GestureDrag(touch.position, touch.deltaPosition);
			else
				Debug.LogError("Unexpected single touch");
		}

		private void ProcessTwoFingerGesture(Touch first, Touch second)
		{
			AddDebugOutput("TwoFingerGesture");

			if (first.phase == TouchPhase.Moved || second.phase == TouchPhase.Moved)
			{
				Vector2 firstCurrent = first.position;
				Vector2 secondCurrent = second.position;

				Vector2 firstPrevious = first.position - first.deltaPosition;
				Vector2 secondPrevious = second.position - second.deltaPosition;

				Vector2 previousDelta = secondPrevious - firstPrevious;
				Vector2 currentDelta = secondCurrent - firstCurrent;

				float scalar = currentDelta.sqrMagnitude / Mathf.Max(Mathf.Epsilon, previousDelta.sqrMagnitude);

				GesturePinch(scalar);
			}
			else
			{
				// neither moved so ... 
				GesturePinch(1);
			}
		}

		private void ProcessFacePalmGesture()
		{
			AddDebugOutput("FacePalmGesture");

			// not a problem, just not currently looking for more than two finger (or face) gestures
		}

		#endregion
		
		#region Gestures

		Vector2 TransformPixelsToViewportSpace(Vector2 input)
		{
			Vector2 result = (Screen.width > 0 && Screen.height > 0) ? new Vector2(input.x / Screen.width, input.y / Screen.height) : Vector2.zero;

			return result;
		}

		private void GestureSelect(Vector2 position)
		{
			AddDebugOutput("GestureSelect position = " + position);

			Vector2 _position = TransformPixelsToViewportSpace(position);
			
			DispatchOnSelect(position);
		}

		private void GestureHold(Vector2 position)
		{
			AddDebugOutput("GestureHold position = " + position);

			Vector2 _position = TransformPixelsToViewportSpace(position);
			
			DispatchOnHold(_position);
		}

		private void GestureDrag(Vector2 position, Vector2 deltaPosition)
		{
			AddDebugOutput("GestureDrag position = " + position + " delta = " + deltaPosition);

			Vector2 _position = TransformPixelsToViewportSpace(position);
			
			DispatchOnDrag(_position, deltaPosition);
		}

		private void GesturePinch(float scalar)
		{
			AddDebugOutput("GesturePinch scalar = " + scalar);

			DispatchOnPinch(scalar);
		}

		#endregion

		#region Dispatchers

		private void DispatchOnIsTouchingChanged()
		{
			if (OnIsTouchingChanged != null)
				OnIsTouchingChanged(IsTouching);
		}

		private void DispatchOnSelect(Vector2 position)
		{
			if (OnSelect != null)
				OnSelect(position);
		}

		private void DispatchOnHold(Vector2 position)
		{
			if (OnHold != null)
				OnHold(position);
		}

		private void DispatchOnDrag(Vector2 position, Vector2 deltaPosition)
		{
			if (OnDrag != null)
				OnDrag(position, deltaPosition);
		}

		private void DispatchOnPinch(float scalar)
		{
			if (OnPinch != null)
				OnPinch(scalar);
		}

		#endregion
	}
}

/*
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Kudan.AR;



public class InteractionManager : MonoBehaviour {

    public static InteractionManager Instance;

    public Camera camera;

    public Color hover, selected;

    public GameObject refPlane, refPosition;

    public float scaleSpeed, rotateSpeed;
    public GameObject selectedObject, light;

    Vector3 direction;
    Vector2 touchPrevPos;

    public bool touching;

    void Awake() {
        Instance = this;
    }

    void Start() {
        TouchMode("release");
    }

    void TouchMode(string s) {
        switch (s) {
            case "touch":
                if (selectedObject != null) {
                    selectedObject.GetComponent<POS>().Selecting(selected, 0.4f);
                }
                TouchMode("touching");
                break;
            case "touching":
                touching = true;
                break;
            case "release":
                touching = false;
                break;
        }
    }

    void Update() {
        CentreRay();
        if (!EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId)) {
            Inputs();
        }
    }

    void CentreRay() {
        Ray ray = new Ray(camera.transform.position, camera.transform.forward);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit)) {
            if (!touching) {
                if (hit.collider.tag == "POS") {
                    selectedObject = hit.collider.gameObject;
                    ARManager.Instance.debugText.GetComponent<Text>().color = Color.blue;
                    ARManager.Instance.debugText.GetComponent<Text>().text = "Selecting";

                    selectedObject.GetComponent<POS>().Hovering(hover, 0.4f);
                    ARUIManager.Instance.EnableDeleteButton(true);
                }
            }
        }
        else {
            if (!touching) {

                ARManager.Instance.debugText.GetComponent<Text>().color = Color.red;
                ARManager.Instance.debugText.GetComponent<Text>().text = "Null";
                selectedObject = null;
                ARUIManager.Instance.EnableDeleteButton(false);
            }
        }
    }

    void Inputs() {
        if (Input.touchCount > 0) {
            if (Input.GetTouch(0).phase == TouchPhase.Ended) {
                TouchMode("release");
            }

            if (Input.GetTouch(0).phase == TouchPhase.Began) {
                TouchMode("touch");
                InitialiseRefPosition();
            }

            if (Input.touchCount == 1) {
                if (selectedObject != null && touching) {
                    ARManager.Instance.debugText.GetComponent<Text>().color = Color.green;
                    ARManager.Instance.debugText.GetComponent<Text>().text = "Moving";
                    Move();
                    Rotate();
                }
            }

            if (Input.touchCount == 2) {
                if (selectedObject != null) {
                    ARManager.Instance.debugText.GetComponent<Text>().color = Color.green;
                    ARManager.Instance.debugText.GetComponent<Text>().text = "Scaling";
                    Scale();
                }
            }
        }
    }

    void Scale() {
        Touch touchzero = Input.GetTouch(0);
        Touch touchOne = Input.GetTouch(1);

        Vector2 touchZeroPrevPos = touchzero.position - touchzero.deltaPosition;
        Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

        float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
        float touchDeltaMag = (touchzero.position - touchOne.position).magnitude;

        float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

        float scale = selectedObject.transform.localScale.x;
        scale -= deltaMagnitudeDiff * scaleSpeed;
        selectedObject.transform.localScale = new Vector3(scale, scale, scale);
    }

    void Rotate() {
        Touch touch = Input.GetTouch(0);
        selectedObject.transform.Rotate(0, -touch.deltaPosition.x * rotateSpeed, 0, Space.Self);
    }

    void Move() {
        selectedObject.transform.position = refPosition.transform.position;
    }

    void InitialiseRefPosition() {
        refPosition.transform.position = selectedObject.transform.position;
        refPosition.transform.LookAt(camera.transform.position);
    }

    void ResetPOSRotation() {
        if (selectedObject != null) {
            selectedObject.transform.SetParent(ARManager.Instance.markerlessParent.transform);
            selectedObject.transform.localRotation = Quaternion.Euler(0, selectedObject.transform.localRotation.y, 0);
            selectedObject.GetComponent<POS>().UpdateTransform();
        }
    }

    public void ChangeLightSource(float r) {
        light.transform.rotation = Quaternion.Euler((int)r, light.transform.rotation.y, light.transform.rotation.z);
    }

    public void DestroyObject() {
        Destroy(selectedObject);
        selectedObject = null;
    }
}


*/
