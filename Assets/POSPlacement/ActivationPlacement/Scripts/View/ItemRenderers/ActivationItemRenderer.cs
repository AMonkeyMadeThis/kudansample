﻿using AMonkeyMadeThis.Bamboo.AssetBundle;
using AMonkeyMadeThis.Bamboo.Localisation.Model;
using AMonkeyMadeThis.Bamboo.UI.Component.Data.ItemRenderers;
using POSPlacement.Common.Data.Scripts.Model.VO;
using UnityEngine.UI;

namespace POSPlacement.ActivationPlacement.Scripts.View.ItemRenderers
{
	/// <summary>
	/// ActivationItemRenderer
	/// </summary>
	public class ActivationItemRenderer : ItemRenderer
	{
		#region Properties

		/// <summary>
		/// Data cast as ActivationVO
		/// </summary>
		ActivationVO ActivationData
		{
			get { return Data as ActivationVO; }
		}

		#endregion

		#region Skin parts

		/// <summary>
		/// NameLabel
		/// </summary>
		public Text NameLabel;
		/// <summary>
		/// NameLabel
		/// </summary>
		public Image IconImage;

		#endregion

		#region Lifecycle

		protected override void PopulateUI()
		{
			base.PopulateUI();

			populateName();
			populateIcon();
		}

		private void populateIcon()
		{
			if (ActivationData != null && IconImage != null)
			{
				var prefabAsset = ActivationData.Asset as PrefabAsset;

				if (prefabAsset != null && prefabAsset.Icon != null)
					IconImage.sprite = prefabAsset.Icon;
			}
		}

		private void populateName()
		{
			if (ActivationData != null && NameLabel != null)
			{
				string name = LocalisationModel.Instance.LocaliseString(ActivationData.Name, ResourceBundle);
				NameLabel.text = name;
			}
		}

		#endregion
	}
}
