﻿using AMonkeyMadeThis.Common.Controller;
using POSPlacement.AreaSelection.Scripts.Model;
using POSPlacement.Common.Data.Scripts.Model;
using POSPlacement.Common.Data.Scripts.Model.VO;
using POSPlacement.Common.Scripts.Application.Controller;
using POSPlacement.Common.Scripts.Application.Model;
using UnityEngine;

namespace POSPlacement.AreaSelection.Scripts.Controller
{
	/// <summary>
	/// AreaSelectionViewController
	/// </summary>
	public class AreaSelectionViewController : AbstractController
	{
		#region Properties

		/// <summary>
		/// ApplicationController
		/// </summary>
		ApplicationController ApplicationController;
		/// <summary>
		/// ApplicationModel
		/// </summary>
		ApplicationModel ApplicationModel;
		/// <summary>
		/// ApplicationDataModel
		/// </summary>
		ApplicationDataModel ApplicationDataModel;
		/// <summary>
		/// AreaSelectionViewModel
		/// </summary>
		AreaSelectionViewModel AreaSelectionViewModel;

		#endregion

		#region Lifecycle

		protected override void CollectComponents()
		{
			base.CollectComponents();

			ApplicationController = ApplicationController.Instance;
			ApplicationModel = ApplicationModel.Instance;
			ApplicationDataModel = ApplicationDataModel.Instance;
			AreaSelectionViewModel = GetComponent<AreaSelectionViewModel>();
		}

		public override void Start()
		{
			base.Start();

			PopulateModel();
		}

		#endregion

		#region Data

		private void PopulateModel()
		{
			if (ApplicationModel != null && ApplicationModel.CurrentBrand != null && ApplicationModel.CurrentBrand.Areas.Count > 0 && ApplicationDataModel.Areas != null && ApplicationDataModel.Areas.Count > 0)
			{
				if (AreaSelectionViewModel != null)
					AreaSelectionViewModel.Areas = ApplicationDataModel.GetAreas(ApplicationModel.CurrentBrand.Areas);
				else
					Debug.LogError("Could not find RegionSelectionViewModel");
			}
			else
			{
				Debug.LogError("ApplicationModel.CurrentBrand.Areas should be populated at this point");
			}
		}
		
		public void SelectArea(AreaVO area)
		{
			if (ApplicationModel != null)
				ApplicationModel.CurrentArea = area;
			else
				Debug.LogError("Could not find ApplicationModel");
		}

		public void SelectBrand(BrandVO brand)
		{
			if (ApplicationModel != null)
				ApplicationModel.CurrentBrand = brand;
			else
				Debug.LogError("Could not find ApplicationModel");
		}

		#endregion

		#region Navigation

		public void GoBack()
		{
			ApplicationController.GoBack();
		}

		#endregion
	}
}
