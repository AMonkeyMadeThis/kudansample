﻿using AMonkeyMadeThis.Common.Model;
using POSPlacement.Common.Data.Scripts.Model.VO;
using System.Collections.Generic;

namespace POSPlacement.AreaSelection.Scripts.Model
{
	/// <summary>
	/// AreaSelectionViewModel
	/// </summary>
	public class AreaSelectionViewModel : AbstractModel
	{
		#region Events

		/// <summary>
		/// Delegate to handle changes in areas
		/// </summary>
		/// <param name="areas"></param>
		public delegate void AreasChanged(List<AreaVO> areas);
		/// <summary>
		/// Dispatched when areas change
		/// </summary>
		public event AreasChanged OnAreasChanged;

		#endregion

		#region Properties

		List<AreaVO> _areas;
		/// <summary>
		/// Areas
		/// </summary>
		public List<AreaVO> Areas
		{
			get { return _areas; }
			set
			{
				SetAreas(value);
			}
		}
		
		#endregion

		#region Mutators

		private void SetAreas(List<AreaVO> value)
		{
			_areas = value;
			DispatchOnAreasChanged();
		}

		#endregion

		#region Dispatchers

		private void DispatchOnAreasChanged()
		{
			if (OnAreasChanged != null)
				OnAreasChanged(Areas);
		}

		#endregion
	}
}
