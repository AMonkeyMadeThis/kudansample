﻿using AMonkeyMadeThis.Bamboo.Localisation.Controller;
using AMonkeyMadeThis.Bamboo.UI.Component.Data;
using AMonkeyMadeThis.Bamboo.UI.Component.Data.ItemRenderers;
using AMonkeyMadeThis.Common.Model.VO;
using AMonkeyMadeThis.Common.View;
using POSPlacement.AreaSelection.Scripts.Controller;
using POSPlacement.AreaSelection.Scripts.Model;
using POSPlacement.Common.Data.Scripts.Model.VO;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace POSPlacement.AreaSelection.Scripts.View
{
	/// <summary>
	/// AreaSelectionView
	/// </summary>
	public class AreaSelectionView : AbstractView
	{
		#region Consts

		/// <summary>
		/// RESOURCE_BUNDLE_NAME_AREAS
		/// </summary>
		const string RESOURCE_BUNDLE_NAME_AREAS = "Areas";

		#endregion

		#region Properties

		/// <summary>
		/// AreaItemRendererPrototype
		/// </summary>
		public ItemRenderer AreaItemRendererPrototype;
		/// <summary>
		/// AreaSelectionViewModel
		/// </summary>
		AreaSelectionViewModel AreaSelectionViewModel;
		/// <summary>
		/// AreaSelectionViewController
		/// </summary>
		AreaSelectionViewController AreaSelectionViewController;

		#endregion

		#region Skin parts

		/// <summary>
		/// AreasDataGroup
		/// </summary>
		public DataGroup AreasDataGroup;

		#endregion

		#region Lifecycle

		protected override void InitVars()
		{
			base.InitVars();

			SetAreaSelectionViewModelItemRendererFunction();
		}

		protected override void CollectComponents()
		{
			base.CollectComponents();

			AreaSelectionViewController = GetComponent<AreaSelectionViewController>();
			AreaSelectionViewModel = GetComponent<AreaSelectionViewModel>();
		}

		public override void Start()
		{
			base.Start();

			if (AreaSelectionViewModel != null)
				PopulateAreasList(AreaSelectionViewModel.Areas);
		}

		#endregion

		#region Screen rotation

		protected override void ConfigureScreenRotation()
		{
			Screen.autorotateToPortrait = true;
			Screen.autorotateToPortraitUpsideDown = true;
			Screen.autorotateToLandscapeLeft = true;
			Screen.autorotateToLandscapeRight = true;
		}

		#endregion

		#region Navigation

		public override void GoBack()
		{
			AreaSelectionViewController.GoBack();
		}

		#endregion

		#region Data group

		private void PopulateAreasList(List<AreaVO> regions)
		{
			if (AreasDataGroup != null)
			{
				var alphabeticallySorted = regions.OrderBy(go => LocalisationController.Instance.LocaliseString(go.Name, RESOURCE_BUNDLE_NAME_AREAS));

				AreasDataGroup.dataSource = alphabeticallySorted.ToArray();
			}
		}

		#endregion

		#region Item renderers

		private void SetAreaSelectionViewModelItemRendererFunction()
		{
			if (AreasDataGroup != null)
				AreasDataGroup.ItemRendererFunction = RegionsDataGroupItemRendererFunction;
		}

		/// <summary>
		/// Select an appropriate item renderer for the value object, we know this will only be getting
		/// instances of AreaVO (unless something has gone badly wrong) but the DataGroup is generic
		/// and only really cares if it is given descendents of ValueObjects to render with ItemRenderers.
		/// </summary>
		/// <param name="item">base class for any data, this method will inspec its type and select a specific ItemRenderer</param>
		/// <returns></returns>
		private ItemRenderer RegionsDataGroupItemRendererFunction(ValueObject item)
		{
			if (item is AreaVO)
				return AreaItemRendererPrototype;

			// we could give a 'DefaultItemRenderer' but we don't want anything else showing up in the list, not that it could / should
			return null;
		}
		
		#endregion

		#region Listeners

		public override void AddListeners()
		{
			base.AddListeners();

			AddListeners(AreaSelectionViewModel);
			AddListeners(AreasDataGroup);
		}

		private void AddListeners(AreaSelectionViewModel areaSelectionViewModel)
		{
			if (areaSelectionViewModel != null)
			{
				areaSelectionViewModel.OnAreasChanged += HandleAreaSelectionViewModelOnRegionsChanged;
			}
		}

		private void AddListeners(DataGroup areasDataGroup)
		{
			if (areasDataGroup != null)
			{
				areasDataGroup.OnItemSelected += HandleAreasDataGroupOnItemSelected;
			}
		}

		public override void RemoveListeners()
		{
			base.RemoveListeners();

			RemoveListeners(AreaSelectionViewModel);
			RemoveListeners(AreasDataGroup);
		}

		private void RemoveListeners(AreaSelectionViewModel areaSelectionViewModel)
		{
			if (areaSelectionViewModel != null)
			{
				areaSelectionViewModel.OnAreasChanged -= HandleAreaSelectionViewModelOnRegionsChanged;
			}
		}

		private void RemoveListeners(DataGroup areasDataGroup)
		{
			if (areasDataGroup != null)
			{
				areasDataGroup.OnItemSelected -= HandleAreasDataGroupOnItemSelected;
			}
		}

		#endregion

		#region Handlers

		private void HandleAreaSelectionViewModelOnRegionsChanged(List<AreaVO> areas)
		{
			PopulateAreasList(areas);
		}

		private void HandleAreasDataGroupOnItemSelected(ItemRenderer itemRenderer)
		{
			if (itemRenderer != null)
			{
				var area = itemRenderer.Data as AreaVO;

				if (area != null)
					AreaSelectionViewController.SelectArea(area);
			}
		}

		#endregion
	}
}
