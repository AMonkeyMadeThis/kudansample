﻿using AMonkeyMadeThis.Bamboo.Localisation.Model;
using AMonkeyMadeThis.Bamboo.UI.Component.Data.ItemRenderers;
using POSPlacement.Common.Data.Scripts.Model.VO;
using UnityEngine.UI;

namespace POSPlacement.AreaSelection.Scripts.View.ItemRenderers
{
	/// <summary>
	/// AreaItemRenderer
	/// </summary>
	public class AreaItemRenderer : ItemRenderer
	{
		#region Properties

		/// <summary>
		/// Data cast as AreaVO
		/// </summary>
		AreaVO AreaData
		{
			get { return Data as AreaVO; }
		}

		#endregion

		#region Skin parts

		/// <summary>
		/// NameLabel
		/// </summary>
		public Text NameLabel;

		#endregion

		#region Lifecycle

		protected override void PopulateUI()
		{
			base.PopulateUI();

			populateName();
		}

		private void populateName()
		{
			string name = LocalisationModel.Instance.LocaliseString(AreaData.Name, ResourceBundle);
			NameLabel.text = name;
		}

		#endregion
	}
}
