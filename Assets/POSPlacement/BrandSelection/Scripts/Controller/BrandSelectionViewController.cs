﻿using AMonkeyMadeThis.Common.Controller;
using POSPlacement.BrandSelection.Scripts.Model;
using POSPlacement.Common.Data.Scripts.Model;
using POSPlacement.Common.Data.Scripts.Model.VO;
using POSPlacement.Common.Scripts.Application.Controller;
using POSPlacement.Common.Scripts.Application.Model;
using UnityEngine;

namespace POSPlacement.BrandSelection.Scripts.Controller
{
	/// <summary>
	/// BrandSelectionViewController
	/// </summary>
	public class BrandSelectionViewController : AbstractController
	{
		#region Properties

		/// <summary>
		/// ApplicationController
		/// </summary>
		ApplicationController ApplicationController;
		/// <summary>
		/// ApplicationModel
		/// </summary>
		ApplicationModel ApplicationModel;
		/// <summary>
		/// ApplicationDataModel
		/// </summary>
		ApplicationDataModel ApplicationDataModel;
		/// <summary>
		/// BrandSelectionViewModel
		/// </summary>
		BrandSelectionViewModel BrandSelectionViewModel;

		#endregion

		#region Lifecycle

		protected override void CollectComponents()
		{
			base.CollectComponents();

			ApplicationController = ApplicationController.Instance;
			ApplicationModel = ApplicationModel.Instance;
			ApplicationDataModel = ApplicationDataModel.Instance;
			BrandSelectionViewModel = GetComponent<BrandSelectionViewModel>();
		}

		public override void Start()
		{
			base.Start();

			PopulateModel();
		}

		#endregion

		#region Navigation

		public void GoBack()
		{
			ApplicationController.GoBack();
		}

		#endregion

		#region Data

		private void PopulateModel()
		{
			if (ApplicationModel != null && ApplicationModel.CurrentRegion != null && ApplicationModel.CurrentRegion.Brands.Count > 0 && ApplicationDataModel.Brands != null && ApplicationDataModel.Brands.Count > 0)
			{
				if (BrandSelectionViewModel != null)
					BrandSelectionViewModel.Brands = ApplicationDataModel.GetBrands(ApplicationModel.CurrentRegion.Brands);
				else
					Debug.LogError("Could not find RegionSelectionViewModel");
			}
			else
			{
				Debug.LogError("ApplicationModel.CurrentRegion.Brands should be populated at this point");
			}
		}
		
		public void SelectBrand(BrandVO brand)
		{
			if (ApplicationModel != null)
				ApplicationModel.CurrentBrand = brand;
			else
				Debug.LogError("Could not find ApplicationModel");
		}

		#endregion
	}
}
