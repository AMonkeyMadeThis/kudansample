﻿using AMonkeyMadeThis.Common.Model;
using POSPlacement.Common.Data.Scripts.Model.VO;
using System.Collections.Generic;

namespace POSPlacement.BrandSelection.Scripts.Model
{
	/// <summary>
	/// BrandSelectionViewModel
	/// </summary>
	public class BrandSelectionViewModel : AbstractModel
	{
		#region Events

		/// <summary>
		/// Delegate to handle changes in brands
		/// </summary>
		/// <param name="brands"></param>
		public delegate void BrandsChanged(List<BrandVO> brands);
		/// <summary>
		/// Dispatched when brands change
		/// </summary>
		public event BrandsChanged OnBrandsChanged;

		#endregion

		#region Properties

		List<BrandVO> _brands;
		/// <summary>
		/// Regions
		/// </summary>
		public List<BrandVO> Brands
		{
			get { return _brands; }
			set
			{
				SetBrands(value);
			}
		}
		
		#endregion

		#region Mutators

		private void SetBrands(List<BrandVO> value)
		{
			_brands = value;
			DispatchOnBrandsChanged();
		}

		#endregion

		#region Dispatchers

		private void DispatchOnBrandsChanged()
		{
			if (OnBrandsChanged != null)
				OnBrandsChanged(Brands);
		}

		#endregion
	}
}
