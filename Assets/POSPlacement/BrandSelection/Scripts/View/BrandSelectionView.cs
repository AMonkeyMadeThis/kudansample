﻿using AMonkeyMadeThis.Bamboo.Localisation.Controller;
using AMonkeyMadeThis.Bamboo.UI.Component.Data;
using AMonkeyMadeThis.Bamboo.UI.Component.Data.ItemRenderers;
using AMonkeyMadeThis.Common.Model.VO;
using AMonkeyMadeThis.Common.View;
using POSPlacement.BrandSelection.Scripts.Controller;
using POSPlacement.BrandSelection.Scripts.Model;
using POSPlacement.Common.Data.Scripts.Model.VO;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace POSPlacement.BrandSelection.Scripts.View
{
	/// <summary>
	/// BrandSelectionView
	/// </summary>
	public class BrandSelectionView : AbstractView
	{
		#region Consts

		/// <summary>
		/// RESOURCE_BUNDLE_NAME_BRANDS
		/// </summary>
		const string RESOURCE_BUNDLE_NAME_BRANDS = "Brands";

		#endregion

		#region Properties

		/// <summary>
		/// BrandItemRendererPrototype
		/// </summary>
		public ItemRenderer BrandItemRendererPrototype;
		/// <summary>
		/// BrandSelectionViewModel
		/// </summary>
		BrandSelectionViewModel BrandSelectionViewModel;
		/// <summary>
		/// BrandSelectionViewController
		/// </summary>
		BrandSelectionViewController BrandSelectionViewController;

		#endregion

		#region Skin parts

		/// <summary>
		/// BrandsDataGroup
		/// </summary>
		public DataGroup BrandsDataGroup;

		#endregion

		#region Lifecycle

		protected override void InitVars()
		{
			base.InitVars();

			SetBrandSelectionViewModelItemRendererFunction();
		}

		protected override void CollectComponents()
		{
			base.CollectComponents();

			BrandSelectionViewController = GetComponent<BrandSelectionViewController>();
			BrandSelectionViewModel = GetComponent<BrandSelectionViewModel>();
		}

		public override void Start()
		{
			base.Start();

			if (BrandSelectionViewModel != null)
				PopulateBrandsList(BrandSelectionViewModel.Brands);
		}

		#endregion

		#region Screen rotation

		protected override void ConfigureScreenRotation()
		{
			Screen.autorotateToPortrait = true;
			Screen.autorotateToPortraitUpsideDown = true;
			Screen.autorotateToLandscapeLeft = true;
			Screen.autorotateToLandscapeRight = true;
		}

		#endregion

		#region Navigation

		public override void GoBack()
		{
			BrandSelectionViewController.GoBack();
		}

		#endregion

		#region Data group

		private void PopulateBrandsList(List<BrandVO> regions)
		{
			if (BrandsDataGroup != null)
			{
				var alphabeticallySorted = regions.OrderBy(go => LocalisationController.Instance.LocaliseString(go.Name, RESOURCE_BUNDLE_NAME_BRANDS));

				BrandsDataGroup.dataSource = alphabeticallySorted.ToArray();
			}
		}

		#endregion

		#region Item renderers

		private void SetBrandSelectionViewModelItemRendererFunction()
		{
			if (BrandsDataGroup != null)
				BrandsDataGroup.ItemRendererFunction = RegionsDataGroupItemRendererFunction;
		}

		/// <summary>
		/// Select an appropriate item renderer for the value object, we know this will only be getting
		/// instances of BrandVO (unless something has gone badly wrong) but the DataGroup is generic
		/// and only really cares if it is given descendents of ValueObjects to render with ItemRenderers.
		/// </summary>
		/// <param name="item">base class for any data, this method will inspec its type and select a specific ItemRenderer</param>
		/// <returns></returns>
		private ItemRenderer RegionsDataGroupItemRendererFunction(ValueObject item)
		{
			if (item is BrandVO)
				return BrandItemRendererPrototype;

			// we could give a 'DefaultItemRenderer' but we don't want anything else showing up in the list, not that it could / should
			return null;
		}
		
		#endregion

		#region Listeners

		public override void AddListeners()
		{
			base.AddListeners();

			AddListeners(BrandSelectionViewModel);
			AddListeners(BrandsDataGroup);
		}

		private void AddListeners(BrandSelectionViewModel brandSelectionViewModel)
		{
			if (brandSelectionViewModel != null)
			{
				brandSelectionViewModel.OnBrandsChanged += HandleBrandSelectionViewModelOnRegionsChanged;
			}
		}

		private void AddListeners(DataGroup regionsDataGroup)
		{
			if (regionsDataGroup != null)
			{
				regionsDataGroup.OnItemSelected += HandleBrandsDataGroupOnItemSelected;
			}
		}

		public override void RemoveListeners()
		{
			base.RemoveListeners();

			RemoveListeners(BrandSelectionViewModel);
		}

		private void RemoveListeners(BrandSelectionViewModel regionSelectionViewModel)
		{
			if (regionSelectionViewModel != null)
			{
				regionSelectionViewModel.OnBrandsChanged -= HandleBrandSelectionViewModelOnRegionsChanged;
			}
		}

		#endregion

		#region Handlers

		private void HandleBrandSelectionViewModelOnRegionsChanged(List<BrandVO> brands)
		{
			PopulateBrandsList(brands);
		}

		private void HandleBrandsDataGroupOnItemSelected(ItemRenderer itemRenderer)
		{
			if (itemRenderer != null)
			{
				var brand = itemRenderer.Data as BrandVO;

				if (brand != null)
					BrandSelectionViewController.SelectBrand(brand);
			}
		}

		#endregion
	}
}
