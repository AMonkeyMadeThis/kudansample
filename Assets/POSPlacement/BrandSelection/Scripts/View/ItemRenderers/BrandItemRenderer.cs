﻿using AMonkeyMadeThis.Bamboo.Localisation.Model;
using AMonkeyMadeThis.Bamboo.UI.Component.Data.ItemRenderers;
using POSPlacement.Common.Data.Scripts.Model.VO;
using UnityEngine.UI;

namespace POSPlacement.BrandSelection.Scripts.View.ItemRenderers
{
	/// <summary>
	/// BrandItemRenderer
	/// </summary>
	public class BrandItemRenderer : ItemRenderer
	{
		#region Properties

		/// <summary>
		/// Data cast as BrandVO
		/// </summary>
		BrandVO BrandData
		{
			get { return Data as BrandVO; }
		}

		#endregion

		#region Skin parts

		/// <summary>
		/// NameLabel
		/// </summary>
		public Text NameLabel;

		#endregion

		#region Lifecycle

		protected override void PopulateUI()
		{
			base.PopulateUI();

			populateName();
		}

		private void populateName()
		{
			string name = LocalisationModel.Instance.LocaliseString(BrandData.Name, ResourceBundle);
			NameLabel.text = name;
		}

		#endregion
	}
}
