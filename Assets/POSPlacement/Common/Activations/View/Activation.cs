﻿using POSPlacement.Common.Activations.View.Parts;
using POSPlacement.Common.Highlighting.View;
using UnityEngine;

namespace POSPlacement.Common.Activations.View
{
	/// <summary>
	/// Activation
	/// </summary>
	public class Activation : InteractiveObject
	{
		#region Events

		/// <summary>
		/// Delegate to handle a change in height (some are adjustable)
		/// </summary>
		/// <param name="height"></param>
		public delegate void HeightChanged(float height);
		/// <summary>
		/// Dispatched when height changes
		/// </summary>
		public event HeightChanged OnHeightChanged;

		/// <summary>
		/// Delegate to handle a change in 'adjustment'
		/// </summary>
		/// <param name="adjustment"></param>
		public delegate void AdjustmentChanged(float adjustment);
		/// <summary>
		/// Dispatched when 'adjustment' changes
		/// </summary>
		public event AdjustmentChanged OnAdjustmentChanged;

		#endregion

		#region Parts

		/// <summary>
		/// AdjustableParts
		/// </summary>
		AdjustablePart[] AdjustableParts;

		#endregion

		#region Properties

		/// <summary>
		/// IsAdjustable
		/// </summary>
		public bool IsAdjustable = false;

		[SerializeField]
		[Range(0, 1)]
		float _adjustment = 0;
		/// <summary>
		/// Adjustment
		/// </summary>
		public float Adjustment
		{
			get { return _adjustment; }
			set
			{
				SetAdjustment(value);
			}
		}

		[SerializeField]
		float _height;
		/// <summary>
		/// Height
		/// </summary>
		public float Height
		{
			get { return _height; }
			set
			{
				SetHeight(value);
			}
		}
		
		#endregion
		
		#region Mutators

		private void SetHeight(float value)
		{
			_height = value;
			DispatchOnHeightChanged();
		}

		private void SetAdjustment(float value)
		{
			if (Adjustment != value)
			{
				_adjustment = value;
				Invalidate();
				DispatchOnAdjustmentChanged();
			}
		}

		#endregion

		#region Lifecycle

		protected override void CollectComponents()
		{
			base.CollectComponents();

			AdjustableParts = GetComponentsInChildren<AdjustablePart>();
		}

		protected override void CommitProperties()
		{
			UpdateAdjustment();
			base.CommitProperties();
		}

		#endregion

		#region Adjustment

		private void UpdateAdjustment()
		{
			// Note: we may be 'adjustable' but not IsAdjustable. EG- no opening even a virtual umbrella indoors ;p
			if (IsAdjustable && AdjustableParts != null && AdjustableParts.Length > 0)
			{
				foreach(var part in AdjustableParts)
				{
					if (part != null)
						part.Adjustment = Adjustment;
				}
			}
		}

		#endregion

		#region Dispatchers

		private void DispatchOnHeightChanged()
		{
			if (OnHeightChanged != null)
				OnHeightChanged(Height);
		}

		private void DispatchOnAdjustmentChanged()
		{
			if (OnAdjustmentChanged != null)
				OnAdjustmentChanged(Adjustment);
		}

		#endregion
	}
}
