﻿using AMonkeyMadeThis.Common.Behaviour;
using UnityEngine;

namespace POSPlacement.Common.Activations.View.Parts
{
	/// <summary>
	/// AdjustablePart
	/// </summary>
	public abstract class AdjustablePart : InvalidatingBehaviour
	{
		#region Events

		/// <summary>
		/// Delegate to handle a change in 'adjustment'
		/// </summary>
		/// <param name="adjustment"></param>
		public delegate void AdjustmentChanged(float adjustment);
		/// <summary>
		/// Dispatched when 'adjustment' changes
		/// </summary>
		public event AdjustmentChanged OnAdjustmentChanged;

		#endregion

		#region Properties

		[SerializeField]
		[Range(0, 1)]
		float _adjustment = 0;
		/// <summary>
		/// Adjustment
		/// </summary>
		public float Adjustment
		{
			get { return _adjustment; }
			set
			{
				SetAdjustment(value);
			}
		}

		#endregion

		#region Mutators

		private void SetAdjustment(float value)
		{
			float _value = Mathf.Clamp01(value);

			if (Adjustment != _value)
			{
				_adjustment = _value;
				Invalidate();
				DispatchOnAdjustmentChanged();
			}
		}

		#endregion

		#region Lifecycle

		protected override void CommitProperties()
		{
			UpdateAdjustment();
			base.CommitProperties();
		}

		#endregion

		#region Adjustment

		/// <summary>
		/// UpdateAdjustment is abstract to force sub-classes to implement their own form of adjustment
		/// </summary>
		protected abstract void UpdateAdjustment();
		
		#endregion

		#region Dispatchers

		private void DispatchOnAdjustmentChanged()
		{
			if (OnAdjustmentChanged != null)
				OnAdjustmentChanged(Adjustment);
		}

		#endregion

		#region Editor

		public override void OnValidate()
		{
			base.OnValidate();
			UpdateAdjustment();
		}

		#endregion
	}
}
