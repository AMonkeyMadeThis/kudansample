﻿using UnityEngine;

namespace POSPlacement.Common.Activations.View.Parts
{
	/// <summary>
	/// RotatingAdjustablePart
	/// </summary>
	public class RotatingAdjustablePart : AdjustablePart
	{
		#region Properties

		/// <summary>
		/// MinRotation
		/// </summary>
		public Vector3 MinRotation;
		/// <summary>
		/// MaxRotation
		/// </summary>
		public Vector3 MaxRotation;

		#endregion

		#region Adjustment

		/// <summary>
		/// UpdateAdjustment
		/// </summary>
		protected override void UpdateAdjustment()
		{
			Vector3 delta = MaxRotation - MinRotation;
			Vector3 rotation = MinRotation + delta * Adjustment;

			transform.localRotation = Quaternion.Euler(rotation);
		}

		#endregion

		#region Helpers

		/// <summary>
		/// SetMinRotation
		/// </summary>
		[ContextMenu("Set MinRotation")]
		void SetMinRotation()
		{
			MinRotation = transform.localRotation.eulerAngles;
		}

		/// <summary>
		/// SetMaxRotation
		/// </summary>
		[ContextMenu("Set MaxRotation")]
		void SetMaxRotation()
		{
			MaxRotation = transform.localRotation.eulerAngles;
		}

		/// <summary>
		/// GoToMinRotation
		/// </summary>
		[ContextMenu("GoTo MinRotation")]
		void GoToMinRotation()
		{
			transform.localRotation = Quaternion.Euler(MinRotation);
		}

		/// <summary>
		/// GoToMaxRotation
		/// </summary>
		[ContextMenu("GoTo MaxRotation")]
		void GoToMaxRotation()
		{
			transform.localRotation = Quaternion.Euler(MaxRotation);
		}

		#endregion
	}
}
