﻿using UnityEngine;

namespace POSPlacement.Common.Activations.View.Parts
{
	/// <summary>
	/// TranslatingAdjustablePart
	/// </summary>
	public class TranslatingAdjustablePart : AdjustablePart
	{
		#region Properties

		/// <summary>
		/// MinPosition
		/// </summary>
		public Vector3 MinPosition;
		/// <summary>
		/// MaxPosition
		/// </summary>
		public Vector3 MaxPosition;

		#endregion

		#region Adjustment

		/// <summary>
		/// UpdateAdjustment
		/// </summary>
		protected override void UpdateAdjustment()
		{
			Vector3 delta = MaxPosition - MinPosition;
			Vector3 position = MinPosition + delta * Adjustment;

			transform.localPosition = position;
		}

		#endregion

		#region Helpers

		/// <summary>
		/// SetMinPosition
		/// </summary>
		[ContextMenu("Set MinPosition")]
		void SetMinPosition()
		{
			MinPosition = transform.localPosition;
		}

		/// <summary>
		/// SetMaxPosition
		/// </summary>
		[ContextMenu("Set MaxPosition")]
		void SetMaxPosition()
		{
			MaxPosition = transform.localPosition;
		}

		/// <summary>
		/// GoToMinPosition
		/// </summary>
		[ContextMenu("GoTo MinPosition")]
		void GoToMinPosition()
		{
			transform.localPosition = MinPosition;
		}

		/// <summary>
		/// GoToMaxPosition
		/// </summary>
		[ContextMenu("GoTo MaxPosition")]
		void GoToMaxPosition()
		{
			transform.localPosition = MaxPosition;
		}

		#endregion
	}
}
