﻿using AMonkeyMadeThis.Bamboo.Network;
using AMonkeyMadeThis.Common.Controller;
using POSPlacement.Common.AssetBundle.Model;
using POSPlacement.Common.AssetBundle.Service;
using POSPlacement.Common.AssetBundle.Service.Outcome;
using POSPlacement.Common.Data.Scripts.Model;
using POSPlacement.Common.Data.Scripts.Model.VO;
using System.Collections.Generic;
using UnityEngine;

namespace POSPlacement.Common.AssetBundle.Controller
{
	/// <summary>
	/// AssetBundleController
	/// </summary>
	public class AssetBundleController : AbstractController
	{
		#region Singleton

		private static AssetBundleController _instance;
		/// <summary>
		/// Instance
		/// </summary>
		public static AssetBundleController Instance
		{
			get { return _instance; }
			private set
			{
				_instance = value;
			}
		}

		#endregion

		#region Events

		/// <summary>
		/// Delegate to handle an error
		/// </summary>
		public delegate void Error(string message);
		/// <summary>
		/// Dispatched when an error occurs
		/// </summary>
		public event Error OnError;

		#endregion

		#region Properties

		/// <summary>
		/// ApplicationDataModel
		/// </summary>
		ApplicationDataModel ApplicationDataModel;
		/// <summary>
		/// AssetBundleModel
		/// </summary>
		AssetBundleModel AssetBundleModel;
		/// <summary>
		/// AssetBundleService
		/// </summary>
		AssetBundleService AssetBundleService;

		#endregion

		#region Constructor

		/// <summary>
		/// AssetBundleController
		/// </summary>
		public AssetBundleController()
		{
			Instance = this;
		}

		#endregion

		#region Lifecycle

		protected override void CollectComponents()
		{
			base.CollectComponents();

			ApplicationDataModel = ApplicationDataModel.Instance;
			AssetBundleModel = AssetBundleModel.Instance;
			AssetBundleService = GetComponent<AssetBundleService>();
		}

		#endregion

		#region Listeners

		public override void AddListeners()
		{
			base.AddListeners();

			AddListeners(ApplicationDataModel);
		}

		private void AddListeners(ApplicationDataModel applicationDataModel)
		{
			if (ApplicationDataModel != null)
			{
				applicationDataModel.OnAssetBundlesChanged += HandleApplicationDataModelOnAssetBundlesChanged;
			}
		}

		public override void RemoveListeners()
		{
			base.RemoveListeners();

			RemoveListeners(ApplicationDataModel);
		}

		private void RemoveListeners(ApplicationDataModel applicationDataModel)
		{
			if (ApplicationDataModel != null)
			{
				applicationDataModel.OnAssetBundlesChanged -= HandleApplicationDataModelOnAssetBundlesChanged;
			}
		}

		#endregion

		#region Handlers

		private void HandleApplicationDataModelOnAssetBundlesChanged(List<AssetBundleVO> assetBundles)
		{
			//LoadAssets(assetBundles);
		}

		#endregion

		#region Loading

		public AssetBundleService LoadAssets(List<AssetBundleVO> assetBundles)
		{
			Responder responder = new Responder(LoadAssetsResult, LoadAssetsFault);
			AssetBundleService.LoadAssets(assetBundles, responder);

			return AssetBundleService;
		}

		private void LoadAssetsResult(object outcome)
		{
			var result = outcome as AssetBundleServiceResult;

			if (result != null)
			{
				AssetBundleModel.AddAssets(result.Assets);
			}
			else
			{
				DispatchOnError("Unexpected result downloading assets");
			}
		}

		private void LoadAssetsFault(object outcome)
		{
			var fault = outcome as AssetBundleServiceFault;

			if (fault != null)
			{
				string pattern = "Failed to download {0}, due to {1}.";
				string message = string.Format(pattern, fault.Source, fault.Reason);

				DispatchOnError(message);
			}
			else
			{
				DispatchOnError("Unexpected fault downloading assets");
			}
		}

		#endregion

		#region Dispatchers

		private void DispatchOnError(string message)
		{
			if (OnError != null)
				OnError(message);
		}

		#endregion
	}
}
