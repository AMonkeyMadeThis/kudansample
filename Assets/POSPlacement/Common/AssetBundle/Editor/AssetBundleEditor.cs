﻿using System.IO;
using UnityEditor;

namespace POSPlacement.Common.AssetBundle.Editor
{
	public class AssetBundleEditor
	{
		const string AssetBundlesOutputPath = "AssetBundles";
		const string GetPlatformName = "Android";

		/// <summary>
		/// BuildAssetBundles
		/// </summary>
		[MenuItem("Assets/AssetBundles/Build AssetBundles")]
		static public void BuildAssetBundles()
		{
			// Choose the output path according to the build target.
			string outputPath = Path.Combine(AssetBundlesOutputPath, GetPlatformName);
			if (!Directory.Exists(outputPath))
				Directory.CreateDirectory(outputPath);

			//@TODO: use append hash... (Make sure pipeline works correctly with it.)
			BuildPipeline.BuildAssetBundles(outputPath, BuildAssetBundleOptions.None, EditorUserBuildSettings.activeBuildTarget);
		}
	}
}
