﻿using System;

namespace POSPlacement.Common.AssetBundle.Enum
{
	/// <summary>
	/// AssetBundleType
	/// </summary>
	[Flags]
	public enum AssetBundleType
	{
		NA,
		Mixed,
		Locale,
		Activation,
		Media
	}
}
