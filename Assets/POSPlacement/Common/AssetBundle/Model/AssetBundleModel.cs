﻿using AMonkeyMadeThis.Bamboo.AssetBundle;
using AMonkeyMadeThis.Common.Model;
using System.Collections.Generic;

namespace POSPlacement.Common.AssetBundle.Model
{
	/// <summary>
	/// AssetBundleModel
	/// </summary>
	public class AssetBundleModel : AbstractModel
	{
		#region Singleton

		private static AssetBundleModel _instance;
		/// <summary>
		/// Instance
		/// </summary>
		public static AssetBundleModel Instance
		{
			get { return _instance; }
			private set
			{
				_instance = value;
			}
		}

		#endregion

		#region Events

		/// <summary>
		/// Delegate to handle a change in assets
		/// </summary>
		/// <param name="assets"></param>
		public delegate void AssetsChanged(List<IAsset> assets);
		/// <summary>
		/// Dispatched when assets change
		/// </summary>
		public event AssetsChanged OnAssetsChanged;

		#endregion

		#region Constructor

		/// <summary>
		/// AssetBundleModel
		/// </summary>
		public AssetBundleModel()
		{
			Instance = this;
		}

		#endregion

		#region Properties
		
		List<IAsset> _assets;
		/// <summary>
		/// Assets
		/// </summary>
		public List<IAsset> Assets
		{
			get { return _assets; }
			private set
			{
				SetAssets(value);
			}
		}

		#endregion

		#region Mutators

		public void AddAsset(IAsset asset)
		{
			if (!Assets.Contains(asset))
			{
				List<IAsset> assets = (Assets != null) ? Assets : new List<IAsset>();
				assets.Add(asset);
				Assets = assets;
			}
		}

		public void AddAssets(List<IAsset> assets)
		{
			if (assets != null && assets.Count > 0)
			{
				// Make a copy to edit
				List<IAsset> _assets = (Assets != null) ? Assets : new List<IAsset>();

				foreach (var asset in assets)
				{
					if (!_assets.Contains(asset))
					{
						_assets.Add(asset);
					}
				}

				// Set assets again to update changes
				Assets = assets;
			}
		}

		public void SetAssets(List<IAsset> assets)
		{
			_assets = assets;
			DispatchOnAssetsChanged();
		}

		#endregion

		#region Accessors

		public IAsset GetAssetByID(string id)
		{
			foreach(var asset in Assets)
			{
				if (asset != null && asset.AssetID == id)
					return asset;
			}

			return null;
		}

		#endregion

		#region Dispatchers

		private void DispatchOnAssetsChanged()
		{
			if (OnAssetsChanged != null)
				OnAssetsChanged(Assets);
		}

		#endregion
	}
}
