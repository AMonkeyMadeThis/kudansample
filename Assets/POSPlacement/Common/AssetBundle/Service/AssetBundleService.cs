﻿using AMonkeyMadeThis.Bamboo.AssetBundle;
using AMonkeyMadeThis.Bamboo.Network;
using AMonkeyMadeThis.Common.Service;
using POSPlacement.Common.AssetBundle.Service.Outcome;
using POSPlacement.Common.Config.Scripts.Model;
using POSPlacement.Common.Data.Scripts.Model.VO;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace POSPlacement.Common.AssetBundle.Service
{
	public class AssetBundleService : AbstractService
	{
		#region Singleton

		private static AssetBundleService _instance;
		/// <summary>
		/// Instance
		/// </summary>
		public static AssetBundleService Instance
		{
			get { return _instance; }
			private set
			{
				_instance = value;
			}
		}

		#endregion

		#region Events

		/// <summary>
		/// Delegate to handle changes in the progress
		/// </summary>
		/// <param name="progress"></param>
		public delegate void ProgressChanged(float progress);
		/// <summary>
		/// Dispatched when the progress changes
		/// </summary>
		public event ProgressChanged OnProgressChanged;
		/// <summary>
		/// Delegate to handle changes in the current job
		/// </summary>
		/// <param name="name"></param>
		public delegate void CurrentJobChanged(string name);
		/// <summary>
		/// Dispatched when the current job changes
		/// </summary>
		public event CurrentJobChanged OnCurrentJobChanged;
		/// <summary>
		/// Delegate to handle completion of loading
		/// </summary>
		/// <param name="name"></param>
		public delegate void LoadingComplete(List<IAsset> loadedAssets);
		/// <summary>
		/// Dispatched when loading finishes
		/// </summary>
		public event LoadingComplete OnLoadingComplete;

		#endregion

		#region Properties

		float _progress;
		/// <summary>
		/// Progress
		/// </summary>
		public float Progress
		{
			get { return _progress; }
			set
			{
				SetProgress(value);
			}
		}

		string _currentJob;
		/// <summary>
		/// CurrentJob
		/// </summary>
		public string CurrentJob
		{
			get { return _currentJob; }
			set
			{
				SetCurrentJob(value);
			}
		}
		/// <summary>
		/// Dev only cache buster
		/// </summary>
		public bool ClearCache = false;
		/// <summary>
		/// ApplicationConfigModel
		/// </summary>
		ApplicationConfigModel ApplicationConfigModel;

		#endregion

		#region Mutators

		private void SetProgress(Single value)
		{
			_progress = value;
			DispatchOnProgressChanged();
		}

		private void SetCurrentJob(String value)
		{
			_currentJob = value;
			DispatchCurrentJobChanged();
		}

		#endregion

		#region Constructor

		/// <summary>
		/// AssetBundleService
		/// </summary>
		public AssetBundleService()
		{
			Instance = this;
		}

		#endregion

		#region Lifecycle

		protected override void CollectComponents()
		{
			base.CollectComponents();

			ApplicationConfigModel = ApplicationConfigModel.Instance;
		}

		#endregion

		#region Loading

		public void LoadAssets(List<AssetBundleVO> assetBundles, Responder responder)
		{
			// get assets uri
			string assetsUri = ApplicationConfigModel.Data.AssetsURL;
			// reset progress
			Progress = 0;
			// start loading
			StartCoroutine(DoLoadAssets(assetsUri, assetBundles, responder));
		}

		IEnumerator DoLoadAssets(string assetsUri, List<AssetBundleVO> assetBundles, Responder responder)
		{
			List<IAsset> loadedAssets = new List<IAsset>();
			
			// Wait for cache
			if (Caching.enabled)
			{
				while(!Caching.ready)
				{
					yield return null;
				}
			}

			if (ClearCache)
				Caching.CleanCache();
			
			if (assetBundles != null && assetBundles.Count > 0)
			{
				float progressStep = 1f / assetBundles.Count;

				foreach (var assetBundle in assetBundles)
				{
					Debug.Log("Starting download: " + assetBundle.Name);
					CurrentJob = assetBundle.Name;

					yield return null;

					string uri = assetsUri + assetBundle.URL;
					int version = assetBundle.Version;
					WWW www = WWW.LoadFromCacheOrDownload(uri, version);

					float progress = Progress;

					while(!www.isDone)
					{
						CurrentJob = assetBundle.Name;
						Progress = www.progress;// progress + (progressStep * www.progress);

						yield return null;
					}

					// add progress
					//Progress = progress + progressStep;
					
					if (!string.IsNullOrEmpty(www.error))
					{
						responder.Fault.Invoke(AssetBundleServiceFault.Factory(uri, AssetBundleServiceFault.Cause.NoInternet));
						yield break;
					}
					else if (www.isDone)
					{
						// Show 100% even if from cache
						Progress = 1;
						UnityEngine.AssetBundle bundle = www.assetBundle;

						if (!string.IsNullOrEmpty(www.error))
						{
							responder.Fault.Invoke(AssetBundleServiceFault.Factory(uri, AssetBundleServiceFault.Cause.NoInternet));
							yield break;
						}
						else if (bundle != null)
						{
							// Load data assets- ones based on scriptable object
							Debug.Log("Getting data assets");
							AssetBundleRequest dataAssetRequest = bundle.LoadAllAssetsAsync<DataAsset>();
							// Wait for async load
							while(!dataAssetRequest.isDone)
							{
								yield return null;
							}
							// load assets
							DataAsset[] dataAssets = GetDataAssets(dataAssetRequest.allAssets);
							loadedAssets.AddRange(dataAssets);

							// Load prefab assets- ones based on gameobject, this doesn't seem to like returning
							// components, so we have to get all gameobjects, then try getting the type we are
							// actually interested in.
							Debug.Log("Getting prefab assets");
							AssetBundleRequest prefabAssetRequest = bundle.LoadAllAssetsAsync<GameObject>();
							// Wait for async load
							while (!prefabAssetRequest.isDone)
							{
								yield return null;
							}
							// load assets
							PrefabAsset[] prefabAssets = GetPrefabAssets(prefabAssetRequest.allAssets);
							loadedAssets.AddRange(prefabAssets);
							
							// Unload bundle but leave assets
							bundle.Unload(false);

							Debug.Log("Finished loading " + assetBundle.Name);
						}
						else
						{
							responder.Fault.Invoke(AssetBundleServiceFault.Factory(uri, AssetBundleServiceFault.Cause.WrongType));
							yield break;
						}
					}
					else
					{
						responder.Fault.Invoke(AssetBundleServiceFault.Factory(uri, AssetBundleServiceFault.Cause.FailedToLoadResource));
						yield break;
					}
				}

				DispatchOnLoadingComplete(loadedAssets);
				responder.Result.Invoke(AssetBundleServiceResult.Factory(loadedAssets));
			}
			else
			{
				responder.Fault.Invoke(AssetBundleServiceFault.Factory(null, AssetBundleServiceFault.Cause.NoAssetsToLoad));
			}
		}
		
		private PrefabAsset[] GetPrefabAssets(object[] assets)
		{
			List<PrefabAsset> result = new List<PrefabAsset>();

			foreach (var asset in assets)
			{
				Debug.Log("Inspecting " + asset);

				var gameObject = asset as GameObject;

				if (gameObject != null)
				{
					PrefabAsset prefabAsset = gameObject.GetComponent<PrefabAsset>();

					if (prefabAsset != null)
					{
						Debug.Log("Collecting " + prefabAsset.name);
						result.Add(prefabAsset);
					}
				}
			}

			return result.ToArray();
		}

		private DataAsset[] GetDataAssets(object[] assets)
		{
			List<DataAsset> result = new List<DataAsset>();

			foreach(var asset in assets)
			{
				Debug.Log("Inspecting " + asset);

				DataAsset dataAsset = asset as DataAsset;

				if (dataAsset != null)
				{
					Debug.Log("Collecting " + dataAsset.name);
					result.Add(dataAsset);
				}
			}

			return result.ToArray();
		}

		private PrefabAsset[] GetPrefabAssets(UnityEngine.AssetBundle bundle)
		{
			GameObject[] gameObjects = bundle.LoadAllAssets<GameObject>();
			List<PrefabAsset> result = new List<PrefabAsset>();

			foreach(var gameObject in gameObjects)
			{
				if (gameObject != null)
				{
					PrefabAsset prefabAsset = gameObject.GetComponent<PrefabAsset>();

					if (prefabAsset != null)
						result.Add(prefabAsset);
				}
			}

			return result.ToArray();
		}

		private DataAsset[] GetDataAssets(UnityEngine.AssetBundle bundle)
		{
			DataAsset[] result = bundle.LoadAllAssets<DataAsset>();

			return result;
		}

		#endregion

		#region Dispatchers

		private void DispatchOnProgressChanged()
		{
			if (OnProgressChanged != null)
				OnProgressChanged(Progress);
		}

		private void DispatchCurrentJobChanged()
		{
			if (OnCurrentJobChanged != null)
				OnCurrentJobChanged(CurrentJob);
		}

		private void DispatchOnLoadingComplete(List<IAsset> loadedAssets)
		{
			if (OnLoadingComplete != null)
				OnLoadingComplete(loadedAssets);
		}
		
		#endregion
	}
}
