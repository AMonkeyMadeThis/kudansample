﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace POSPlacement.Common.AssetBundle.Service.Outcome
{
	/// <summary>
	/// AssetBundleServiceFault
	/// </summary>
	public class AssetBundleServiceFault
	{
		#region Factories

		public static AssetBundleServiceFault Factory(string source, Cause reason)
		{
			return new AssetBundleServiceFault(source, reason);
		}

		#endregion

		#region Enums

		/// <summary>
		/// Cause of the failure
		/// </summary>
		[Flags]
		public enum Cause
		{
			NoAssetsToLoad,
			FailedToLoadResource,
			WrongType,
			NoInternet
		}

		#endregion

		#region Properties

		/// <summary>
		/// Source
		/// </summary>
		public string Source;
		/// <summary>
		/// Reason
		/// </summary>
		public Cause Reason;

		#endregion

		#region Constructor

		public AssetBundleServiceFault(string source, Cause reason)
		{
			Source = source;
			Reason = reason;
		}

		#endregion
	}
}
