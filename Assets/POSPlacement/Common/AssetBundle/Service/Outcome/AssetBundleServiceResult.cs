﻿using AMonkeyMadeThis.Bamboo.AssetBundle;
using System.Collections.Generic;

namespace POSPlacement.Common.AssetBundle.Service.Outcome
{
	/// <summary>
	/// AssetBundleServiceResult
	/// </summary>
	public class AssetBundleServiceResult
	{
		#region Factories

		public static AssetBundleServiceResult Factory(List<IAsset> assets)
		{
			return new AssetBundleServiceResult(assets);
		}

		#endregion

		#region Properties

		/// <summary>
		/// Assets
		/// </summary>
		public List<IAsset> Assets;

		#endregion

		#region Constructor

		public AssetBundleServiceResult(List<IAsset> assets)
		{
			Assets = assets;
		}

		#endregion
	}
}
