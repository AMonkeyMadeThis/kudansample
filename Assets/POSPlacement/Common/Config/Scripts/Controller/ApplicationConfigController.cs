﻿using AMonkeyMadeThis.Common.Controller;
using AMonkeyMadeThis.Bamboo.Application.Config;
using POSPlacement.Common.Config.Scripts.Model;

namespace POSPlacement.Common.Config.Scripts.Controller
{
	/// <summary>
	/// ApplicationConfigController
	/// </summary>
	public class ApplicationConfigController : AbstractController
	{
		#region Singleton

		private static ApplicationConfigController _instance;
		/// <summary>
		/// Instance
		/// </summary>
		public static ApplicationConfigController Instance
		{
			get { return _instance; }
			private set
			{
				_instance = value;
			}
		}

		#endregion

		#region Constructor

		/// <summary>
		/// ApplicationConfigController
		/// </summary>
		public ApplicationConfigController()
		{
			Instance = this;
		}

		#endregion

		#region Properties
		
		/// <summary>
		/// ApplicationConfigModel
		/// </summary>
		ApplicationConfigModel ApplicationConfigModel;

		#endregion

		#region Lifecycle

		protected override void CollectComponents()
		{
			base.CollectComponents();

			ApplicationConfigModel = ApplicationConfigModel.Instance;
		}
		
		#endregion
		
		#region Load

		public void LoadConfig(ApplicationConfigData data)
		{
			ApplicationConfigModel.Data = data;
		}

		#endregion
	}
}
