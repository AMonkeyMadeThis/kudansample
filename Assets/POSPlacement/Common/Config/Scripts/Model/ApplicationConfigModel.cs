﻿using AMonkeyMadeThis.Common.Model;
using AMonkeyMadeThis.Bamboo.Application.Config;
using UnityEngine;

namespace POSPlacement.Common.Config.Scripts.Model
{
	/// <summary>
	/// ApplicationConfigModel
	/// </summary>
	public class ApplicationConfigModel : AbstractModel
	{
		#region Singleton

		private static ApplicationConfigModel _instance;
		/// <summary>
		/// Instance
		/// </summary>
		public static ApplicationConfigModel Instance
		{
			get { return _instance; }
			private set
			{
				_instance = value;
			}
		}

		#endregion

		#region Events
		
		/// <summary>
		/// Delegate to handle data changes
		/// </summary>
		/// <param name="data"></param>
		public delegate void DataChanged(ApplicationConfigData data);
		/// <summary>
		/// Dispatched when data changes
		/// </summary>
		public event DataChanged OnDataChanged;

		#endregion

		#region Properties
		
		ApplicationConfigData _data;
		/// <summary>
		/// Data
		/// </summary>
		public ApplicationConfigData Data
		{
			get { return _data; }
			set
			{
				SetData(value);
			}
		}

		#endregion

		#region Mutators

		void SetData(ApplicationConfigData data)
		{
			_data = data;
			DispatchOnDataChanged();
		}

		#endregion

		#region Constructor

		/// <summary>
		/// ApplicationConfigModel
		/// </summary>
		public ApplicationConfigModel()
		{
			Instance = this;
		}

		#endregion

		#region Dispatchers

		private void DispatchOnDataChanged()
		{
			if (OnDataChanged != null)
				OnDataChanged(Data);
		}

		#endregion
	}
}
