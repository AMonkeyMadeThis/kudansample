﻿using AMonkeyMadeThis.Bamboo.Network;
using AMonkeyMadeThis.Common.Controller;
using POSPlacement.Common.Data.Scripts.Model;
using POSPlacement.Common.Data.Scripts.Service;
using POSPlacement.Common.Data.Scripts.Service.Outcome;
using UnityEngine;

namespace POSPlacement.Common.Data.Scripts.Controller
{
	/// <summary>
	/// ApplicationDataController
	/// </summary>
	public class ApplicationDataController : AbstractController
	{
		#region Singleton

		private static ApplicationDataController _instance;
		/// <summary>
		/// Instance
		/// </summary>
		public static ApplicationDataController Instance
		{
			get { return _instance; }
			private set
			{
				_instance = value;
			}
		}

		#endregion

		#region Events

		/// <summary>
		/// Delegate to handle a ConnectionError
		/// </summary>
		public delegate void ConnectionError(string message);
		/// <summary>
		/// Dispatched when a ConnectionError occurs
		/// </summary>
		public event ConnectionError OnConnectionError;

		#endregion

		#region Properties

		/// <summary>
		/// ApplicationDataModel
		/// </summary>
		ApplicationDataModel ApplicationDataModel;
		/// <summary>
		/// ApplicationDataService
		/// </summary>
		ApplicationDataService ApplicationDataService;

		#endregion
		
		#region Constructor

		/// <summary>
		/// ApplicationDataController
		/// </summary>
		public ApplicationDataController()
		{
			Instance = this;
		}

		#endregion

		#region Lifecycle

		protected override void CollectComponents()
		{
			base.CollectComponents();
			
			ApplicationDataModel = ApplicationDataModel.Instance;
			ApplicationDataService = GetComponent<ApplicationDataService>();
		}

		#endregion
		
		#region LoadData
		
		public void LoadData(string dataPath)
		{
			Responder responder = new Responder(LoadDataResult, LoadDataFault);
			ApplicationDataService.Load(dataPath, responder);
		}

		private void LoadDataResult(object outcome)
		{
			var result = outcome as ApplicationDataServiceResult;

			if (result != null)
			{
				ApplicationDataModel.AssetBundles = result.AssetBundles;
				ApplicationDataModel.Regions = result.Regions;
				ApplicationDataModel.Brands = result.Brands;
				ApplicationDataModel.Areas = result.Areas;
				ApplicationDataModel.Activations = result.Activations;
			}
			else
			{
				DispatchOnConnectionError("Enexpected result");
			}
		}

		private void LoadDataFault(object outcome)
		{
			var fault = outcome as ApplicationDataServiceFault;

			if (fault != null)
			{
				string pattern = "Failed to download {0}, due to {1}.";
				string message = string.Format(pattern, fault.Source, fault.Reason);

				DispatchOnConnectionError(message);
			}
			else
			{
				DispatchOnConnectionError("Unexpected fault");
			}
		}

		#endregion

		#region Dispatchers

		private void DispatchOnConnectionError(string message)
		{
			if (OnConnectionError != null)
				OnConnectionError(message);
		}

		#endregion
	}
}
