﻿using AMonkeyMadeThis.Bamboo.AssetBundle;
using AMonkeyMadeThis.Common.Model;
using POSPlacement.Common.AssetBundle.Enum;
using POSPlacement.Common.AssetBundle.Model;
using POSPlacement.Common.Data.Scripts.Model.VO;
using System;
using System.Collections.Generic;

namespace POSPlacement.Common.Data.Scripts.Model
{
	/// <summary>
	/// ApplicationDataModel
	/// </summary>
	public class ApplicationDataModel : AbstractModel
	{
		#region Singleton

		private static ApplicationDataModel _instance;
		/// <summary>
		/// Instance
		/// </summary>
		public static ApplicationDataModel Instance
		{
			get { return _instance; }
			private set
			{
				_instance = value;
			}
		}

		#endregion

		#region Constructor

		/// <summary>
		/// ApplicationDataModel
		/// </summary>
		public ApplicationDataModel()
		{
			Instance = this;
		}

		#endregion

		#region Events

		/// <summary>
		/// Delegate to handle a change in assetBundles
		/// </summary>
		/// <param name="assetBundles"></param>
		public delegate void AssetBundlesChanged(List<AssetBundleVO> assetBundles);
		/// <summary>
		/// Dispatched when the assetBundles change
		/// </summary>
		public event AssetBundlesChanged OnAssetBundlesChanged;
		/// <summary>
		/// Delegate to handle a change in regions
		/// </summary>
		/// <param name="regions"></param>
		public delegate void RegionsChanged(List<RegionVO> regions);
		/// <summary>
		/// Dispatched when the regions change
		/// </summary>
		public event RegionsChanged OnRegionsChanged;
		/// <summary>
		/// Delegate to handle a change in brands
		/// </summary>
		/// <param name="brands"></param>
		public delegate void BrandsChanged(List<BrandVO> brands);
		/// <summary>
		/// Dispatched when the brands change
		/// </summary>
		public event BrandsChanged OnBrandsChanged;
		/// <summary>
		/// Delegate to handle a change in areas
		/// </summary>
		/// <param name="areas"></param>
		public delegate void AreasChanged(List<AreaVO> areas);
		/// <summary>
		/// Dispatched when the areas change
		/// </summary>
		public event AreasChanged OnAreasChanged;
		/// <summary>
		/// Delegate to handle a change in activations
		/// </summary>
		/// <param name="activations"></param>
		public delegate void ActivationsChanged(List<ActivationVO> activations);
		/// <summary>
		/// Dispatched when the activations change
		/// </summary>
		public event ActivationsChanged OnActivationsChanged;

		#endregion

		#region Properties
		
		List<AssetBundleVO> _assetBundles;
		/// <summary>
		/// AssetBundles
		/// </summary>
		public List<AssetBundleVO> AssetBundles
		{
			get { return _assetBundles; }
			set
			{
				SetAssetBundles(value);
			}
		}
		
		List<RegionVO> _regions;
		/// <summary>
		/// Regions
		/// </summary>
		public List<RegionVO> Regions
		{
			get { return _regions; }
			set
			{
				SetRegions(value);
			}
		}
		
		List<BrandVO> _brands;
		/// <summary>
		/// Brands
		/// </summary>
		public List<BrandVO> Brands
		{
			get { return _brands; }
			set
			{
				SetBrands(value);
			}
		}
		
		List<AreaVO> _areas;
		/// <summary>
		/// Areas
		/// </summary>
		public List<AreaVO> Areas
		{
			get { return _areas; }
			set
			{
				SetAreas(value);
			}
		}
		
		List<ActivationVO> _activations;
		/// <summary>
		/// Activations
		/// </summary>
		public List<ActivationVO> Activations
		{
			get { return _activations; }
			set
			{
				SetActivations(value);
			}
		}

		#endregion

		#region Accessors

		public List<AssetBundleVO> GetAssetBundlesByType(AssetBundleType targetType)
		{
			List<AssetBundleVO> result = new List<AssetBundleVO>();

			if (AssetBundles != null && AssetBundles.Count > 0)
			{
				foreach(var assetBundle in AssetBundles)
				{
					if (assetBundle != null && assetBundle.Type == targetType)
						result.Add(assetBundle);
				}
			}

			return result;
		}

		/// <summary>
		/// get a list of populated asset bundle objects relating to the supplied proxy versions
		/// </summary>
		/// <param name="proxies"></param>
		/// <returns></returns>
		public List<AssetBundleVO> GetAssetBundles(List<AssetBundleVO> proxies)
		{
			List<AssetBundleVO> result = new List<AssetBundleVO>();

			if (proxies != null && proxies.Count > 0)
			{
				foreach (var proxy in proxies)
				{
					if (proxy != null)
					{
						AssetBundleVO assetBundle = GetAssetBundleById(proxy.ID);

						if (assetBundle != null)
							result.Add(assetBundle);
					}
				}
			}

			return result;
		}

		/// <summary>
		/// searches for a asset bundle by ID
		/// </summary>
		/// <param name="ID"></param>
		/// <returns></returns>
		public AssetBundleVO GetAssetBundleById(String ID)
		{
			if (!string.IsNullOrEmpty(ID) && AssetBundles != null && AssetBundles.Count > 0)
			{
				foreach (var assetBundle in AssetBundles)
				{
					if (assetBundle != null && assetBundle.ID == ID)
						return assetBundle;
				}
			}

			return null;
		}

		/// <summary>
		/// get a list of populated brand objects relating to the supplied proxy versions
		/// </summary>
		/// <param name="proxies"></param>
		/// <returns></returns>
		public List<BrandVO> GetBrands(List<BrandVO> proxies)
		{
			List<BrandVO> result = new List<BrandVO>();

			if (proxies != null && proxies.Count > 0)
			{
				foreach(var proxy in proxies)
				{
					if (proxy != null)
					{
						BrandVO brand = GetBrandById(proxy.ID);

						if (brand != null)
							result.Add(brand);
					}
				}
			}

			return result;
		}

		/// <summary>
		/// searches for a brand by ID
		/// </summary>
		/// <param name="ID"></param>
		/// <returns></returns>
		public BrandVO GetBrandById(String ID)
		{
			if (!string.IsNullOrEmpty(ID) && Brands != null && Brands.Count > 0)
			{
				foreach(var brand in Brands)
				{
					if (brand != null && brand.ID == ID)
						return brand;
				}
			}

			return null;
		}

		/// <summary>
		/// get a list of populated area objects relating to the supplied proxy versions
		/// </summary>
		/// <param name="proxies"></param>
		/// <returns></returns>
		public List<AreaVO> GetAreas(List<AreaVO> proxies)
		{
			List<AreaVO> result = new List<AreaVO>();

			if (proxies != null && proxies.Count > 0)
			{
				foreach (var proxy in proxies)
				{
					if (proxy != null)
					{
						AreaVO area = GetAreaById(proxy.ID);

						if (area != null)
							result.Add(area);
					}
				}
			}

			return result;
		}

		/// <summary>
		/// searches for a area by ID
		/// </summary>
		/// <param name="ID"></param>
		/// <returns></returns>
		public AreaVO GetAreaById(String ID)
		{
			if (!string.IsNullOrEmpty(ID) && Areas != null && Areas.Count > 0)
			{
				foreach (var area in Areas)
				{
					if (area != null && area.ID == ID)
						return area;
				}
			}

			return null;
		}

		/// <summary>
		/// get a list of populated activation objects relating to the supplied proxy versions
		/// </summary>
		/// <param name="proxies"></param>
		/// <returns></returns>
		public List<ActivationVO> GetActivations(List<ActivationVO> proxies)
		{
			List<ActivationVO> result = new List<ActivationVO>();

			if (proxies != null && proxies.Count > 0)
			{
				foreach (var proxy in proxies)
				{
					if (proxy != null)
					{
						ActivationVO activation = GetActivationById(proxy.ID);
						activation.Asset = AssetBundleModel.Instance.GetAssetByID(proxy.ID);
						
						if (activation != null)
							result.Add(activation);
					}
				}
			}

			return result;
		}

		/// <summary>
		/// searches for a activation by ID
		/// </summary>
		/// <param name="ID"></param>
		/// <returns></returns>
		public ActivationVO GetActivationById(String ID)
		{
			if (!string.IsNullOrEmpty(ID) && Activations != null && Activations.Count > 0)
			{
				foreach (var activation in Activations)
				{
					if (activation != null && activation.ID == ID)
						return activation;
				}
			}

			return null;
		}

		#endregion

		#region Mutators

		private void SetAssetBundles(List<AssetBundleVO> value)
		{
			_assetBundles = value;
			DispatchOnAssetBundlesChanged();
		}

		private void SetRegions(List<RegionVO> value)
		{
			_regions = value;
			DispatchOnRegionsChanged();
		}

		private void SetBrands(List<BrandVO> value)
		{
			_brands = value;
			DispatchOnBrandsChanged();
		}

		private void SetAreas(List<AreaVO> value)
		{
			_areas = value;
			DispatchOnAreasChanged();
		}

		private void SetActivations(List<ActivationVO> value)
		{
			_activations = value;
			DispatchOnActivationsChanged();
		}

		#endregion

		#region Dispatchers

		private void DispatchOnAssetBundlesChanged()
		{
			if (OnAssetBundlesChanged != null)
				OnAssetBundlesChanged(AssetBundles);
		}

		private void DispatchOnRegionsChanged()
		{
			if (OnRegionsChanged != null)
				OnRegionsChanged(Regions);
		}

		private void DispatchOnBrandsChanged()
		{
			if (OnBrandsChanged != null)
				OnBrandsChanged(Brands);
		}

		private void DispatchOnAreasChanged()
		{
			if (OnAreasChanged != null)
				OnAreasChanged(Areas);
		}

		private void DispatchOnActivationsChanged()
		{
			if (OnActivationsChanged != null)
				OnActivationsChanged(Activations);
		}

		#endregion
	}
}
