﻿using AMonkeyMadeThis.Common.Model.DTO;
using System.Xml.Serialization;

namespace POSPlacement.Common.Data.Scripts.Model.DTO
{
	/// <summary>
	/// ActivationDTO
	/// </summary>
	[XmlRoot(ElementName = "Activation")]
	public class ActivationDTO : AbstractDTO
	{
		/// <summary>
		/// Name
		/// </summary>
		[XmlAttribute(AttributeName = "name")]
		public string Name;
		/// <summary>
		/// Size
		/// </summary>
		[XmlAttribute(AttributeName = "height")]
		public float Height;
	}
}
