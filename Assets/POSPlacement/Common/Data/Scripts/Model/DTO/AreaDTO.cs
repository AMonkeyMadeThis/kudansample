﻿using AMonkeyMadeThis.Common.Model.DTO;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace POSPlacement.Common.Data.Scripts.Model.DTO
{
	/// <summary>
	/// AreaDTO
	/// </summary>
	[XmlRoot(ElementName = "Area")]
	public class AreaDTO : AbstractDTO
	{
		/// <summary>
		/// Name
		/// </summary>
		[XmlAttribute(AttributeName = "name")]
		public string Name;
		/// <summary>
		/// AssetBundles
		/// </summary>
		[XmlArray(ElementName = "AssetBundles")]
		[XmlArrayItem(ElementName = "AssetBundle")]
		public List<AssetBundleDTO> AssetBundles;
		/// <summary>
		/// Activations
		/// </summary>
		[XmlArray(ElementName = "Activations")]
		[XmlArrayItem(ElementName = "Activation")]
		public List<ActivationDTO> Activations;
	}
}
