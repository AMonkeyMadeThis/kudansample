﻿using AMonkeyMadeThis.Common.Model.DTO;
using POSPlacement.Common.AssetBundle.Enum;
using System.Xml.Serialization;

namespace POSPlacement.Common.Data.Scripts.Model.DTO
{
	/// <summary>
	/// AssetBundleDTO
	/// </summary>
	[XmlRoot(ElementName = "AssetBundle")]
	public class AssetBundleDTO : AbstractDTO
	{
		/// <summary>
		/// Name
		/// </summary>
		[XmlAttribute(AttributeName = "name")]
		public string Name;
		/// <summary>
		/// URL
		/// </summary>
		[XmlAttribute(AttributeName = "url")]
		public string URL;
		/// <summary>
		/// Version
		/// </summary>
		[XmlAttribute(AttributeName = "version")]
		public int Version;
		/// <summary>
		/// Type
		/// </summary>
		[XmlAttribute(AttributeName = "type")]
		public AssetBundleType Type;
	}
}
