﻿using AMonkeyMadeThis.Common.Model.DTO;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace POSPlacement.Common.Data.Scripts.Model.DTO
{
	/// <summary>
	/// BrandDTO
	/// </summary>
	[XmlRoot(ElementName = "Brand")]
	public class BrandDTO : AbstractDTO
	{
		/// <summary>
		/// Name
		/// </summary>
		[XmlAttribute(AttributeName = "name")]
		public string Name;
		/// <summary>
		/// Areas
		/// </summary>
		[XmlArray(ElementName = "Areas")]
		[XmlArrayItem(ElementName = "Area")]
		public List<AreaDTO> Areas;
		/// <summary>
		/// Watermark
		/// </summary>
		[XmlAttribute(AttributeName = "watermark")]
		public string Watermark;
	}
}
