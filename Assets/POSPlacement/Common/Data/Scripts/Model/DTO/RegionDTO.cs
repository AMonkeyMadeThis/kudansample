﻿using AMonkeyMadeThis.Common.Model.DTO;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace POSPlacement.Common.Data.Scripts.Model.DTO
{
	/// <summary>
	/// RegionDTO
	/// </summary>
	[XmlRoot(ElementName = "Region")]
	public class RegionDTO : AbstractDTO
	{
		/// <summary>
		/// Name
		/// </summary>
		[XmlAttribute(AttributeName = "name")]
		public string Name;
		/// <summary>
		/// Brands
		/// </summary>
		[XmlArray(ElementName = "Brands")]
		[XmlArrayItem(ElementName = "Brand")]
		public List<BrandDTO> Brands;
	}
}
