﻿using AMonkeyMadeThis.Bamboo.AssetBundle;
using AMonkeyMadeThis.Common.Model.VO;

namespace POSPlacement.Common.Data.Scripts.Model.VO
{
	/// <summary>
	/// ActivationVO
	/// </summary>
	public class ActivationVO : ValueObject
	{
		/// <summary>
		/// ID
		/// </summary>
		public string ID;
		/// <summary>
		/// Name
		/// </summary>
		public string Name;
		/// <summary>
		/// Height
		/// </summary>
		public float Height;
		/// <summary>
		/// Asset
		/// </summary>
		public IAsset Asset;
	}
}
