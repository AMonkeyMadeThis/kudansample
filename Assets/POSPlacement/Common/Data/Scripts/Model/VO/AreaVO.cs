﻿using AMonkeyMadeThis.Common.Model.VO;
using System.Collections.Generic;

namespace POSPlacement.Common.Data.Scripts.Model.VO
{
	/// <summary>
	/// AreaVO
	/// </summary>
	public class AreaVO : ValueObject
	{
		/// <summary>
		/// ID
		/// </summary>
		public string ID;
		/// <summary>
		/// Name
		/// </summary>
		public string Name;
		/// <summary>
		/// AssetBundles
		/// </summary>
		public List<AssetBundleVO> AssetBundles;
		/// <summary>
		/// Activations
		/// </summary>
		public List<ActivationVO> Activations;
	}
}
