﻿using AMonkeyMadeThis.Common.Model.VO;
using POSPlacement.Common.AssetBundle.Enum;

namespace POSPlacement.Common.Data.Scripts.Model.VO
{
	/// <summary>
	/// AssetBundleVO
	/// </summary>
	public class AssetBundleVO : ValueObject
	{
		/// <summary>
		/// ID
		/// </summary>
		public string ID;
		/// <summary>
		/// Name
		/// </summary>
		public string Name;
		/// <summary>
		/// URL
		/// </summary>
		public string URL;
		/// <summary>
		/// Version
		/// </summary>
		public int Version;
		/// <summary>
		/// Type
		/// </summary>
		public AssetBundleType Type;
	}
}
