﻿using AMonkeyMadeThis.Common.Model.VO;
using System.Collections.Generic;

namespace POSPlacement.Common.Data.Scripts.Model.VO
{
	/// <summary>
	/// BrandVO
	/// </summary>
	public class BrandVO : ValueObject
	{
		/// <summary>
		/// ID
		/// </summary>
		public string ID;
		/// <summary>
		/// Name
		/// </summary>
		public string Name;
		/// <summary>
		/// Areas
		/// </summary>
		public List<AreaVO> Areas;
		/// <summary>
		/// Watermark
		/// </summary>
		public string Watermark;
	}
}
