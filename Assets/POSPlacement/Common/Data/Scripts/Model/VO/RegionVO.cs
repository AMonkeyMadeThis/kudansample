﻿using AMonkeyMadeThis.Common.Model.VO;
using System.Collections.Generic;

namespace POSPlacement.Common.Data.Scripts.Model.VO
{
	/// <summary>
	/// RegionVO
	/// </summary>
	public class RegionVO : ValueObject
	{
		/// <summary>
		/// ID
		/// </summary>
		public string ID;
		/// <summary>
		/// Name
		/// </summary>
		public string Name;
		/// <summary>
		/// Brands
		/// </summary>
		public List<BrandVO> Brands;
	}
}
