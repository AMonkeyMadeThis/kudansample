﻿using AMonkeyMadeThis.Common.Service;
using AMonkeyMadeThis.Bamboo.Network;
using POSPlacement.Common.Data.Scripts.Service.Outcome;
using POSPlacement.Common.Data.Scripts.Service.Response;
using System;
using System.Collections;
using UnityEngine;

namespace POSPlacement.Common.Data.Scripts.Service
{
	/// <summary>
	/// ApplicationDataService
	/// </summary>
	public class ApplicationDataService : AbstractService
	{
		/// <summary>
		/// Load
		/// </summary>
		/// <param name="dataPath"></param>
		/// <param name="responder"></param>
		public void Load(String dataPath, Responder responder)
		{
			StartCoroutine(LoadData(dataPath, responder));
		}

		private IEnumerator LoadData(string dataPath, Responder responder)
		{
			ResourceRequest request = Resources.LoadAsync(dataPath);

			yield return request;

			if (request.isDone)
			{
				var asset = request.asset as TextAsset;

				if (asset != null)
				{
					var data = ApplicationDataServiceResponse.Factory(asset.text);

					if (data != null)
					{
						var result = ApplicationDataServiceResult.Factory(data);

						if (result != null)
						{
							responder.Result.Invoke(result);
						}
						else
						{
							responder.Fault.Invoke(ApplicationDataServiceFault.Factory(dataPath, ApplicationDataServiceFault.Cause.TranslationFailure));
						}
					}
					else
					{
						responder.Fault.Invoke(ApplicationDataServiceFault.Factory(dataPath, ApplicationDataServiceFault.Cause.SerialisationFailure));
					}
				}
				else
				{
					responder.Fault.Invoke(ApplicationDataServiceFault.Factory(dataPath, ApplicationDataServiceFault.Cause.WrongType));
				}
			}
			else
			{
				responder.Fault.Invoke(ApplicationDataServiceFault.Factory(dataPath, ApplicationDataServiceFault.Cause.FailedToLoadResource));
			}
		}
	}
}
