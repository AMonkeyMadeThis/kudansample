﻿using System;

namespace POSPlacement.Common.Data.Scripts.Service.Outcome
{
	/// <summary>
	/// ApplicationDataServiceFault
	/// </summary>
	public class ApplicationDataServiceFault
	{
		#region Factories

		public static ApplicationDataServiceFault Factory(string source, Cause reason)
		{
			return new ApplicationDataServiceFault(source, reason);
		}

		#endregion

		#region Enums

		/// <summary>
		/// Cause of the failure
		/// </summary>
		[Flags]
		public enum Cause
		{
			FailedToLoadResource,
			WrongType,
			SerialisationFailure,
			TranslationFailure
		}

		#endregion

		#region Properties

		/// <summary>
		/// Source
		/// </summary>
		public string Source;
		/// <summary>
		/// Reason
		/// </summary>
		public Cause Reason;

		#endregion

		#region Constructor

		public ApplicationDataServiceFault(string source, Cause reason)
		{
			Source = source;
			Reason = reason;
		}

		#endregion
	}
}
