﻿using POSPlacement.Common.Data.Scripts.Model.VO;
using POSPlacement.Common.Data.Scripts.Service.Response;
using POSPlacement.Common.Data.Scripts.Service.Translator;
using System.Collections.Generic;

namespace POSPlacement.Common.Data.Scripts.Service.Outcome
{
	/// <summary>
	/// ApplicationDataServiceResult
	/// </summary>
	public class ApplicationDataServiceResult
	{
		#region Factories

		/// <summary>
		/// ApplicationDataServiceResult
		/// </summary>
		/// <param name="source"></param>
		/// <returns></returns>
		public static ApplicationDataServiceResult Factory(ApplicationDataServiceResponse source)
		{
			ApplicationDataServiceResult result = new ApplicationDataServiceResult();
			result.AssetBundles = AssetBundleTranslator.Translate(source.AssetBundles);
			result.Regions = RegionTranslator.Translate(source.Regions);
			result.Brands = BrandTranslator.Translate(source.Brands);
			result.Areas = AreaTranslator.Translate(source.Areas);
			result.Activations = ActivationTranslator.Translate(source.Activations);

			return result;
		}

		#endregion

		#region Properties

		/// <summary>
		/// AssetBundles
		/// </summary>
		public List<AssetBundleVO> AssetBundles;
		/// <summary>
		/// Regions
		/// </summary>
		public List<RegionVO> Regions;
		/// <summary>
		/// Brands
		/// </summary>
		public List<BrandVO> Brands;
		/// <summary>
		/// Areas
		/// </summary>
		public List<AreaVO> Areas;
		/// <summary>
		/// Activations
		/// </summary>
		public List<ActivationVO> Activations;

		#endregion
	}
}
