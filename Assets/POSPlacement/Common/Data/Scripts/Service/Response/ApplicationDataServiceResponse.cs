﻿using AMonkeyMadeThis.Util;
using POSPlacement.Common.Data.Scripts.Model.DTO;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace POSPlacement.Common.Data.Scripts.Service.Response
{
	/// <summary>
	/// ApplicationDataServiceResponse
	/// </summary>
	[XmlRoot(ElementName = "data")]
	public class ApplicationDataServiceResponse
	{
		#region Factories

		/// <summary>
		/// ApplicationDataServiceResponse
		/// </summary>
		/// <param name="source"></param>
		/// <returns></returns>
		public static ApplicationDataServiceResponse Factory(string source)
		{
			var result = XmlUtils.Deserialise<ApplicationDataServiceResponse>(source);

			return result;
		}

		#endregion

		#region Properties

		/// <summary>
		/// AssetBundles
		/// </summary>
		[XmlArray(ElementName = "AssetBundles")]
		[XmlArrayItem(ElementName = "AssetBundle")]
		public List<AssetBundleDTO> AssetBundles;
		/// <summary>
		/// Regions
		/// </summary>
		[XmlArray(ElementName = "Regions")]
		[XmlArrayItem(ElementName = "Region")]
		public List<RegionDTO> Regions;
		/// <summary>
		/// Brands
		/// </summary>
		[XmlArray(ElementName = "Brands")]
		[XmlArrayItem(ElementName = "Brand")]
		public List<BrandDTO> Brands;
		/// <summary>
		/// Areas
		/// </summary>
		[XmlArray(ElementName = "Areas")]
		[XmlArrayItem(ElementName = "Area")]
		public List<AreaDTO> Areas;
		/// <summary>
		/// Activations
		/// </summary>
		[XmlArray(ElementName = "Activations")]
		[XmlArrayItem(ElementName = "Activation")]
		public List<ActivationDTO> Activations;

		#endregion
	}
}
