﻿using POSPlacement.Common.Data.Scripts.Model.DTO;
using POSPlacement.Common.Data.Scripts.Model.VO;
using System.Collections.Generic;

namespace POSPlacement.Common.Data.Scripts.Service.Translator
{
	public static class ActivationTranslator
	{
		public static ActivationDTO Translate(ActivationVO source)
		{
			ActivationDTO result = new ActivationDTO();
			result.ID = source.ID;
			result.Name = source.Name;
			result.Height = source.Height;

			return result;
		}

		public static List<ActivationDTO> Translate(List<ActivationVO> source)
		{
			List<ActivationDTO> result = new List<ActivationDTO>();

			foreach(var item in source)
			{
				ActivationDTO translated = Translate(item);
				result.Add(translated);
			}

			return result;
		}

		public static ActivationVO Translate(ActivationDTO source)
		{
			ActivationVO result = new ActivationVO();
			result.ID = source.ID;
			result.Name = source.Name;
			result.Height = source.Height;

			return result;
		}

		public static List<ActivationVO> Translate(List<ActivationDTO> source)
		{
			List<ActivationVO> result = new List<ActivationVO>();

			foreach (var item in source)
			{
				ActivationVO translated = Translate(item);
				result.Add(translated);
			}

			return result;
		}
	}
}
