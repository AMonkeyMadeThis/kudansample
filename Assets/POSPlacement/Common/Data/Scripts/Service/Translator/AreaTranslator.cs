﻿using POSPlacement.Common.Data.Scripts.Model.DTO;
using POSPlacement.Common.Data.Scripts.Model.VO;
using System.Collections.Generic;

namespace POSPlacement.Common.Data.Scripts.Service.Translator
{
	public static class AreaTranslator
	{
		public static AreaDTO Translate(AreaVO source)
		{
			AreaDTO result = new AreaDTO();
			result.ID = source.ID;
			result.Name = source.Name;
			result.AssetBundles = AssetBundleTranslator.Translate(source.AssetBundles);
			result.Activations = ActivationTranslator.Translate(source.Activations);

			return result;
		}

		public static List<AreaDTO> Translate(List<AreaVO> source)
		{
			List<AreaDTO> result = new List<AreaDTO>();

			foreach (var item in source)
			{
				AreaDTO translated = Translate(item);
				result.Add(translated);
			}

			return result;
		}

		public static AreaVO Translate(AreaDTO source)
		{
			AreaVO result = new AreaVO();
			result.ID = source.ID;
			result.Name = source.Name;
			result.AssetBundles = AssetBundleTranslator.Translate(source.AssetBundles);
			result.Activations = ActivationTranslator.Translate(source.Activations);

			return result;
		}

		public static List<AreaVO> Translate(List<AreaDTO> source)
		{
			List<AreaVO> result = new List<AreaVO>();

			foreach (var item in source)
			{
				AreaVO translated = Translate(item);
				result.Add(translated);
			}

			return result;
		}
	}
}
