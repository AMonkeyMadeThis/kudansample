﻿using POSPlacement.Common.Data.Scripts.Model.DTO;
using POSPlacement.Common.Data.Scripts.Model.VO;
using System.Collections.Generic;

namespace POSPlacement.Common.Data.Scripts.Service.Translator
{
	public static class AssetBundleTranslator
	{
		public static AssetBundleDTO Translate(AssetBundleVO source)
		{
			AssetBundleDTO result = new AssetBundleDTO();
			result.ID = source.ID;
			result.Name = source.Name;
			result.URL = source.URL;
			result.Version = source.Version;
			result.Type = source.Type;

			return result;
		}

		public static List<AssetBundleDTO> Translate(List<AssetBundleVO> source)
		{
			List<AssetBundleDTO> result = new List<AssetBundleDTO>();

			foreach (var item in source)
			{
				AssetBundleDTO translated = Translate(item);
				result.Add(translated);
			}

			return result;
		}

		public static AssetBundleVO Translate(AssetBundleDTO source)
		{
			AssetBundleVO result = new AssetBundleVO();
			result.ID = source.ID;
			result.Name = source.Name;
			result.URL = source.URL;
			result.Version = source.Version;
			result.Type = source.Type;

			return result;
		}

		public static List<AssetBundleVO> Translate(List<AssetBundleDTO> source)
		{
			List<AssetBundleVO> result = new List<AssetBundleVO>();

			foreach (var item in source)
			{
				AssetBundleVO translated = Translate(item);
				result.Add(translated);
			}

			return result;
		}
	}
}
