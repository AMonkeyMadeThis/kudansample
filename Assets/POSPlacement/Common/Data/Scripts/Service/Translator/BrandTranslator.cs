﻿using POSPlacement.Common.Data.Scripts.Model.DTO;
using POSPlacement.Common.Data.Scripts.Model.VO;
using System.Collections.Generic;

namespace POSPlacement.Common.Data.Scripts.Service.Translator
{
	public static class BrandTranslator
	{
		public static BrandDTO Translate(BrandVO source)
		{
			BrandDTO result = new BrandDTO();
			result.ID = source.ID;
			result.Name = source.Name;
			result.Areas = AreaTranslator.Translate(source.Areas);
			result.Watermark = source.Watermark;

			return result;
		}

		public static List<BrandDTO> Translate(List<BrandVO> source)
		{
			List<BrandDTO> result = new List<BrandDTO>();

			foreach (var item in source)
			{
				BrandDTO translated = Translate(item);
				result.Add(translated);
			}

			return result;
		}

		public static BrandVO Translate(BrandDTO source)
		{
			BrandVO result = new BrandVO();
			result.ID = source.ID;
			result.Name = source.Name;
			result.Areas = AreaTranslator.Translate(source.Areas);
			result.Watermark = source.Watermark;

			return result;
		}

		public static List<BrandVO> Translate(List<BrandDTO> source)
		{
			List<BrandVO> result = new List<BrandVO>();

			foreach (var item in source)
			{
				BrandVO translated = Translate(item);
				result.Add(translated);
			}

			return result;
		}
	}
}
