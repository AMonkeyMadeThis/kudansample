﻿using POSPlacement.Common.Data.Scripts.Model.DTO;
using POSPlacement.Common.Data.Scripts.Model.VO;
using System.Collections.Generic;

namespace POSPlacement.Common.Data.Scripts.Service.Translator
{
	public static class RegionTranslator
	{
		public static RegionDTO Translate(RegionVO source)
		{
			RegionDTO result = new RegionDTO();
			result.ID = source.ID;
			result.Name = source.Name;
			result.Brands = BrandTranslator.Translate(source.Brands);

			return result;
		}

		public static List<RegionDTO> Translate(List<RegionVO> source)
		{
			List<RegionDTO> result = new List<RegionDTO>();

			foreach (var item in source)
			{
				RegionDTO translated = Translate(item);
				result.Add(translated);
			}

			return result;
		}

		public static RegionVO Translate(RegionDTO source)
		{
			RegionVO result = new RegionVO();
			result.ID = source.ID;
			result.Name = source.Name;
			result.Brands = BrandTranslator.Translate(source.Brands);

			return result;
		}

		public static List<RegionVO> Translate(List<RegionDTO> source)
		{
			List<RegionVO> result = new List<RegionVO>();

			foreach (var item in source)
			{
				RegionVO translated = Translate(item);
				result.Add(translated);
			}

			return result;
		}
	}
}
