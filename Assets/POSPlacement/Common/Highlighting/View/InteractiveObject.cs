﻿using AMonkeyMadeThis.Common.Behaviour;
using HighlightingSystem;
using UnityEngine;

namespace POSPlacement.Common.Highlighting.View
{
	/// <summary>
	/// InteractiveObject
	/// </summary>
	[RequireComponent(typeof(Highlighter))]
	public class InteractiveObject : InvalidatingBehaviour
	{
		#region Events

		/// <summary>
		/// Delegate to handle a change in selected
		/// </summary>
		/// <param name="target"></param>
		public delegate void SelectedChanged(InteractiveObject target);
		/// <summary>
		/// Dispatched when selected changes
		/// </summary>
		public event SelectedChanged OnSelectedChanged;
		/// <summary>
		/// Delegate to handle changes in SelectionFadeDuration
		/// </summary>
		/// <param name="target"></param>
		public delegate void SelectionFadeDurationChanged(InteractiveObject target);
		/// <summary>
		/// Dispatched when SelectionFadeDuration changes
		/// </summary>
		public event SelectionFadeDurationChanged OnSelectionFadeDurationChanged;
		/// <summary>
		/// Delegate to handle changes in SelectionColor
		/// </summary>
		/// <param name="target"></param>
		public delegate void SelectionColorChanged(InteractiveObject target);
		/// <summary>
		/// Dispatched when SelectionColor changes
		/// </summary>
		public event SelectionColorChanged OnSelectionColorChanged;

		#endregion

		#region Properties

		bool _selected = false;
		/// <summary>
		/// Selected
		/// </summary>
		public bool Selected
		{
			get { return _selected; }
			set
			{
				SetSelected(value);
			}
		}

		[SerializeField]
		float _selectionFadeDuration = .4f;
		/// <summary>
		/// SelectionFadeDuration
		/// </summary>
		public float SelectionFadeDuration
		{
			get { return _selectionFadeDuration; }
			set
			{
				SetSelectionFadeDuration(value);
			}
		}

		[SerializeField]
		Color _selectionColor;
		/// <summary>
		/// SelectionColor
		/// </summary>
		public Color SelectionColor
		{
			get { return _selectionColor; }
			set
			{
				SetSelectionColor(value);
			}
		}

		#endregion

		#region Skin Parts

		/// <summary>
		/// Highlighter
		/// </summary>
		Highlighter Highlighter;

		#endregion

		#region Mutators

		private void SetSelected(bool value)
		{
			if (Selected != value)
			{
				_selected = value;
				DispatchOnSelectedChanged();
				Invalidate();
			}
		}

		private void SetSelectionFadeDuration(float value)
		{
			if (SelectionFadeDuration != value)
			{
				_selectionFadeDuration = value;
				DispatchOnSelectionFadeDurationChanged();
				Invalidate();
			}
		}

		private void SetSelectionColor(Color value)
		{
			if (!SelectionColor.Equals(value))
			{
				_selectionColor = value;
				DispatchOnSelectionColorChanged();
				Invalidate();
			}
		}

		#endregion

		#region Lifecycle

		protected override void CollectComponents()
		{
			base.CollectComponents();

			Highlighter = GetComponent<Highlighter>();
		}

		protected override void CommitProperties()
		{
			UpdateHighlight();
			base.CommitProperties();
		}

		#endregion

		#region Highlighting

		private void UpdateHighlight()
		{
			if (Highlighter != null)
			{
				if (Selected == true)
					Highlighter.ConstantOn(SelectionColor, SelectionFadeDuration);
				else
					Highlighter.ConstantOff(SelectionFadeDuration);
			}
		}

		#endregion

		#region Dispatchers

		private void DispatchOnSelectedChanged()
		{
			if (OnSelectedChanged != null)
				OnSelectedChanged(this);
		}

		private void DispatchOnSelectionFadeDurationChanged()
		{
			if (OnSelectionFadeDurationChanged != null)
				OnSelectionFadeDurationChanged(this);
		}

		private void DispatchOnSelectionColorChanged()
		{
			if (OnSelectionColorChanged != null)
				OnSelectionColorChanged(this);
		}

		#endregion
	}
}
