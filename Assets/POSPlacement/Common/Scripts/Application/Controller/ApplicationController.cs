﻿using AMonkeyMadeThis.Bamboo.UI.Component.Application;
using AMonkeyMadeThis.Common.Controller;
using POSPlacement.Common.Data.Scripts.Model;
using POSPlacement.Common.Data.Scripts.Model.VO;
using POSPlacement.Common.Scripts.Application.Enum;
using POSPlacement.Common.Scripts.Application.Model;
using System.Linq;
using UnityEngine;

namespace POSPlacement.Common.Scripts.Application.Controller
{
	/// <summary>
	/// ApplicationController
	/// </summary>
	public class ApplicationController : AbstractController
	{
		#region Singleton

		private static ApplicationController _instance;
		/// <summary>
		/// Instance
		/// </summary>
		public static ApplicationController Instance
		{
			get { return _instance; }
			private set
			{
				_instance = value;
			}
		}

		#endregion

		#region Properties

		/// <summary>
		/// ApplicationModel
		/// </summary>
		ApplicationModel ApplicationModel;
		/// <summary>
		/// ApplicationDataModel
		/// </summary>
		ApplicationDataModel ApplicationDataModel;
		/// <summary>
		/// ModuleLoader
		/// </summary>
		ModuleLoader ModuleLoader;

		#endregion

		#region Constructor

		/// <summary>
		/// ApplicationController
		/// </summary>
		public ApplicationController()
		{
			Instance = this;
		}

		#endregion

		#region Lifecycle

		protected override void CollectComponents()
		{
			base.CollectComponents();

			ApplicationModel = ApplicationModel.Instance;
			ApplicationDataModel = ApplicationDataModel.Instance;
			ModuleLoader = ModuleLoader.Instance;
		}

		#endregion

		#region Application state

		/// <summary>
		/// ApplicationInitialised
		/// </summary>
		public void ApplicationInitialised()
		{
			GoToRegionSelection();
		}

		public void AssetsLoaded()
		{
			GoToActivationPlacement();
		}

		#endregion

		#region Navigation

		private void GoToHome()
		{
			GoTo(Scene.Main);
		}

		private void GoToRegionSelection()
		{
			if (ApplicationDataModel != null && ApplicationDataModel.Regions != null && ApplicationDataModel.Regions.Count > 0)
			{
				if (ApplicationDataModel.Regions.Count > 1)
				{
					GoTo(Scene.RegionSelection);
				}
				else
				{
					PreSelectFirstRegion();
				}
			}
			else
			{
				Debug.LogError("DataModel or Regions is empty (check loaded data)");
			}
		}

		private void PreSelectFirstRegion()
		{
			RegionVO firstRegion = ApplicationDataModel.Regions.First();

			ApplicationModel.CurrentRegion = firstRegion;
		}

		private void GoToBrandSelection()
		{
			if (ApplicationModel != null && ApplicationModel.CurrentRegion != null && ApplicationModel.CurrentRegion.Brands != null && ApplicationModel.CurrentRegion.Brands.Count > 0)
			{
				if (ApplicationModel.CurrentRegion.Brands.Count > 1)
				{
					GoTo(Scene.BrandSelection);
				}
				else
				{
					PreSelectFirstBrand();
				}
			}
			else
			{
				GoToRegionSelection();
			}
		}

		private void PreSelectFirstBrand()
		{
			BrandVO firstBrand = ApplicationDataModel.Brands.First();

			ApplicationModel.CurrentBrand = firstBrand;
		}

		private void GoToAreaSelection()
		{
			if (ApplicationDataModel != null && ApplicationDataModel.Areas != null && ApplicationDataModel.Areas.Count > 0)
			{
				if (ApplicationDataModel.Areas.Count > 1)
				{
					GoTo(Scene.AreaSelection);
				}
				else
				{
					PreSelectFirstArea();
				}
			}
			else
			{
				Debug.LogError("DataModel or Areas is empty (check loaded data)");
			}
		}

		private void PreSelectFirstArea()
		{
			AreaVO firstArea = ApplicationDataModel.Areas.First();

			ApplicationModel.CurrentArea = firstArea;
		}

		private void GoToLoading()
		{
			GoTo(Scene.Loading);
		}

		private void GoToActivationPlacement()
		{
			GoTo(Scene.ActivationPlacement);
		}

		void GoTo(Scene scene)
		{
			if (ApplicationModel != null)
				ApplicationModel.CurrentScene = scene;
		}

		public void GoBack()
		{
			if (ApplicationModel != null)
			{
				// if in ActivationPlacement
				if (ApplicationModel.CurrentArea != null)
				{
					ApplicationModel.CurrentArea = null;

					if (ApplicationModel.CurrentBrand.Areas.Count > 1)
						GoToAreaSelection();
					else
						GoBack();
				}
				// if in AreaSelection
				else if (ApplicationModel.CurrentBrand != null)
				{
					ApplicationModel.CurrentBrand = null;

					if (ApplicationModel.CurrentRegion.Brands.Count > 1)
						GoToBrandSelection();
					else
						GoBack();
				}
				// if in BrandSelection
				else if (ApplicationModel.CurrentRegion != null)
				{
					ApplicationModel.CurrentRegion = null;

					if (ApplicationDataModel.Regions.Count > 1)
						GoToRegionSelection();
					else
						GoBack();
				}
				// if in region selection
				else
				{
					// Already home ... quit?
					GoToHome();
				}
			}
		}

		#endregion

		#region Listeners

		public override void AddListeners()
		{
			base.AddListeners();

			AddListeners(ApplicationModel);
		}

		private void AddListeners(ApplicationModel applicationModel)
		{
			if (applicationModel != null)
			{
				applicationModel.OnCurrentRegionChanged += HandleApplicationModelOnCurrentRegionChanged;
				applicationModel.OnCurrentBrandChanged += HandleApplicationModelOnCurrentBrandChanged;
				applicationModel.OnCurrentAreaChanged += HandleApplicationModelOnCurrentAreaChanged;
				applicationModel.OnCurrentSceneChanged += HandleApplicationModelOnCurrentSceneChanged;
			}
		}

		public override void RemoveListeners()
		{
			base.RemoveListeners();

			RemoveListeners(ApplicationModel);
		}

		private void RemoveListeners(ApplicationModel applicationModel)
		{
			if (applicationModel != null)
			{
				applicationModel.OnCurrentRegionChanged -= HandleApplicationModelOnCurrentRegionChanged;
				applicationModel.OnCurrentBrandChanged -= HandleApplicationModelOnCurrentBrandChanged;
				applicationModel.OnCurrentAreaChanged -= HandleApplicationModelOnCurrentAreaChanged;
				applicationModel.OnCurrentSceneChanged -= HandleApplicationModelOnCurrentSceneChanged;
			}
		}

		#endregion

		#region Handlers

		private void HandleApplicationModelOnCurrentRegionChanged(RegionVO current)
		{
			if (current != null)
				GoToBrandSelection();
			//else
			//	GoToRegionSelection();
		}

		private void HandleApplicationModelOnCurrentBrandChanged(BrandVO current)
		{
			if (current != null)
				GoToAreaSelection();
			//else
			//	GoToBrandSelection();
		}

		private void HandleApplicationModelOnCurrentAreaChanged(AreaVO current)
		{
			if (current != null)
				GoToLoading();
			//else
			//	GoToAreaSelection();
		}
		
		private void HandleApplicationModelOnCurrentSceneChanged(Scene current)
		{
			if (ModuleLoader != null)
			{
				string sceneName = current.ToString();

				ModuleLoader.GoTo(sceneName);
			}
		}

		#endregion
	}
}
