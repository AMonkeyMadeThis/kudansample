﻿using System;

namespace POSPlacement.Common.Scripts.Application.Enum
{
	/// <summary>
	/// Scene
	/// </summary>
	[Flags]
	public enum Scene
	{
		Main,
		Home,
		Settings,
		RegionSelection,
		BrandSelection,
		AreaSelection,
		Loading,
		ActivationPlacement
	}
}
