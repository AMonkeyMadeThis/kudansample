﻿using AMonkeyMadeThis.Common.Model;
using POSPlacement.Common.Data.Scripts.Model.VO;
using POSPlacement.Common.Scripts.Application.Enum;

namespace POSPlacement.Common.Scripts.Application.Model
{
	/// <summary>
	/// ApplicationModel
	/// </summary>
	public class ApplicationModel : AbstractModel
	{
		#region Singleton

		private static ApplicationModel _instance;
		/// <summary>
		/// Instance
		/// </summary>
		public static ApplicationModel Instance
		{
			get { return _instance; }
			private set
			{
				_instance = value;
			}
		}

		#endregion

		#region Constructor

		/// <summary>
		/// ApplicationModel
		/// </summary>
		public ApplicationModel()
		{
			Instance = this;
		}

		#endregion

		#region Events

		/// <summary>
		/// Delegate to handle a change in current scene
		/// </summary>
		/// <param name="current"></param>
		public delegate void CurrentSceneChanged(Scene current);
		/// <summary>
		/// Dispatched when the current scene changes
		/// </summary>
		public event CurrentSceneChanged OnCurrentSceneChanged;
		/// <summary>
		/// Delegate to handle a change in current region
		/// </summary>
		/// <param name="current"></param>
		public delegate void CurrentRegionChanged(RegionVO current);
		/// <summary>
		/// Dispatched when the current region changes
		/// </summary>
		public event CurrentRegionChanged OnCurrentRegionChanged;
		/// <summary>
		/// Delegate to handle a change in current brand
		/// </summary>
		/// <param name="current"></param>
		public delegate void CurrentBrandChanged(BrandVO current);
		/// <summary>
		/// Dispatched when current brand changes
		/// </summary>
		public event CurrentBrandChanged OnCurrentBrandChanged;
		/// <summary>
		/// Delegate to handle a change in current area
		/// </summary>
		/// <param name="current"></param>
		public delegate void CurrentAreaChanged(AreaVO current);
		/// <summary>
		/// Dispatched when the current area changes
		/// </summary>
		public event CurrentAreaChanged OnCurrentAreaChanged;

		#endregion

		#region Properties

		Scene _currentScene;
		/// <summary>
		/// CurrentScene
		/// </summary>
		public Scene CurrentScene
		{
			get { return _currentScene; }
			set
			{
				SetCurrentScene(value);
			}
		}

		RegionVO _currentRegion;
		/// <summary>
		/// CurrentRegion
		/// </summary>
		public RegionVO CurrentRegion
		{
			get { return _currentRegion; }
			set
			{
				SetCurrentRegion(value);
			}
		}

		BrandVO _currentBrand;
		/// <summary>
		/// CurrentBrand
		/// </summary>
		public BrandVO CurrentBrand
		{
			get { return _currentBrand; }
			set
			{
				SetCurrentBrand(value);
			}
		}
		
		AreaVO _currentArea;
		/// <summary>
		/// CurrentArea
		/// </summary>
		public AreaVO CurrentArea
		{
			get { return _currentArea; }
			set
			{
				SetCurrentArea(value);
			}
		}

		#endregion

		#region Mutators

		private void SetCurrentScene(Scene value)
		{
			_currentScene = value;
			DispatchOnCurrentSceneChanged();
		}

		private void SetCurrentRegion(RegionVO value)
		{
			_currentRegion = value;
			DispatchOnCurrentRegionChanged();
		}

		private void SetCurrentBrand(BrandVO value)
		{
			_currentBrand = value;
			DispatchOnCurrentBrandChanged();
		}

		private void SetCurrentArea(AreaVO value)
		{
			_currentArea = value;
			DispatchOnCurrentAreaChanged();
		}

		#endregion

		#region Dispatchers

		private void DispatchOnCurrentSceneChanged()
		{
			if (OnCurrentSceneChanged != null)
				OnCurrentSceneChanged(CurrentScene);
		}

		private void DispatchOnCurrentRegionChanged()
		{
			if (OnCurrentRegionChanged != null)
				OnCurrentRegionChanged(CurrentRegion);
		}

		private void DispatchOnCurrentBrandChanged()
		{
			if (OnCurrentBrandChanged != null)
				OnCurrentBrandChanged(CurrentBrand);
		}

		private void DispatchOnCurrentAreaChanged()
		{
			if (OnCurrentAreaChanged != null)
				OnCurrentAreaChanged(CurrentArea);
		}

		#endregion
	}
}
