﻿using AMonkeyMadeThis.Bamboo.AssetBundle;
using AMonkeyMadeThis.Bamboo.Localisation.Controller;
using AMonkeyMadeThis.Bamboo.Localisation.Model;
using AMonkeyMadeThis.Common.Controller;
using POSPlacement.Common.AssetBundle.Controller;
using POSPlacement.Common.AssetBundle.Model;
using POSPlacement.Common.AssetBundle.Service;
using POSPlacement.Common.Data.Scripts.Model;
using POSPlacement.Common.Data.Scripts.Model.VO;
using POSPlacement.Common.Scripts.Application.Controller;
using POSPlacement.Common.Scripts.Application.Model;
using POSPlacement.Loading.Scripts.Model;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace POSPlacement.Loading.Scripts.Controller
{
	/// <summary>
	/// LoadingViewController
	/// </summary>
	[RequireComponent(typeof(LoadingViewModel))]
	public class LoadingViewController : AbstractController
	{
		#region Consts

		/// <summary>
		/// Asset bundle name for 'Application'
		/// </summary>
		const string ASSET_BUNDLE_APPLICATION = "Application";
		/// <summary>
		/// Asset bundle name for 'AssetBundles'
		/// </summary>
		const string ASSET_BUNDLE_ASSET_BUNDLES = "AssetBundles";
		/// <summary>
		/// Localisation key to load the asset job pattern eg- "Loading: {0}" or "Descargando: {0}"
		/// </summary>
		const string ASSET_JOB_PATTERN = "Application.Main.Loading.Assets.Job";

		#endregion

		#region Properties
		
		/// <summary>
		/// ApplicationController
		/// </summary>
		ApplicationController ApplicationController;
		/// <summary>
		/// ApplicationModel
		/// </summary>
		ApplicationModel ApplicationModel;
		/// <summary>
		/// ApplicationDataModel
		/// </summary>
		ApplicationDataModel ApplicationDataModel;
		/// <summary>
		/// AssetBundleController
		/// </summary>
		AssetBundleController AssetBundleController;
		/// <summary>
		/// AssetBundleService
		/// </summary>
		AssetBundleService AssetBundleService;
		/// <summary>
		/// AssetBundleModel
		/// </summary>
		AssetBundleModel AssetBundleModel;
		/// <summary>
		/// LocalisationController
		/// </summary>
		LocalisationController LocalisationController;
		/// <summary>
		/// LocalisationModel
		/// </summary>
		LocalisationModel LocalisationModel;
		/// <summary>
		/// LoadingViewModel
		/// </summary>
		LoadingViewModel LoadingViewModel;

		#endregion

		#region Lifecycle

		protected override void CollectComponents()
		{
			base.CollectComponents();
			
			ApplicationController = ApplicationController.Instance;
			ApplicationModel = ApplicationModel.Instance;
			ApplicationDataModel = ApplicationDataModel.Instance;
			AssetBundleController = AssetBundleController.Instance;
			AssetBundleService = AssetBundleService.Instance;
			AssetBundleModel = AssetBundleModel.Instance;
			LocalisationController = LocalisationController.Instance;
			LocalisationModel = LocalisationModel.Instance;
			LoadingViewModel = GetComponent<LoadingViewModel>();
		}

		public override void Start()
		{
			base.Start();

			//TODO load assets
			LoadAreaAssets();
		}

		private void LoadAreaAssets()
		{
			if (ApplicationModel != null && ApplicationModel.CurrentArea != null && ApplicationModel.CurrentArea.AssetBundles.Count > 0 && ApplicationDataModel.AssetBundles != null && ApplicationDataModel.AssetBundles.Count > 0)
			{
				List<AssetBundleVO> assetBundles = ApplicationDataModel.GetAssetBundles(ApplicationModel.CurrentArea.AssetBundles);

				if (assetBundles != null && assetBundles.Count > 0)
					LoadActivations(assetBundles);
				else
					Debug.LogError("CurrentArea has no asset bundle dependencies");
			}
			else
			{
				Debug.LogError("ApplicationModel.CurrentArea.AssetBundles should be populated at this point");
			}
		}

		#endregion

		#region Loading

		private void LoadActivations(List<AssetBundleVO> activationAssets)
		{
			AssetBundleController.LoadAssets(activationAssets);
			ShowLoadingPage();
		}

		#endregion

		#region Page navigation

		private void ShowLoadingPage()
		{
			LoadingViewModel.CurrentPage = LoadingViewModel.Page.Loading;
		}

		private void ShowErrorPage()
		{
			LoadingViewModel.CurrentPage = LoadingViewModel.Page.Error;
		}

		#endregion

		#region Scene navigation

		public void GoNext()
		{
			ApplicationController.AssetsLoaded();
		}

		#endregion

		#region Listeners

		public override void AddListeners()
		{
			base.AddListeners();

			AddListeners(AssetBundleService);
			AddListeners(AssetBundleController);
		}

		private void AddListeners(AssetBundleService service)
		{
			if (service != null)
			{
				service.OnCurrentJobChanged += HandleAssetBundleServiceOnCurrentJobChanged;
				service.OnProgressChanged += HandleAssetBundleServiceOnProgressChanged;
				service.OnLoadingComplete += HandleAssetBundleServiceOnLoadingComplete;
			}
		}

		private void AddListeners(AssetBundleController assetBundleController)
		{
			if (assetBundleController != null)
			{
				assetBundleController.OnError += HandleAssetBundleControllerOnError;
			}
		}

		public override void RemoveListeners()
		{
			base.RemoveListeners();

			RemoveListeners(AssetBundleService);
			RemoveListeners(AssetBundleController);
		}

		private void RemoveListeners(AssetBundleService service)
		{
			if (service != null)
			{
				service.OnCurrentJobChanged -= HandleAssetBundleServiceOnCurrentJobChanged;
				service.OnProgressChanged -= HandleAssetBundleServiceOnProgressChanged;
				service.OnLoadingComplete -= HandleAssetBundleServiceOnLoadingComplete;
			}
		}

		private void RemoveListeners(AssetBundleController assetBundleController)
		{
			if (assetBundleController != null)
			{
				assetBundleController.OnError -= HandleAssetBundleControllerOnError;
			}
		}

		#endregion

		#region Handlers

		private void HandleAssetBundleServiceOnCurrentJobChanged(String jobName)
		{
			string assetJobPattern = LocalisationModel.LocaliseString(ASSET_JOB_PATTERN, ASSET_BUNDLE_APPLICATION);
			string assetJobName = LocalisationModel.LocaliseString(jobName, ASSET_BUNDLE_ASSET_BUNDLES);
			string assetJobMessage = string.Format(assetJobPattern, assetJobName);

			LoadingViewModel.AssetJob = assetJobMessage;
		}

		private void HandleAssetBundleServiceOnProgressChanged(float progress)
		{
			LoadingViewModel.AssetProgress = progress;
		}

		private void HandleAssetBundleServiceOnLoadingComplete(List<IAsset> loadedAssets)
		{
			if (loadedAssets != null && loadedAssets.Count > 0)
				GoNext();
		}

		private void HandleAssetBundleControllerOnError(String message)
		{
			LoadingViewModel.ErrorMessage = message;
			ShowErrorPage();
		}

		#endregion
	}
}
