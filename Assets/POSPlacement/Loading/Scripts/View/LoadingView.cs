﻿using AMonkeyMadeThis.Bamboo.UI.Component.Navigation;
using AMonkeyMadeThis.Common.View;
using POSPlacement.Loading.Scripts.Controller;
using POSPlacement.Loading.Scripts.Model;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace POSPlacement.Loading.Scripts.View
{
	/// <summary>
	/// LoadingView
	/// </summary>
	[RequireComponent(typeof(LoadingViewModel))]
	[RequireComponent(typeof(LoadingViewController))]
	public class LoadingView : AbstractView
	{
		#region Properties

		/// <summary>
		/// LoadingViewModel
		/// </summary>
		LoadingViewModel LoadingViewModel;
		/// <summary>
		/// LoadingViewController
		/// </summary>
		LoadingViewController LoadingViewController;

		#endregion

		#region Skin parts

		/// <summary>
		/// ViewStack
		/// </summary>
		public ViewStack ViewStack;
		/// <summary>
		/// AssetProgressBar
		/// </summary>
		public Image AssetProgressBar;
		/// <summary>
		/// AssetJobLabel
		/// </summary>
		public Text AssetJobLabel;
		/// <summary>
		/// ErrorMessageLabel
		/// </summary>
		public Text ErrorMessageLabel;

		#endregion

		#region Lifecycle

		protected override void CollectComponents()
		{
			base.CollectComponents();

			LoadingViewModel = GetComponent<LoadingViewModel>();
			LoadingViewController = GetComponent<LoadingViewController>();
		}

		#endregion

		#region Screen rotation

		protected override void ConfigureScreenRotation()
		{
			Screen.autorotateToPortrait = true;
			Screen.autorotateToPortraitUpsideDown = true;
			Screen.autorotateToLandscapeLeft = true;
			Screen.autorotateToLandscapeRight = true;
		}

		#endregion

		#region Page navigation

		private void ShowPage(String page)
		{
			if (ViewStack != null)
				ViewStack.GoToLabel(page);
		}
		
		#endregion

		#region Listeners

		public override void AddListeners()
		{
			base.AddListeners();

			AddListeners(LoadingViewModel);
		}

		private void AddListeners(LoadingViewModel loadingViewModel)
		{
			if (loadingViewModel != null)
			{
				loadingViewModel.OnCurrentPageChanged += HandleLoadingViewModelOnCurrentPageChanged;
				loadingViewModel.OnAssetJobChanged += HandleLoadingViewModelOnAssetJobChanged;
				loadingViewModel.OnAssetProgressChanged += HandleLoadingViewModelOnAssetProgressChanged;
				loadingViewModel.OnErrorMessageChanged += HandleLoadingViewModelOnErrorMessageChanged;
			}
		}

		public override void RemoveListeners()
		{
			base.RemoveListeners();

			RemoveListeners(LoadingViewModel);
		}

		private void RemoveListeners(LoadingViewModel loadingViewModel)
		{
			if (loadingViewModel != null)
			{
				loadingViewModel.OnCurrentPageChanged -= HandleLoadingViewModelOnCurrentPageChanged;
				loadingViewModel.OnAssetJobChanged -= HandleLoadingViewModelOnAssetJobChanged;
				loadingViewModel.OnAssetProgressChanged -= HandleLoadingViewModelOnAssetProgressChanged;
				loadingViewModel.OnErrorMessageChanged -= HandleLoadingViewModelOnErrorMessageChanged;
			}
		}

		#endregion

		#region Handlers

		private void HandleLoadingViewModelOnErrorMessageChanged(String errorMessage)
		{
			if (ErrorMessageLabel != null)
				ErrorMessageLabel.text = errorMessage;
		}

		private void HandleLoadingViewModelOnCurrentPageChanged(LoadingViewModel.Page current)
		{
			string page = current.ToString();

			ShowPage(page);
		}

		private void HandleLoadingViewModelOnAssetJobChanged(String jobName)
		{
			if (AssetJobLabel != null)
				AssetJobLabel.text = jobName;
		}

		private void HandleLoadingViewModelOnAssetProgressChanged(float progress)
		{
			// This should be a 'component' where I just do AssetProgressBar.progress = progress, but time is short ...
			if (AssetProgressBar != null)
				AssetProgressBar.fillAmount = progress;
		}

		#endregion
	}
}
