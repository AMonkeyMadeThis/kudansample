﻿using AMonkeyMadeThis.Bamboo.Application.Config;
using AMonkeyMadeThis.Bamboo.Localisation.Controller;
using AMonkeyMadeThis.Bamboo.Localisation.Enum;
using AMonkeyMadeThis.Bamboo.Localisation.Model;
using AMonkeyMadeThis.Common.Controller;
using POSPlacement.Common.AssetBundle.Controller;
using POSPlacement.Common.AssetBundle.Enum;
using POSPlacement.Common.AssetBundle.Model;
using POSPlacement.Common.AssetBundle.Service;
using POSPlacement.Common.Config.Scripts.Controller;
using POSPlacement.Common.Config.Scripts.Model;
using POSPlacement.Common.Data.Scripts.Controller;
using POSPlacement.Common.Data.Scripts.Model;
using POSPlacement.Common.Data.Scripts.Model.VO;
using POSPlacement.Common.Scripts.Application.Controller;
using POSPlacement.Main.Scripts.Model;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace POSPlacement.Main.Scripts.Controller
{
	/// <summary>
	/// MainViewController
	/// </summary>
	[RequireComponent(typeof(MainViewModel))]
	public class MainViewController : AbstractController
	{
		#region Consts

		/// <summary>
		/// Asset bundle name for 'Application'
		/// </summary>
		const string ASSET_BUNDLE_APPLICATION = "Application";
		/// <summary>
		/// Asset bundle name for 'AssetBundles'
		/// </summary>
		const string ASSET_BUNDLE_ASSET_BUNDLES = "AssetBundles";
		/// <summary>
		/// Localisation key to load the asset job pattern eg- "Loading: {0}" or "Descargando: {0}"
		/// </summary>
		const string ASSET_JOB_PATTERN = "Application.Main.Loading.Assets.Job";

		#endregion

		#region Properties

		/// <summary>
		/// Configuration
		/// </summary>
		public ApplicationConfigData Configuration;
		/// <summary>
		/// ApplicationController
		/// </summary>
		ApplicationController ApplicationController;
		/// <summary>
		/// ApplicationConfigController
		/// </summary>
		ApplicationConfigController ApplicationConfigController;
		/// <summary>
		/// AssetBundleModel
		/// </summary>
		ApplicationConfigModel ApplicationConfigModel;
		/// <summary>
		/// ApplicationDataController
		/// </summary>
		ApplicationDataController ApplicationDataController;
		/// <summary>
		/// ApplicationDataModel
		/// </summary>
		ApplicationDataModel ApplicationDataModel;
		/// <summary>
		/// AssetBundleController
		/// </summary>
		AssetBundleController AssetBundleController;
		/// <summary>
		/// AssetBundleModel
		/// </summary>
		AssetBundleModel AssetBundleModel;
		/// <summary>
		/// LocalisationController
		/// </summary>
		LocalisationController LocalisationController;
		/// <summary>
		/// LocalisationModel
		/// </summary>
		LocalisationModel LocalisationModel;
		/// <summary>
		/// MainViewModel
		/// </summary>
		MainViewModel MainViewModel;

		#endregion

		#region Lifecycle

		protected override void CollectComponents()
		{
			base.CollectComponents();

			ApplicationController = ApplicationController.Instance;
			ApplicationConfigController = ApplicationConfigController.Instance;
			ApplicationConfigModel = ApplicationConfigModel.Instance;
			ApplicationDataController = ApplicationDataController.Instance;
			ApplicationDataModel = ApplicationDataModel.Instance;
			AssetBundleController = AssetBundleController.Instance;
			AssetBundleModel = AssetBundleModel.Instance;
			AssetBundleController = AssetBundleController.Instance;
			LocalisationController = LocalisationController.Instance;
			LocalisationModel = LocalisationModel.Instance;
			MainViewModel = GetComponent<MainViewModel>();
		}

		public override void Start()
		{
			base.Start();

			LoadConfig();
		}

		#endregion

		#region Loading

		void LoadConfig()
		{
			ShowConfigPage();
			ApplicationConfigController.LoadConfig(Configuration);
		}

		void LoadData(string dataPath)
		{
			ShowDataPage();
			ApplicationDataController.LoadData(dataPath);
		}

		void LoadLocalisation(List<AssetBundleVO> assetBundles)
		{
			ShowLocalisationPage();
			LocalisationController.LoadLocalisation(assetBundles);
		}

		private void LoadActivations(List<AssetBundleVO> activationAssets)
		{
			ShowAssetsPage();

			AssetBundleService service = AssetBundleController.LoadAssets(activationAssets);

			AddListeners(service);
		}

		#endregion

		#region Page navigation

		void ShowConfigPage()
		{
			MainViewModel.CurrentPage = MainViewModel.Page.Config;
		}

		void ShowDataPage()
		{
			MainViewModel.CurrentPage = MainViewModel.Page.Data;
		}

		void ShowLocalisationPage()
		{
			MainViewModel.CurrentPage = MainViewModel.Page.Localisation;
		}

		void ShowAssetsPage()
		{
			MainViewModel.CurrentPage = MainViewModel.Page.Assets;
		}

		private void ShowCompletePage()
		{
			MainViewModel.CurrentPage = MainViewModel.Page.Complete;
		}

		private void ShowErrorPage()
		{
			MainViewModel.CurrentPage = MainViewModel.Page.Error;
		}

		#endregion

		#region Scene navigation

		public void GoNext()
		{
			ApplicationController.ApplicationInitialised();
		}

		#endregion

		#region Listeners

		public override void AddListeners()
		{
			base.AddListeners();

			AddListeners(AssetBundleController);
			AddListeners(ApplicationConfigModel);
			AddListeners(ApplicationDataModel);
			AddListeners(LocalisationModel);
		}

		private void AddListeners(ApplicationConfigModel applicationConfigModel)
		{
			if (applicationConfigModel != null)
			{
				applicationConfigModel.OnDataChanged += HandleApplicationConfigModelOnDataChanged;
			}
		}

		private void AddListeners(ApplicationDataModel applicationDataModel)
		{
			if (applicationDataModel != null)
			{
				applicationDataModel.OnAssetBundlesChanged += HandleApplicationDataModelOnAssetBundlesChanged;
			}
		}

		private void AddListeners(LocalisationModel localisationModel)
		{
			if (localisationModel != null)
			{
				localisationModel.OnLocaleChanged += HandleLocalisationModelOnLocaleChanged;
			}
		}
		
		private void AddListeners(AssetBundleService service)
		{
			if (service != null)
			{
				service.OnCurrentJobChanged += HandleAssetBundleServiceOnCurrentJobChanged;
				service.OnProgressChanged += HandleAssetBundleServiceOnProgressChanged;
			}
		}

		private void AddListeners(AssetBundleController assetBundleController)
		{
			if (assetBundleController != null)
			{
				assetBundleController.OnError += HandleAssetBundleControllerOnError;
			}
		}

		public override void RemoveListeners()
		{
			base.RemoveListeners();

			RemoveListeners(AssetBundleController);
			RemoveListeners(ApplicationConfigModel);
			RemoveListeners(ApplicationDataModel);
			RemoveListeners(LocalisationModel);
		}

		private void RemoveListeners(ApplicationConfigModel applicationConfigModel)
		{
			if (applicationConfigModel != null)
			{
				applicationConfigModel.OnDataChanged -= HandleApplicationConfigModelOnDataChanged;
			}
		}

		private void RemoveListeners(ApplicationDataModel applicationDataModel)
		{
			if (applicationDataModel != null)
			{
				applicationDataModel.OnAssetBundlesChanged -= HandleApplicationDataModelOnAssetBundlesChanged;
			}
		}
		
		private void RemoveListeners(AssetBundleService service)
		{
			if (service != null)
			{
				service.OnCurrentJobChanged -= HandleAssetBundleServiceOnCurrentJobChanged;
				service.OnProgressChanged -= HandleAssetBundleServiceOnProgressChanged;
			}
		}

		private void RemoveListeners(LocalisationModel localisationModel)
		{
			if (localisationModel != null)
			{
				localisationModel.OnLocaleChanged -= HandleLocalisationModelOnLocaleChanged;
			}
		}

		private void RemoveListeners(AssetBundleController assetBundleController)
		{
			if (assetBundleController != null)
			{
				assetBundleController.OnError -= HandleAssetBundleControllerOnError;
			}
		}

		#endregion

		#region Handlers

		private void HandleApplicationConfigModelOnDataChanged(ApplicationConfigData data)
		{
			LoadData(data.DataPath);
		}

		private void HandleApplicationDataModelOnAssetBundlesChanged(List<AssetBundleVO> assetBundles)
		{
			List<AssetBundleVO> localisationAssets = ApplicationDataModel.GetAssetBundlesByType(AssetBundleType.Locale);

			LoadLocalisation(localisationAssets);
		}

		private void HandleLocalisationModelOnLocaleChanged()
		{
			if (LocalisationModel.Current != LocaleCode.NA)
			{
				List<AssetBundleVO> activationAssets = ApplicationDataModel.GetAssetBundlesByType(AssetBundleType.Activation);

				//LoadActivations(activationAssets);

				ShowCompletePage();
			}
		}

		private void HandleAssetBundleServiceOnCurrentJobChanged(String jobName)
		{
			string assetJobPattern = LocalisationModel.LocaliseString(ASSET_JOB_PATTERN, ASSET_BUNDLE_APPLICATION);
			string assetJobName = LocalisationModel.LocaliseString(jobName, ASSET_BUNDLE_ASSET_BUNDLES);
			string assetJobMessage = string.Format(assetJobPattern, assetJobName);

			MainViewModel.AssetJob = assetJobMessage;
		}

		private void HandleAssetBundleServiceOnProgressChanged(float progress)
		{
			MainViewModel.AssetProgress = progress;
		}

		private void HandleAssetBundleControllerOnError(String message)
		{
			MainViewModel.ErrorMessage = message;
			ShowErrorPage();
		}

		private void HandleAssetBundleServiceOnConnectionError(String message)
		{
			MainViewModel.ErrorMessage = message;
			ShowErrorPage();
		}

		#endregion
	}
}
