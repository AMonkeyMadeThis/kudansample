﻿using AMonkeyMadeThis.Common.Model;
using System;

namespace POSPlacement.Main.Scripts.Model
{
	public class MainViewModel : AbstractModel
	{
		#region Events

		/// <summary>
		/// Delegate to handle changes in page
		/// </summary>
		/// <param name="current"></param>
		public delegate void CurrentPageChanged(Page current);
		/// <summary>
		/// Dispatched when current page changes
		/// </summary>
		public event CurrentPageChanged OnCurrentPageChanged;
		/// <summary>
		/// Delegate to handle changes in asset progress
		/// </summary>
		/// <param name="progress"></param>
		public delegate void AssetProgressChanged(float progress);
		/// <summary>
		/// Dispatched when asset progress changes
		/// </summary>
		public event AssetProgressChanged OnAssetProgressChanged;
		/// <summary>
		/// Delegate to handle changes in asset job
		/// </summary>
		/// <param name="jobName"></param>
		public delegate void AssetJobChanged(string jobName);
		/// <summary>
		/// Dispatched when asset job changes
		/// </summary>
		public event AssetJobChanged OnAssetJobChanged;
		/// <summary>
		/// Delegate to handle changes in error message
		/// </summary>
		/// <param name="errorMessage"></param>
		public delegate void ErrorMessageChanged(string errorMessage);
		/// <summary>
		/// Dispatched when error message changes
		/// </summary>
		public event ErrorMessageChanged OnErrorMessageChanged;

		#endregion

		#region Enums

		/// <summary>
		/// Page
		/// </summary>
		[Flags]
		public enum Page
		{
			None,
			Config,
			Data,
			Localisation,
			Assets,
			Complete,
			Error
		}

		#endregion

		#region Properties

		Page _currentPage;
		/// <summary>
		/// Current
		/// </summary>
		public Page CurrentPage
		{
			get { return _currentPage; }
			set
			{
				SetCurrent(value);
			}
		}

		float _assetProgress;
		/// <summary>
		/// AssetProgress
		/// </summary>
		public float AssetProgress
		{
			get { return _assetProgress; }
			set
			{
				SetAssetProgress(value);
			}
		}

		string _assetJob;
		/// <summary>
		/// AssetJob
		/// </summary>
		public string AssetJob
		{
			get { return _assetJob; }
			set
			{
				SetAssetJob(value);
			}
		}

		string _errorMessage;
		/// <summary>
		/// AssetJob
		/// </summary>
		public string ErrorMessage
		{
			get { return _errorMessage; }
			set
			{
				SetErrorMessage(value);
			}
		}

		#endregion

		#region Mutators

		private void SetCurrent(Page value)
		{
			_currentPage = value;
			DispatchOnCurrentPageChanged();
		}

		private void SetAssetProgress(float value)
		{
			_assetProgress = value;
			DispatchOnAssetProgressChanged();
		}

		private void SetAssetJob(String value)
		{
			_assetJob = value;
			DispatchOnAssetJobChanged();
		}

		private void SetErrorMessage(String value)
		{
			_errorMessage = value;
			DispatchOnErrorMessageChanged();
		}

		#endregion

		#region Dispatchers

		private void DispatchOnCurrentPageChanged()
		{
			if (OnCurrentPageChanged != null)
				OnCurrentPageChanged(CurrentPage);
		}

		private void DispatchOnAssetProgressChanged()
		{
			if (OnAssetProgressChanged != null)
				OnAssetProgressChanged(AssetProgress);
		}

		private void DispatchOnAssetJobChanged()
		{
			if (OnAssetJobChanged != null)
				OnAssetJobChanged(AssetJob);
		}

		private void DispatchOnErrorMessageChanged()
		{
			if (OnErrorMessageChanged != null)
				OnErrorMessageChanged(ErrorMessage);
		}

		#endregion
	}
}
