﻿using AMonkeyMadeThis.Bamboo.UI.Component.Navigation;
using AMonkeyMadeThis.Common.View;
using POSPlacement.Main.Scripts.Controller;
using POSPlacement.Main.Scripts.Model;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace POSPlacement.Main.Scripts.View
{
	/// <summary>
	/// MainView
	/// </summary>
	[RequireComponent(typeof(MainViewModel))]
	[RequireComponent(typeof(MainViewController))]
	public class MainView : AbstractView
	{
		#region Properties

		/// <summary>
		/// MainViewModel
		/// </summary>
		MainViewModel MainViewModel;
		/// <summary>
		/// MainViewController
		/// </summary>
		MainViewController MainViewController;

		#endregion

		#region Skin parts

		/// <summary>
		/// ViewStack
		/// </summary>
		public ViewStack ViewStack;
		/// <summary>
		/// AssetProgressBar
		/// </summary>
		public Image AssetProgressBar;
		/// <summary>
		/// AssetJobLabel
		/// </summary>
		public Text AssetJobLabel;
		/// <summary>
		/// ErrorMessageLabel
		/// </summary>
		public Text ErrorMessageLabel;

		#endregion

		#region Lifecycle

		public override void Awake()
		{
			base.Awake();

			ConfigureScreenRotation();
		}
		
		protected override void CollectComponents()
		{
			base.CollectComponents();

			MainViewModel = GetComponent<MainViewModel>();
			MainViewController = GetComponent<MainViewController>();
		}

		#endregion

		#region Screen rotation

		protected override void ConfigureScreenRotation()
		{
			Screen.autorotateToPortrait = true;
			Screen.autorotateToPortraitUpsideDown = true;
			Screen.autorotateToLandscapeLeft = true;
			Screen.autorotateToLandscapeRight = true;
		}

		#endregion

		#region Page navigation

		private void ShowPage(String page)
		{
			if (ViewStack != null)
				ViewStack.GoToLabel(page);
		}

		public void GoNext()
		{
			MainViewController.GoNext();
		}

		#endregion

		#region Listeners

		public override void AddListeners()
		{
			base.AddListeners();

			AddListeners(MainViewModel);
		}

		private void AddListeners(MainViewModel mainViewModel)
		{
			if (mainViewModel != null)
			{
				mainViewModel.OnCurrentPageChanged += HandleMainViewModelOnCurrentPageChanged;
				mainViewModel.OnAssetJobChanged += HandleMainViewModelOnAssetJobChanged;
				mainViewModel.OnAssetProgressChanged += HandleMainViewModelOnAssetProgressChanged;
				mainViewModel.OnErrorMessageChanged += HandleMainViewModelOnErrorMessageChanged;
			}
		}

		public override void RemoveListeners()
		{
			base.RemoveListeners();

			RemoveListeners(MainViewModel);
		}

		private void RemoveListeners(MainViewModel mainViewModel)
		{
			if (mainViewModel != null)
			{
				mainViewModel.OnCurrentPageChanged -= HandleMainViewModelOnCurrentPageChanged;
				mainViewModel.OnAssetJobChanged -= HandleMainViewModelOnAssetJobChanged;
				mainViewModel.OnAssetProgressChanged -= HandleMainViewModelOnAssetProgressChanged;
				mainViewModel.OnErrorMessageChanged -= HandleMainViewModelOnErrorMessageChanged;
			}
		}

		#endregion

		#region Handlers

		private void HandleMainViewModelOnCurrentPageChanged(MainViewModel.Page current)
		{
			string page = current.ToString();

			ShowPage(page);
		}

		private void HandleMainViewModelOnAssetJobChanged(String jobName)
		{
			if (AssetJobLabel != null)
				AssetJobLabel.text = jobName;
		}

		private void HandleMainViewModelOnAssetProgressChanged(float progress)
		{
			// This should be a 'component' where I just do AssetProgressBar.progress = progress, but time is short ...
			if (AssetProgressBar != null)
				AssetProgressBar.fillAmount = progress;
		}

		private void HandleMainViewModelOnErrorMessageChanged(String errorMessage)
		{
			if (ErrorMessageLabel != null)
				ErrorMessageLabel.text = errorMessage;
		}

		#endregion
	}
}
