﻿using AMonkeyMadeThis.Common.Controller;
using POSPlacement.Common.Data.Scripts.Model;
using POSPlacement.Common.Data.Scripts.Model.VO;
using POSPlacement.Common.Scripts.Application.Controller;
using POSPlacement.Common.Scripts.Application.Model;
using POSPlacement.RegionSelection.Scripts.Model;
using UnityEngine;

namespace POSPlacement.RegionSelection.Scripts.Controller
{
	/// <summary>
	/// BrandSelectionViewController
	/// </summary>
	public class RegionSelectionViewController : AbstractController
	{
		#region Properties

		/// <summary>
		/// ApplicationController
		/// </summary>
		ApplicationController ApplicationController;
		/// <summary>
		/// ApplicationModel
		/// </summary>
		ApplicationModel ApplicationModel;
		/// <summary>
		/// ApplicationDataModel
		/// </summary>
		ApplicationDataModel ApplicationDataModel;
		/// <summary>
		/// BrandSelectionViewModel
		/// </summary>
		RegionSelectionViewModel RegionSelectionViewModel;

		#endregion

		#region Lifecycle

		protected override void CollectComponents()
		{
			base.CollectComponents();

			ApplicationController = ApplicationController.Instance;
			ApplicationModel = ApplicationModel.Instance;
			ApplicationDataModel = ApplicationDataModel.Instance;
			RegionSelectionViewModel = GetComponent<RegionSelectionViewModel>();
		}

		public override void Start()
		{
			base.Start();

			PopulateModel();
		}

		#endregion

		#region Navigation

		public void GoBack()
		{
			ApplicationController.GoBack();
		}

		#endregion

		#region Data

		private void PopulateModel()
		{
			if (ApplicationDataModel != null && ApplicationDataModel.Regions != null && ApplicationDataModel.Regions.Count > 0)
			{
				if (RegionSelectionViewModel != null)
					RegionSelectionViewModel.Regions = ApplicationDataModel.Regions;
				else
					Debug.LogError("Could not find RegionSelectionViewModel");
			}
			else
			{
				Debug.LogError("Regions should be populated at this point");
			}
		}

		public void SelectRegion(RegionVO region)
		{
			if (ApplicationModel != null)
				ApplicationModel.CurrentRegion = region;
			else
				Debug.LogError("Could not find ApplicationModel");
		}

		#endregion
	}
}
