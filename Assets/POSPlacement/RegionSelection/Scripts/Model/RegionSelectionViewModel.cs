﻿using AMonkeyMadeThis.Common.Model;
using POSPlacement.Common.Data.Scripts.Model.VO;
using System.Collections.Generic;

namespace POSPlacement.RegionSelection.Scripts.Model
{
	/// <summary>
	/// BrandSelectionViewModel
	/// </summary>
	public class RegionSelectionViewModel : AbstractModel
	{
		#region Events

		/// <summary>
		/// Delegate to handle changes in regions
		/// </summary>
		/// <param name="regions"></param>
		public delegate void RegionsChanged(List<RegionVO> regions);
		/// <summary>
		/// Dispatched when regions change
		/// </summary>
		public event RegionsChanged OnRegionsChanged;

		#endregion

		#region Properties

		List<RegionVO> _regions;
		/// <summary>
		/// Regions
		/// </summary>
		public List<RegionVO> Regions
		{
			get { return _regions; }
			set
			{
				SetRegions(value);
			}
		}
		
		#endregion

		#region Mutators

		private void SetRegions(List<RegionVO> value)
		{
			_regions = value;
			DispatchOnRegionsChanged();
		}

		#endregion

		#region Dispatchers

		private void DispatchOnRegionsChanged()
		{
			if (OnRegionsChanged != null)
				OnRegionsChanged(Regions);
		}

		#endregion
	}
}
