﻿using System;
using AMonkeyMadeThis.Bamboo.Localisation.Model;
using AMonkeyMadeThis.Bamboo.UI.Component.Data.ItemRenderers;
using POSPlacement.Common.Data.Scripts.Model.VO;
using UnityEngine.UI;

namespace POSPlacement.RegionSelection.Scripts.View.ItemRenderers
{
	public class RegionItemRenderer : ItemRenderer
	{
		#region Properties

		/// <summary>
		/// Data cast as RegionVO
		/// </summary>
		RegionVO RegionData
		{
			get { return Data as RegionVO; }
		}

		#endregion

		#region Skin parts

		/// <summary>
		/// NameLabel
		/// </summary>
		public Text NameLabel;

		#endregion

		#region Lifecycle

		protected override void PopulateUI()
		{
			base.PopulateUI();

			populateName();
		}

		private void populateName()
		{
			string name = LocalisationModel.Instance.LocaliseString(RegionData.Name, ResourceBundle);
			NameLabel.text = name;
		}

		#endregion
	}
}
