﻿using AMonkeyMadeThis.Bamboo.Localisation.Controller;
using AMonkeyMadeThis.Bamboo.UI.Component.Data;
using AMonkeyMadeThis.Bamboo.UI.Component.Data.ItemRenderers;
using AMonkeyMadeThis.Common.Model.VO;
using AMonkeyMadeThis.Common.View;
using POSPlacement.Common.Data.Scripts.Model.VO;
using POSPlacement.RegionSelection.Scripts.Controller;
using POSPlacement.RegionSelection.Scripts.Model;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace POSPlacement.RegionSelection.Scripts.View
{
	/// <summary>
	/// BrandSelectionView
	/// </summary>
	public class RegionSelectionView : AbstractView
	{
		#region Consts

		/// <summary>
		/// RESOURCE_BUNDLE_NAME_REGIONS
		/// </summary>
		const string RESOURCE_BUNDLE_NAME_REGIONS = "Regions";

		#endregion

		#region Properties

		/// <summary>
		/// RegionItemRendererPrototype
		/// </summary>
		public ItemRenderer RegionItemRendererPrototype;
		/// <summary>
		/// BrandSelectionViewModel
		/// </summary>
		RegionSelectionViewModel RegionSelectionViewModel;
		/// <summary>
		/// BrandSelectionViewController
		/// </summary>
		RegionSelectionViewController RegionSelectionViewController;

		#endregion

		#region Skin parts

		/// <summary>
		/// RegionsDataGroup
		/// </summary>
		public DataGroup RegionsDataGroup;

		#endregion

		#region Lifecycle

		protected override void InitVars()
		{
			base.InitVars();

			SetRegionSelectionViewModelItemRendererFunction();
		}

		protected override void CollectComponents()
		{
			base.CollectComponents();

			RegionSelectionViewController = GetComponent<RegionSelectionViewController>();
			RegionSelectionViewModel = GetComponent<RegionSelectionViewModel>();
		}

		public override void Start()
		{
			base.Start();

			if (RegionSelectionViewModel != null)
				PopulateRegionList(RegionSelectionViewModel.Regions);
		}

		#endregion

		#region Screen rotation

		protected override void ConfigureScreenRotation()
		{
			Screen.autorotateToPortrait = true;
			Screen.autorotateToPortraitUpsideDown = true;
			Screen.autorotateToLandscapeLeft = true;
			Screen.autorotateToLandscapeRight = true;
		}

		#endregion

		#region Navigation

		public override void GoBack()
		{
			RegionSelectionViewController.GoBack();
		}

		#endregion

		#region Data group

		private void PopulateRegionList(List<RegionVO> regions)
		{
			if (RegionsDataGroup != null)
			{
				var alphabeticallySorted = regions.OrderBy(go => LocalisationController.Instance.LocaliseString(go.Name, RESOURCE_BUNDLE_NAME_REGIONS));

				RegionsDataGroup.dataSource = alphabeticallySorted.ToArray();
			}
		}

		#endregion

		#region Item renderers

		private void SetRegionSelectionViewModelItemRendererFunction()
		{
			if (RegionsDataGroup != null)
				RegionsDataGroup.ItemRendererFunction = RegionsDataGroupItemRendererFunction;
		}

		/// <summary>
		/// Select an appropriate item renderer for the value object, we know this will only be getting
		/// instances of RegionVO (unless something has gone badly wrong) but the DataGroup is generic
		/// and only really cares if it is given descendents of ValueObjects to render with ItemRenderers.
		/// </summary>
		/// <param name="item">base class for any data, this method will inspec its type and select a specific ItemRenderer</param>
		/// <returns></returns>
		private ItemRenderer RegionsDataGroupItemRendererFunction(ValueObject item)
		{
			if (item is RegionVO)
				return RegionItemRendererPrototype;

			// we could give a 'DefaultItemRenderer' but we don't want anything else showing up in the list, not that it could / should
			return null;
		}
		
		#endregion

		#region Listeners

		public override void AddListeners()
		{
			base.AddListeners();

			AddListeners(RegionSelectionViewModel);
			AddListeners(RegionsDataGroup);
		}

		private void AddListeners(RegionSelectionViewModel regionSelectionViewModel)
		{
			if (regionSelectionViewModel != null)
			{
				regionSelectionViewModel.OnRegionsChanged += HandleRegionSelectionViewModelOnRegionsChanged;
			}
		}

		private void AddListeners(DataGroup regionsDataGroup)
		{
			if (regionsDataGroup != null)
			{
				regionsDataGroup.OnItemSelected += HandleRegionsDataGroupOnItemSelected;
			}
		}

		public override void RemoveListeners()
		{
			base.RemoveListeners();

			RemoveListeners(RegionSelectionViewModel);
		}

		private void RemoveListeners(RegionSelectionViewModel regionSelectionViewModel)
		{
			if (regionSelectionViewModel != null)
			{
				regionSelectionViewModel.OnRegionsChanged -= HandleRegionSelectionViewModelOnRegionsChanged;
			}
		}

		#endregion

		#region Handlers

		private void HandleRegionSelectionViewModelOnRegionsChanged(List<RegionVO> regions)
		{
			PopulateRegionList(regions);
		}

		private void HandleRegionsDataGroupOnItemSelected(ItemRenderer itemRenderer)
		{
			if (itemRenderer != null)
			{
				var region = itemRenderer.Data as RegionVO;

				if (region != null)
					RegionSelectionViewController.SelectRegion(region);
			}
		}

		#endregion
	}
}
